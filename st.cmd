require essioc
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet(EPICS_DB_INCLUDE_PATH, "$(E3_CMD_TOP)/db:$(EPICS_DB_INCLUDE_PATH=.)")

epicsEnvSet("EPICS_CA_ADDR_LIST", "idmz-ro-epics-gw-tn.esss.lu.se")
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")


require iocshutils

dbLoadRecords("bpt20klDewar.dbd")
dbLoadRecords("EL_CM_level.dbd")
dbLoadRecords("SPOKE_CM_level.dbd")

updateMenuConvert


dbLoadRecords("heinv.db")

iocInit()
