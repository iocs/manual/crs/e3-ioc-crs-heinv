/*
Description
Subroutine for subRecord. Init function for class instanciation.
Subroutine name: "subHeMassflow"

Parmeters/Variables:
        A - Difference Pressure (delta P) [ mbar ]
        B - Upstream Pressure [ bara ]
        C - Temperature [ K ]
        D - Density [ kg/m3 ] Output!
        K - Orfice Constant
        The massflow result is returned in VAL  [ g/s ]
        Field D is OUTPUT of density
********************************************************* */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "subRecord.h"
#include "registryFunction.h"
#include "alarm.h"
#include "caeventmask.h"
#include "dbEvent.h"
#include "epicsExport.h"
#include "HeMassflowData.h"
#include "HePack.h"

#define iDP_mbar        prec->a      // Orifice Flow Measurement [ mbar ]
#define iPress_bar      prec->b      // Upstream Pressure [ bara ]
#define iTemp           prec->c      // Temperature [ K ]
#define iConstant       prec->k      // Konstante fuer Blende

#define oMassflow       prec->val    // Massflow output [ g/s ]
#define oDensity        prec->d      // Density output [ kg/m3 ]

typedef struct {
  long magic;
  p_cryoHe_SinglePhase p;
  p_cryoHe_SaturatedVapor v;
  p_cryoHe_SaturatedLiquid l;
} class_pointers_type;

#define HeMassflowMagic	764587L

void subHeMassflowInit (subRecord *prec)
{
  class_pointers_type *cp;
  
  cp = (class_pointers_type *)malloc ((size_t)sizeof(class_pointers_type));
  
  cp->magic = HeMassflowMagic;
  cp->p = NULL; // Construct_cryoHe_SinglePhase();
  cp->v = Construct_cryoHe_SaturatedVapor();
  cp->l = NULL; // Construct_cryoHe_SaturatedLiquid();

  prec->dpvt = (void *)cp;
  prec->brsv = INVALID_ALARM;
}

long subHeMassflow (subRecord *prec)
{
  // p_cryoHe_SinglePhase	p;
   p_cryoHe_SaturatedVapor	v;
  // p_cryoHe_SaturatedLiquid	l;

  int iP, iT1, iT2;
  short phase;
  double press, temp, tempR;
  double dens1, dens2, density;
  double position;	// position of 1/temp or press between value[i] and value[i+1]
  HepackError_t error;
  char dsta[sizeof(prec->dsta)];

  if (!prec->dpvt || ((class_pointers_type *)prec->dpvt)->magic != HeMassflowMagic) {
    error.code = NOT_INITIALIZED;
    prec->brsv = INVALID_ALARM;
    goto subHeMassflow_exit;
  }

  v = ((class_pointers_type *)prec->dpvt)->v;

  press = iPress_bar * 1e5; // convert bar into Pa
  temp = iTemp;
  error.code = NO_ERROR;

  phase = (temp < HePrp_TCRIT && press > cryoHe_SaturatedVapor_Pres_vsT (v, temp, &error)) ? LIQUID : GAS;
  if (error.code != NO_ERROR) goto subHeMassflow_exit;

  for (iP = 0; iP < MAX_iP && press > tabPress[iP+1]; iP++);
  for (iT1 = 0; iT1 < MAX_iT && temp > tabTemp[iT1+1]; iT1++);
  iT2 = iT1;

  if (tabPress[iP] < HePrp_PCRIT) {
    if (phase == GAS) {
      while (tab[iP][iT1].phase == LIQUID && iT1 < MAX_iT) iT1++;
      while (tab[iP+1][iT2].phase == LIQUID && iT2 < MAX_iT) iT2++;
    }
    else { // phase == LIQUID
      while (tab[iP][iT1+1].phase == GAS && iT1 > 0) iT1--;
      while (tab[iP+1][iT2+1].phase == GAS && iT2 > 0) iT2--;
    }
  }

  // positions can be <0 or >1 if index has been shifted because gas is near evaporation line
  tempR = 1./temp; // use reciprocal value because density is proportional to 1/T
  position = (tempR - tabTempR[iT1]) / (tabTempR[iT1+1] - tabTempR[iT1]);// position of 1/T at p1 between 1/T1@p1 and 1/T2@p1
  dens1 = tab[iP][iT1].density +   position * (tab[iP][iT1+1].density   - tab[iP][iT1].density);
  position = (tempR - tabTempR[iT2]) / (tabTempR[iT2+1] - tabTempR[iT2]);// position of 1/T at p2 between 1/T1@p2 and 1/T2@p2
  dens2 = tab[iP+1][iT2].density + position * (tab[iP+1][iT2+1].density - tab[iP+1][iT2].density);
  position = (press - tabPress[iP]) / (tabPress[iP+1] - tabPress[iP]);   // position between p1 and p2
  density = (dens1 + position * (dens2 - dens1));

  if (iDP_mbar < 0)
    oMassflow = -iConstant * sqrt(-iDP_mbar * density);
  else
    oMassflow =  iConstant * sqrt( iDP_mbar * density);

  oDensity = density;
  return 0;

subHeMassflow_exit:
  snprintf (dsta, sizeof(prec->dsta), "%s", (error.code < UNKNOWN_ERROR) ? err_string[error.code]:"?");
  if (strcmp (dsta, prec->dsta)) {
    strcpy (prec->dsta, dsta);
    db_post_events (prec, &prec->dsta, DBE_LOG | DBE_VALUE);
  }
  return -1;	// Error
}
#undef iDP_mbar
#undef iPress_bar
#undef iTemp
#undef iConstant
#undef oMassflow
#undef oDensity

epicsRegisterFunction(subHeMassflowInit);
epicsRegisterFunction(subHeMassflow);
