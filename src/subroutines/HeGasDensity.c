#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <epicsExport.h>
#include <subRecord.h>
#include <aSubRecord.h>

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++/
/ Routine HeGasDensity

Purpose:
Calculates volume of helium Density and Mass.

/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define _P      psub->a  /* Input; Pressure Value in bara */
#define _T      psub->b  /* Input; Temperature Value in K */
#define _V      psub->c  /* Input; Volume in Liter */
#define _Rho    psub->vala  /* Output; Density in Kg/m3 */
#define _M      psub->valb  /* Output; Mass in Kg */

/* Formula constants */
#define RHO_COEFFICIENT 0.4446
#define RHO_EXPONENT 1.2
#define RHO_MOLAR_MASS 48.14  /* Molar mass of helium in g/mol */

long HeGasDensity(struct subRecord *psub)
{
    /* Calculate Density (Rho) using the given formula */
    _Rho = 1 / (1 + RHO_COEFFICIENT * (pow((_P / _T), RHO_EXPONENT))) * (_P / _T) * RHO_MOLAR_MASS;

    /* Calculate Mass (M) using the given formula */
    _M = _Rho * _V * 0.001;  /* Volume is in liters, so we multiply by 0.001 to convert to cubic meters */

    /* Debugging log for checking values */
    printf("HeGasDensity: Pressure = %.2f ; Temperature = %.2f ; Density (Rho) = %.2f ; Mass (M) = %.2f\n",
           _P, _T, _Rho, _M);

    return 0; /* 0 = sync. processing, no callback */
}

epicsRegisterFunction(HeGasDensity);
