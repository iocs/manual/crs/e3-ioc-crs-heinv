/*	$Id: heliumSubr.c,v 1.3 2022/12/09 09:26:36 schoeneb Exp $
 *	Author:	Christian Gerke
 *	Date:	12-01-2004
 *
 *	Experimental Physics and Industrial Control System (EPICS)
 *
 * Modification Log:
 * -----------------
 *	15.1.2008 ChG: add subLossHeliumBath
 */
/*+/mod***********************************************************************
* TITLE heliumSubr.c - helium routines
*
* DESCRIPTION
*	This module contains routines for helium properties.
*	* subVolCyl
*	* subVolHelium
*	* subLossHeliumBath
*
* QUICK REFERENCE
*
* BUGS
*
* NOTES
*
* SEE ALSO
*
*-***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "subRecord.h"
#include "alarm.h"
#include "dbEvent.h"		/* db_post_events */
#include "caeventmask.h"	/* DBE_LOG ... */
#include "registryFunction.h"
#include "epicsExport.h"

#define KUBIKMETER_FL_IN_GAS (1000./1.4) /* m**3 Gas / liter liquid He*/


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++/
Routine subVolCyl

Purpose:
Calculates volume of helium in lying cylinder.


Input:
level 0..100% from record field A,
radius of cylinder              B,
length of cylinder              C.

Output:
equivalent Gas volume in m**3 of liquid helium into the VAL field (0..720)

All names beginning with "_" are placeholders for record fields.

/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define _Level		psub->a	/* Input; measured level, % of diameter */
#define _Radius		psub->b	/* Input; radius */
#define _Length		psub->c	/* Input; length */
#define _Value          psub->val /* Output; volume */

long subVolCyl (struct subRecord *psub)
{
  double norm_level, beta, area, vol;
 
/*  first geometry */

  norm_level = (1. - _Level / 50.);
  beta       = acos( norm_level);
  area       = _Radius*_Radius * ( beta - norm_level * sin( beta));
  vol        = area * _Length;

/*  second helium properties */
  
  _Value     = vol  * KUBIKMETER_FL_IN_GAS;
  
  return 0;	/* 0 = sync. processing, no callback */
}
#undef _Level	
#undef _Radius	
#undef _Length	
#undef _Value        
#undef KUBIKMETER_FL_IN_GAS

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++/
Routine subVolHelium

Purpose:
For a given volume of helium with certain Temperature and pressure this
routine calculates which volume this amount of helium would have at
room temperature and 1 bar


Input:
volume in m3         A,
temperature in K     B,
pressure in bar      C.

Output:
equivalent Gas volume in m**3  into the VAL field

All names beginning with "_" are placeholders for record fields.

/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define _Volume		psub->a	/* Input; volume of container with helium */
#define _Temp		psub->b	/* Input; measured temperature in container */
#define _Press		psub->c	/* Input; measured pressure in container */
#define _Value          psub->val /* Output; normalized volume */

#define VolLiquid 0.0079 /* volume of 1 kg liquid helium at 4.5 K and 2.5 bar */
#define VolNormal 5.9886 /* volume of 1 kg helium at 288,16 K (15 C) and 1 bar*/

long subVolHelium (struct subRecord *psub)
{
  double a, b, actualSpecificVolume;
 

  if (_Temp < 0.225 * _Press + 4.5)             /*  assume constant density */
    actualSpecificVolume = VolLiquid;            	/*   for liquid helium      */
  else
    {
    a = 0.021 * pow( _Press, -1.0056);
    if (_Press < 2.)
      b = -005;
    else
      b = -0.0184 * pow( _Press, -1.041) + 0.002;
    actualSpecificVolume = a * _Temp + b;  
    }
  _Value = _Volume / actualSpecificVolume * VolNormal;
  
  return 0;	/* 0 = sync. processing, no callback */
}
#undef _Volume
#undef _Temp
#undef _Press
#undef _Value
#undef VolLiquid
#undef VolNormal

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++/
Routine subLossHeliumBath

Purpose:
adapt FORTRAN subroutine TESWATTS written by Bernd Petersen 21.07.1994
use C++ version of HEPAK written by V. Gubarev

Das Unterprogramm errechnet nach Vorgabe der Eingangstemperatur, des
Eingangsdrucks und des Entspannungsdrucks die Qualitaet des Fluids im
Austritt eines Joule-Thomson-Ventils fuer HELIUM.
Ausserdem nach Vorgabe des Gesamtmassenflusses die Verdampfungsverluste
eines Badkryostaten unter der Voraussetzung, dass der Fluessigkeitslevel
im Kryostaten konstant gehalten wird.
Vom Gesamtmassenfluss wird der Gasanteil bei der JT-Entspannung abgezogen.

Input:
temperature at entrance [K]  		A,
pressure at entrance [bar]     		B,
pressure at exit [mbar]      		C,
vaporated total massflow [g/s]		D,
Heater power [W]			E,
Insulaton heat loss [W]			F,

Output:
temperature at exit [K]	2-phase		G,
quality of medium			H,
 [0 = fluid, 1 = gaseous
  0 < Qual < 1	2 phase
  +2,-2,-1 	1-phase]
enthalpie of single phase liquid [J/kg]	I,
enthalpie of gaseous fraction [J/kg]	J,
enthalpie of liquid fraction [J/kg]	K,
total vapouration power of bath [W]	L,
vaporation power [J/s]			VAL

All names beginning with "_" are placeholders for record fields.
*-***************************************************************************/
#define _Tin	psub->a		/* Input; temperature at entrance */
#define _Pin	psub->b		/* Input; pressure at entrance */
#define _Pout	psub->c		/* Input; pressure at exit */
#define _Xmges	psub->d 	/* Input; vaporated total massflow */
#define _PHeat	psub->e		/* Input; power of heater */
#define _PIsol	psub->f		/* Input; power due to isolation */
#define _Tout	psub->g 	/* Output; temprature at exit */
#define _Qual	psub->h 	/* Output; quality of medium */
#define _EnthS	psub->i		/* Output; enthalpie of fluid */
#define _EnthV	psub->j		/* Output; enthalpie of gaseous fraction (2-phase) */
#define _EnthL	psub->k		/* Output; enthalpie of liquid fraction (2-phase) */
#define _Power	psub->l		/* Output; total vaporation power of bath */
#define _Watt	psub->val	/* Output; vaporation power due to load */

#include "HePack.h"

typedef struct {
  long magic;
  p_cryoHe_SinglePhase p;
  p_cryoHe_SaturatedVapor v;
  p_cryoHe_SaturatedLiquid l;
  } class_pointers_type;

#define LossHeliumBathMagic 174410L

void subLossHeliumBathInit (struct subRecord *psub)
  {
  class_pointers_type *cp;
  
  cp = (class_pointers_type *)malloc ((size_t)sizeof(class_pointers_type));
  
  cp->magic = LossHeliumBathMagic;
  cp->p = Construct_cryoHe_SinglePhase();
  cp->v = Construct_cryoHe_SaturatedVapor();
  cp->l = Construct_cryoHe_SaturatedLiquid();

  psub->dpvt = (void *)cp;
  psub->brsv = INVALID_ALARM;
  }


long subLossHeliumBath (struct subRecord *psub)
{
  p_cryoHe_SinglePhase		p;
  p_cryoHe_SaturatedVapor	v;
  p_cryoHe_SaturatedLiquid	l;
  
  double dens, flowTotal, pIn, pOut, tOut;
  double enthS, enthL, enthV, enthVaporation;
  double qual;
  HepackError_t error;
  char dsta[sizeof(psub->dsta)];

  if (!psub->dpvt || ((class_pointers_type *)psub->dpvt)->magic != LossHeliumBathMagic) {
    error.code = NOT_INITIALIZED;
    psub->brsv = INVALID_ALARM;
    goto subLossHeliumBath_exit;
  }

  p = ((class_pointers_type *)psub->dpvt)->p;
  v = ((class_pointers_type *)psub->dpvt)->v;
  l = ((class_pointers_type *)psub->dpvt)->l;

  error.code = NO_ERROR;
  error.line[0] = '\0';
 
  flowTotal = _Xmges / 1000.;	/* _Xmges [g/s]  -> flowTotal [kg/s] */
  pIn = _Pin * 100000.;		/* _Pin   [bar]  -> pIn       [Pa]   */
  pOut = _Pout * 100.;		/* _Pout  [mbar] -> pOut      [Pa]   */

  dens  = cryoHe_SinglePhase_Dens_vsPT (p, pIn, _Tin, &error);
  if (error.code != NO_ERROR) goto subLossHeliumBath_exit;
  enthS = cryoHe_SinglePhase_Enth_vsDTP (p, dens, _Tin, pIn, &error);
  if (error.code != NO_ERROR) goto subLossHeliumBath_exit;
  
  /* 2-phase temperature */
  tOut = cryoHe_SaturatedVapor_Temp_vsP (v, pOut, &error);
  if (error.code != NO_ERROR) goto subLossHeliumBath_exit;
  
  /* quality = fraction of gas */
  
  enthV = cryoHe_SaturatedVapor_Enth_vsT (v, tOut, &error);
  if (error.code != NO_ERROR) goto subLossHeliumBath_exit;
  enthL = cryoHe_SaturatedLiquid_Enth_vsT (l, tOut, &error);
  if (error.code != NO_ERROR) goto subLossHeliumBath_exit;
  enthVaporation = enthV - enthL;

  if ((enthS > enthL) && (enthV > enthL))
    qual = (enthS - enthL) / enthVaporation;
  else
    qual = 0.;

  _EnthS = enthS;
  _EnthV = enthV;
  _EnthL = enthL;
  _Qual = qual;
  _Tout = tOut;
  
  if ((0. < qual) && (qual < 1.))
    {
    _Power = (1. - qual) * flowTotal * enthVaporation;
    _Watt  = _Power - _PHeat - _PIsol;	/* correct for power from heater and isolation */
    }
  else
    {
    _Power = 0.;
    _Watt  = 0.;
    }
  if (psub->dsta[0] != '\0') {
    psub->dsta[0] = '\0';
    db_post_events (psub, &psub->dsta, DBE_LOG | DBE_VALUE);
  }
  return 0;	/* 0 = sync. processing, no callback */

subLossHeliumBath_exit:
  snprintf (dsta, sizeof(psub->dsta), "%s", (error.code < UNKNOWN_ERROR) ? err_string[error.code]:"?");
  if (strcmp (dsta, psub->dsta)) {
    strcpy (psub->dsta, dsta);
    db_post_events (psub, &psub->dsta, DBE_LOG | DBE_VALUE);
  }
  return (-1);	/* Error */
}
#undef _Tin
#undef _Pin
#undef _Pout
#undef _Xmges
#undef _PHeat
#undef _PIsol
#undef _Tout
#undef _Qual
#undef _EnthS
#undef _EnthV
#undef _EnthL
#undef _Power
#undef _Watt

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++/
Routine HepakEnthalpyPT

Purpose:
calculate Enthalpy from Helium Properties
use C++ version of HEPAK written by V. Gubarev

Input:
temperature [K]         A,
pressure [bar]          B,
Helium phase            C
        1 = 1-phase Helium
        2 = 2-phase Helium

Output:
1-phase helium enthalpy [J/kg]                          I,
2-phase helium enthalpy, gaseous fraction [J/kg]        J,
2-phase helium enthalpy, liquid fraction [J/kg]         K,
1-phase helim enthalpy [J/g]                            VAL,
2-phase helium vaporation, enthalpy difference [J/g]    VAL

All names beginning with "_" are placeholders for record fields.
*-***************************************************************************/
#define _T      psub->a         /* Input; temperature */
#define _P      psub->b         /* Input; pressure */
#define _Phase  psub->c         /* Input; He phase type */
#define _T2     psub->g         /* Output; 2-phase temperature, determined by p */
#define _EnthS  psub->i         /* Output; enthalpy of 1-phase helium */
#define _EnthV  psub->j         /* Output; enthalpy of gaseous fraction (2-phase) */
#define _EnthL  psub->k         /* Output; enthalpy of liquid fraction (2-phase) */
#define _Result psub->val       /* Output;      if 1-phase: heat load
                                                if 2-phase: vaporation power due to load */

#define HepakEnthalpyPTMagic 1391286L

void subHepakEnthalpyPTInit (subRecord *psub)
{
  class_pointers_type *cp;

  cp = (class_pointers_type *)malloc ((size_t)sizeof(class_pointers_type));
  cp->magic = HepakEnthalpyPTMagic;
  cp->p = Construct_cryoHe_SinglePhase();
  cp->v = Construct_cryoHe_SaturatedVapor();
  cp->l = Construct_cryoHe_SaturatedLiquid();
  psub->dpvt = (void *)cp;
  psub->brsv = INVALID_ALARM;
}

long subHepakEnthalpyPT (subRecord *psub)
{
  p_cryoHe_SinglePhase          p;
  p_cryoHe_SaturatedVapor       v;
  p_cryoHe_SaturatedLiquid      l;
  double dens, pres, temp, enthS, enthV, enthL;
  int phas;
  HepackError_t error;
  char dsta[sizeof(psub->dsta)];

  if (!psub->dpvt || ((class_pointers_type *)psub->dpvt)->magic != HepakEnthalpyPTMagic) {
    error.code = NOT_INITIALIZED;
    psub->brsv = INVALID_ALARM;
    goto subHepakEnthalpyPT_exit;
  }

  p = ((class_pointers_type *)psub->dpvt)->p;
  v = ((class_pointers_type *)psub->dpvt)->v;
  l = ((class_pointers_type *)psub->dpvt)->l;

  error.code = NO_ERROR;
  error.line[0] = '\0';

  phas = (int)(_Phase + 0.1);
  pres = _P * 100000.;          /* _P   [bar]  -> pres       [Pa]   */

  switch (phas) {
    case 2:
      /* 2-phase Helium */
      temp  = cryoHe_SaturatedVapor_Temp_vsP (v, pres, &error);
      if (error.code != NO_ERROR) goto subHepakEnthalpyPT_exit;
      enthV = cryoHe_SaturatedVapor_Enth_vsT (v, temp, &error);
      if (error.code != NO_ERROR) goto subHepakEnthalpyPT_exit;
      enthL = cryoHe_SaturatedLiquid_Enth_vsT (l, temp, &error);
      if (error.code != NO_ERROR) goto subHepakEnthalpyPT_exit;
      _Result = (enthV - enthL) / 1000.; /* J/kg -> J/g */
      enthS = 0.;
      break;
    case 1:
    default:
      /* 1-phase Helium */
      temp  = _T;
      dens  = cryoHe_SinglePhase_Dens_vsPT( p, pres, temp, &error);
      if (error.code != NO_ERROR) goto subHepakEnthalpyPT_exit;
      enthS = cryoHe_SinglePhase_Enth_vsDTP( p, dens, temp, pres, &error);
      if (error.code != NO_ERROR) goto subHepakEnthalpyPT_exit;
      _Result = enthS / 1000.; /* J/kg -> J/g */;
      enthV = 0.;
      enthL = 0.;
      break;
  }
  _T2   = temp;
  _EnthS = enthS;
  _EnthV = enthV;
  _EnthL = enthL;
  if (psub->dsta[0] != '\0') {
    psub->dsta[0] = '\0';
    db_post_events (psub, &psub->dsta, DBE_LOG | DBE_VALUE);
  }
  return 0;

subHepakEnthalpyPT_exit:
  snprintf (dsta, sizeof(psub->dsta), "%s", (error.code < UNKNOWN_ERROR) ? err_string[error.code]:"?");
  if (strcmp (dsta, psub->dsta)) {
    strcpy (psub->dsta, dsta);
    db_post_events (psub, &psub->dsta, DBE_LOG | DBE_VALUE);
  }
  return -1;
}

#undef _T
#undef _P
#undef _Phase
#undef _EnthS
#undef _EnthV
#undef _EnthL
#undef _T2
#undef _Result

#define _T      psub->a         /* Input; temperature */
#define _P      psub->b         /* Input; pressure */
#define _Phase  psub->c         /* Input; He phase type */
#define _T2     psub->g         /* Output; 2-phase temperature, determined by p */
#define _EnthS  psub->i         /* Output; entropy of 1-phase helium */
#define _EnthV  psub->j         /* Output; entropy of gaseous fraction (2-phase) */
#define _EnthL  psub->k         /* Output; entropy of liquid fraction (2-phase) */
#define _Result psub->val       /* Output;      if 1-phase: heat load
                                                if 2-phase: vaporation power due to load */

#define HepakEntropyPTMagic 264043993L

void subHepakEntropyPTInit (struct subRecord *psub){
    class_pointers_type *cp;
    cp = (class_pointers_type *)malloc ((size_t)sizeof(class_pointers_type));
    cp->magic = HepakEntropyPTMagic;
    cp->p = Construct_cryoHe_SinglePhase();
    cp->v = Construct_cryoHe_SaturatedVapor();
    cp->l = Construct_cryoHe_SaturatedLiquid();
    psub->dpvt = (void *)cp;
    psub->brsv = INVALID_ALARM;
}

long subHepakEntropyPT (struct subRecord *psub){
  p_cryoHe_SinglePhase          p;
  p_cryoHe_SaturatedVapor       v;
  p_cryoHe_SaturatedLiquid      l;

  double dens, pres, temp, entr, entrV, entrL;
  int phas;
  HepackError_t error;
  char dsta[sizeof(psub->dsta)];

  if (!psub->dpvt || ((class_pointers_type *)psub->dpvt)->magic != HepakEntropyPTMagic) {
    error.code = NOT_INITIALIZED;
    psub->brsv = INVALID_ALARM;
    goto subHepakEntropyPT_exit;
  }

  p = ((class_pointers_type *)psub->dpvt)->p;
  v = ((class_pointers_type *)psub->dpvt)->v;
  l = ((class_pointers_type *)psub->dpvt)->l;

  error.code = NO_ERROR;
  error.line[0] = '\0';

  phas = (int)(_Phase + 0.1);
  pres = _P * 100000.;          /* _P   [bar]  -> pres       [Pa]   */

  switch (phas) {
    case 2:
        /* 2-phase Helium */
        temp  = cryoHe_SaturatedVapor_Temp_vsP (v, pres, &error);
	if (error.code != NO_ERROR) goto subHepakEntropyPT_exit;
        entrV = cryoHe_SaturatedVapor_Entr_vsT (v, temp, &error);
	if (error.code != NO_ERROR) goto subHepakEntropyPT_exit;
        entrL = cryoHe_SaturatedLiquid_Entr_vsT (l, temp, &error);
	if (error.code != NO_ERROR) goto subHepakEntropyPT_exit;
        _Result = (entrV-entrL)/1000;
      break;
    case 1:
    default:
      /* 1-phase Helium */
      temp  = _T;
      dens  = cryoHe_SinglePhase_Dens_vsPT( p, pres, temp, &error);
      if (error.code != NO_ERROR) goto subHepakEntropyPT_exit;
      entr = cryoHe_SinglePhase_Entr_vsDT( p, dens, temp, &error);
      if (error.code != NO_ERROR) goto subHepakEntropyPT_exit;
      _Result = entr/1000;
      break;
  }
  if (psub->dsta[0] != '\0') {
    psub->dsta[0] = '\0';
    db_post_events (psub, &psub->dsta, DBE_LOG | DBE_VALUE);
  }
  return 0;

subHepakEntropyPT_exit:
  snprintf (dsta, sizeof(psub->dsta), "%s %s", error.line, error.code < UNKNOWN_ERROR ? err_string[error.code]:"?");
  if (strcmp (dsta, psub->dsta)) {
    strcpy (psub->dsta, dsta);
    db_post_events (psub, &psub->dsta, DBE_LOG | DBE_VALUE);
  }
  return -1;
}


epicsRegisterFunction(subVolCyl);
epicsRegisterFunction(subVolHelium);
epicsRegisterFunction(subLossHeliumBathInit);
epicsRegisterFunction(subLossHeliumBath);
epicsRegisterFunction(subHepakEnthalpyPTInit);
epicsRegisterFunction(subHepakEnthalpyPT);
epicsRegisterFunction(subHepakEntropyPTInit);
epicsRegisterFunction(subHepakEntropyPT);

