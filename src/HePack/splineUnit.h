//  Created by: V.Gubarev -MKS1-
//

#ifndef _splineUnit_h
#define _splineUnit_h

class splineUnit
{
  public:
  
    //Constructor(s)
    splineUnit();
    
    void init(double x0, double x1, const double *Coeff);
    void init(double x0, double x1, double c1,double c2,double c3,double c4);
    
    //Operators
    double operator () (double x) const;          // y = splineUnit(x)
  
    // Methods
    int    xIn(double x) const;  // true if x between X0 and X1
    double at(double x)  const;  // the same as  y = splineUnit(x)
    double dF(double x)  const;  // calculate derivativeI value at x
    double d2F(double x) const;  // calculate derivativeII value at x
    double Integral(double x0, double x1) const;  // calculate integral on [x0,x1]
  
    double Xo;  // start point
    double Xn;  // end point
    
    // Destructor
    ~splineUnit();

  private:
  
    double A;
    double B;
    double C;
    double D;
};

#endif
