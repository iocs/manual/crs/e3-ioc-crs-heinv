#include "HepackError.h"

struct cryoHe_SinglePhase;
struct cryoHe_SaturatedVapor;
struct cryoHe_SaturatedLiquid;
typedef struct cryoHe_SinglePhase* p_cryoHe_SinglePhase;
typedef struct cryoHe_SaturatedVapor* p_cryoHe_SaturatedVapor;
typedef struct cryoHe_SaturatedLiquid* p_cryoHe_SaturatedLiquid;

double cryoHe_SinglePhase_Dens_vsPT (p_cryoHe_SinglePhase, double, double, HepackError_t *);
double cryoHe_SinglePhase_Enth_vsDTP (p_cryoHe_SinglePhase, double, double, double, HepackError_t *);
double cryoHe_SinglePhase_Entr_vsDT (p_cryoHe_SinglePhase, double, double, HepackError_t *);

p_cryoHe_SinglePhase Construct_cryoHe_SinglePhase (void);
void Destruct_cryoHe_SinglePhase (struct cryoHe_SinglePhase*);

double cryoHe_SaturatedVapor_Temp_vsP (p_cryoHe_SaturatedVapor, double, HepackError_t *);
double cryoHe_SaturatedVapor_Enth_vsT (p_cryoHe_SaturatedVapor, double, HepackError_t *);
double cryoHe_SaturatedVapor_Entr_vsT (p_cryoHe_SaturatedVapor, double, HepackError_t *);
double cryoHe_SaturatedVapor_Pres_vsT (p_cryoHe_SaturatedVapor, double, HepackError_t *);
p_cryoHe_SaturatedVapor Construct_cryoHe_SaturatedVapor (void);
void Destruct_cryoHe_SaturatedVapor (p_cryoHe_SaturatedVapor);

double cryoHe_SaturatedLiquid_Temp_vsP (p_cryoHe_SaturatedLiquid, double, HepackError_t *);
double cryoHe_SaturatedLiquid_Enth_vsT (p_cryoHe_SaturatedLiquid, double, HepackError_t *);
double cryoHe_SaturatedLiquid_Entr_vsT (p_cryoHe_SaturatedLiquid, double, HepackError_t *);
p_cryoHe_SaturatedLiquid Construct_cryoHe_SaturatedLiquid (void);
void Destruct_cryoHe_SaturatedLiquid (p_cryoHe_SaturatedLiquid);
