/*
 * HepackError.c
 *
 *  Created on: Feb 5, 2016
 *      Author: schoeneburg, hrickens
 */

#include "HepackError.h"

const char *err_string[] = {"OK", "DIV/0", "sqrt(-n)", ">MAX i", "out Gibb", "TPD inv", "not initialized", "?"};
