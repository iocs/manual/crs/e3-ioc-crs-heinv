//
//  Calculations for temperatures 0.8 to 1500 K (excluding the 2-phase region)
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 22-May-2006
//  Modified: 
//    on 18-Apr-2007 by V.Gubarev 
//        o Included Visc_vsDT(Dens, Temp)
//    on 23-Apr-2007 by V.Gubarev 
//        o Included Dens_vsPT(Pres, Temp)
//        o Included Temp_vsDP(Dens, Pres)
//
//  Notes:
//    1. Valid for temperature range 0.8 to 1500 K
//
//    2. Test on input parameters excluded (!)
//
//    3. Dens_vsPG(double Pres, double Gibb)
//       valid for compress liquid for temperature range 0.8 to 3 K
//
//    4. Temp_vsPG(double Pres, double Gibb)
//       valid for compress liquid for temperature range 0.8 to 3 K
//
//  Exceptions:
//

#include "cryoHe_SinglePhase.h"
#include <iostream>

//  Class cryoHe_SinglePhase
//
cryoHe_SinglePhase::cryoHe_SinglePhase() {
  for (int i = 0; i <= cryoHe_SinglePhase_meth_; i++)
    stt[i] = 0;
  //
  lowTemp = HePrp_TMIN;
  uppTemp = HePrp_TMAX;
  lowPres = 1.46;
  uppPres = 30.2e+5;
}
// (1) FUNCTION PRESS (D, T)
// Pressure [Pa] as a function of density [kg/m3] and temperature [K]
double cryoHe_SinglePhase::Pres_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (Temp <= 0. || Dens <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(1, Dens, Temp)) {
    switch (overlapTemp(Dens, Temp)) {
    case 0:
      val[1][0] = calcArp.Pres_vsDT(Dens, Temp, perr);
      break;
    case 2:
      val[1][0] = calcMcC.Pres_vsDT(Dens, Temp, perr);
      break;
    case 1:
      double wt = (Temp - minTempMcC) / (maxTempArp - minTempMcC);
      val[1][0] = wt * calcMcC.Pres_vsDT(Dens, Temp, perr) + (1. - wt) * calcArp.Pres_vsDT(Dens, Temp, perr);
      break;
    }
  }
  return val[1][0];
}
// (2) SUBROUTINE DFPT (IDID, D, X, P, T)
// density given pressure and temperature [SI units].
double cryoHe_SinglePhase::Dens_vsPT(double Pres, double Temp, HepackError_t *perr) {
  if (Temp <= 0. || Pres <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(2, Pres, Temp)) {
    int maxCount = 1000;
    double Do = 1e-8;
    if ((Temp <= HePrp_TCRIT) && (Pres >= mLine.lowPres))
      Do = mLine.lowDens;   // Melting state
    else if ((Temp <= HePrp_TCRIT) && (Pres >= sBase.Pres_vsT(Temp, perr)))
      Do = HePrp_DCRIT;     // Saturated liquid
    double Fo = Pres_vsDT(Do, Temp, perr), dD = 5, Dn, Fn;
    while (dD > 1e-10) {
      for (Dn = Do + dD, Fn = Pres_vsDT(Dn, Temp, perr);
          ((Pres - Fo) * (Pres - Fn) > 0) && (!perr || !perr->code) && --maxCount;
          Do = Dn, Fo = Fn, Dn += dD, Fn = Pres_vsDT(Dn, Temp, perr) )
        ;
      if (!maxCount) {HE_ERROR(MAX_ITERATIONS) break;}
      dD = (Dn - Do) / 10;
    }
    val[2][0] = (Do + Dn) / 2;
  }
  return val[2][0];
}
// (3) SUBROUTINE TFDP (IDID, T, X, DL, DV, D, P)
// Temperature given Density and Pressure [SI units].
double cryoHe_SinglePhase::Temp_vsDP(double Dens, double Pres, HepackError_t *perr) {
  if (Dens <= 0. || Pres <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(3, Dens, Pres)) {
    int maxCount = 1000;
    if ((0.000888248 <= Dens) && (Dens <= 146.16) && (1.47515 <= Pres) && (Pres <= 227462))
      val[3][0] = sBase.Temp_vsP(Pres, perr);
    else {
      double To = lowTemp, Fo = Pres_vsDT(Dens, To, perr), dT = 100, Tn, Fn;
      while (dT > 1e-10) {
        for (Tn = To + dT, Fn = Pres_vsDT(Dens, Tn, perr);
            ((Fo - Pres) * (Fn - Pres) > 0) && --maxCount;
            To = Tn, Fo = Fn, Tn += dT, Fn = Pres_vsDT(Dens, Tn, perr))
          ;
        if (!maxCount) {HE_ERROR(MAX_ITERATIONS) break;}
        dT = (Tn - To) / 10;
      }
      val[3][0] = (To + Tn) / 2;
    }
  }
  return val[3][0];
}
// (4) FUNCTION DPDT (D, T)
// dP/dT as a function of density and temperature [SI units]
double cryoHe_SinglePhase::dPdT_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(4, Dens, Temp)) {
    switch (overlapTemp(Dens, Temp)) {
    case 0:
      val[4][0] = calcArp.dPdT_vsDT(Dens, Temp, perr);
      break;
    case 2:
      val[4][0] = calcMcC.dPdT_vsDT(Dens, Temp, perr);
      break;
    case 1:
      double wt = (Temp - minTempMcC) / (maxTempArp - minTempMcC);
      val[4][0] = wt * calcMcC.dPdT_vsDT(Dens, Temp, perr) + (1. - wt) * calcArp.dPdT_vsDT(Dens, Temp, perr);
      break;
    }
  }
  return val[4][0];
}
// (5) FUNCTION DPDD (D, T)
// dP/dD as a function of density and temperature [SI units]
double cryoHe_SinglePhase::dPdD_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(5, Dens, Temp)) {
    switch (overlapTemp(Dens, Temp)) {
    case 0:
      val[5][0] = calcArp.dPdD_vsDT(Dens, Temp, perr);
      break;
    case 2:
      val[5][0] = calcMcC.dPdD_vsDT(Dens, Temp, perr);
      break;
    case 1:
      double wt = (Temp - minTempMcC) / (maxTempArp - minTempMcC);
      val[5][0] = wt * calcMcC.dPdD_vsDT(Dens, Temp, perr) + (1. - wt) * calcArp.dPdD_vsDT(Dens, Temp, perr);
      break;
    }
  }
  return val[5][0];
}
// (6) FUNCTION CV (D, T)
// Cv as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Cv_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(6, Dens, Temp)) {
    switch (overlapTemp(Dens, Temp)) {
    case 0:
      val[6][0] = calcArp.Cv_vsDT(Dens, Temp, perr);
      break;
    case 2:
      val[6][0] = calcMcC.Cv_vsDT(Dens, Temp, perr);
      break;
    case 1:
      double wt = (Temp - minTempMcC) / (maxTempArp - minTempMcC);
      val[6][0] = wt * calcMcC.Cv_vsDT(Dens, Temp, perr) + (1. - wt) * calcArp.Cv_vsDT(Dens, Temp, perr);
      break;
    }
  }
  return val[6][0];
}
// (7) FUNCTION ENTROP (D, T)
// Entropy as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Entr_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(7, Dens, Temp)) {
    switch (overlapTemp(Dens, Temp)) {
    case 0:
      val[7][0] = calcArp.Entr_vsDT(Dens, Temp, perr);
      break;
    case 2:
      val[7][0] = calcMcC.Entr_vsDT(Dens, Temp, perr);
      break;
    case 1:
      double wt = (Temp - minTempMcC) / (maxTempArp - minTempMcC);
      val[7][0] = wt * calcMcC.Entr_vsDT(Dens, Temp, perr) + (1. - wt) * calcArp.Entr_vsDT(Dens, Temp, perr);
      break;
    }
  }
  return val[7][0];
}
// (8) SUBROUTINE SHAUG (PRP)
// Helmholtz energy as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Helm_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(8, Dens, Temp)) {
    switch (overlapTemp(Dens, Temp)) {
    case 0:
      val[8][0] = calcArp.Helm_vsDT(Dens, Temp, perr);
      break;
    case 2:
      val[8][0] = calcMcC.Ener_vsDT(Dens, Temp, perr) - Temp * calcMcC.Entr_vsDT(Dens, Temp, perr);
      break;
    case 1:
      double wt = (Temp - minTempMcC) / (maxTempArp - minTempMcC);
      val[8][0] = wt * (calcMcC.Ener_vsDT(Dens, Temp, perr) - Temp * calcMcC.Entr_vsDT(Dens, Temp, perr))
          + (1. - wt) * calcArp.Entr_vsDT(Dens, Temp, perr);
      break;
    }
  }
  return val[8][0];
}
// (9) SUBROUTINE SHAUG (PRP)
// Internal energy as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Ener_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(9, Dens, Temp)) {
    switch (overlapTemp(Dens, Temp)) {
    case 0:
      val[9][0] = calcArp.Helm_vsDT(Dens, Temp, perr) + Temp * calcArp.Entr_vsDT(Dens, Temp, perr);
      break;
    case 2:
      val[9][0] = calcMcC.Ener_vsDT(Dens, Temp, perr);
      break;
    case 1:
      double wt = (Temp - minTempMcC) / (maxTempArp - minTempMcC);
      val[9][0] = wt * calcMcC.Ener_vsDT(Dens, Temp, perr)
          + (1. - wt) * (calcArp.Helm_vsDT(Dens, Temp, perr) + Temp * calcArp.Entr_vsDT(Dens, Temp, perr));
      break;
    }
  }
  return val[9][0];
}
// (10) SUBROUTINE SHAUG (PRP)
// Gibbs energy as a function of density, temperature, and pressure [SI units]
double cryoHe_SinglePhase::Gibb_vsDTP(double Dens, double Temp, double Pres, HepackError_t *perr) {
  if (Dens <= 0. || Temp <= 0. || Pres <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(10, Dens, Temp, Pres)) {
    switch (overlapTemp(Dens, Temp)) {
    case 0:
      val[10][0] = calcArp.Helm_vsDT(Dens, Temp, perr) + Pres / Dens;
      break;
    case 2:
      val[10][0] = calcMcC.Ener_vsDT(Dens, Temp, perr) - Temp * calcMcC.Entr_vsDT(Dens, Temp, perr) + Pres / Dens;
      break;
    case 1:
      double wt = (Temp - minTempMcC) / (maxTempArp - minTempMcC);
      val[10][0] = wt * (calcMcC.Ener_vsDT(Dens, Temp, perr) - Temp * calcMcC.Entr_vsDT(Dens, Temp, perr) + Pres / Dens)
          + (1. - wt) * (calcArp.Helm_vsDT(Dens, Temp, perr) + Pres / Dens);
      break;
    }
  }
  return val[10][0];
}
// (11) FUNCTION DTG (D)
// Gibbs energy as a function of density and (from /SUBRT/) T. [SI units]
double cryoHe_SinglePhase::Gibb_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(11, Dens, Temp))
    val[11][0] = Gibb_vsDTP(Dens, Temp, calcArp.Pres_vsDT(Dens, Temp, perr), perr);
  return val[11][0];
}
// (12) SUBROUTINE SHAUG (PRP)
// Enthalpy as a function of density, temperature, and pressure [SI units]
double cryoHe_SinglePhase::Enth_vsDTP(double Dens, double Temp, double Pres, HepackError_t *perr) {
  if (Dens <= 0. || Temp <= 0. || Pres <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(12, Dens, Temp, Pres)) {
    switch (overlapTemp(Dens, Temp)) {
    case 0:
      val[12][0] = calcArp.Helm_vsDT(Dens, Temp, perr) + Temp * calcArp.Entr_vsDT(Dens, Temp, perr) + Pres / Dens;
      break;
    case 2:
      val[12][0] = calcMcC.Ener_vsDT(Dens, Temp, perr) + Pres / Dens;
      break;
    case 1:
      double wt = (Temp - minTempMcC) / (maxTempArp - minTempMcC);
      val[12][0] = wt * (calcMcC.Ener_vsDT(Dens, Temp, perr) + Pres / Dens)
          + (1. - wt) * (calcArp.Helm_vsDT(Dens, Temp, perr) + Temp * calcArp.Entr_vsDT(Dens, Temp, perr) + Pres / Dens);
      break;
    }
  }
  return val[12][0];
}
// (13) special function to use in "Properties"
// Enthalpy as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Enth_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(13, Dens, Temp))
    val[13][0] = Enth_vsDTP(Dens, Temp, Pres_vsDT(Dens, Temp, perr), perr);
  return val[13][0];
}
// (14) from SUBROUTINE DERIV (F, DI, TI)
// Cp = Specific heat at constant P [J/(kG-K)] as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Cp_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(14, Dens, Temp))
    val[14][0] = Gamma_vsDT(Dens, Temp, perr) * Cv_vsDT(Dens, Temp, perr);
  return val[14][0];
}
// (15) from SUBROUTINE DERIV (F, DI, TI)
// Gamma = Cp/Cv as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Gamma_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(15, Dens, Temp))
    val[15][0] = 1. + Alpha_vsDT(Dens, Temp, perr) * Grun_vsDT(Dens, Temp, perr);
  return val[15][0];
}
// (16) from SUBROUTINE DERIV (F, DI, TI)
// Alpha = (T/V)(dV/dT)  at constant P as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Alpha_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(16, Dens, Temp))
    val[16][0] = Temp * dPdT_vsDT(Dens, Temp, perr) * Kt_vsDT(Dens, Temp, perr);
  return val[16][0];
}
// (17) from SUBROUTINE DERIV (F, DI, TI)
// Grun = (V/Cv)(dP/dT) at constant V as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Grun_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(17, Dens, Temp))
    val[17][0] = dPdT_vsDT(Dens, Temp, perr) / Dens / Cv_vsDT(Dens, Temp, perr);
  return val[17][0];
}
// (18) from SUBROUTINE DERIV (F, DI, TI)
// Kt = (1/D)(dD/dP)  at constant T [1/Pa] as a function of density and temperature [SI units]
double cryoHe_SinglePhase::Kt_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(18, Dens, Temp))
    val[18][0] = 1. / dPdD_vsDT(Dens, Temp, perr) / Dens;
  return val[18][0];
}
// (19) from SUBROUTINE DERIV (F, DI, TI)
// VSound (Kt) = velocity of sound [m/s] as a function of density and temperature [SI units]
double cryoHe_SinglePhase::VSound_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(19, Dens, Temp)) {
    double out = Gamma_vsDT(Dens, Temp, perr) * dPdD_vsDT(Dens, Temp, perr);
    val[19][0] = sqrt(out < 1. ? 1. : out);
  }
  return val[19][0];
}
// (20) from SUBROUTINE DERIV (F, DI, TI)
// JouleThomson (JT) = Joule-Thomson coefficient [K/Pa] as a function of density and temperature [SI units]
double cryoHe_SinglePhase::JThomson_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(20, Dens, Temp))
    val[20][0] = (Alpha_vsDT(Dens, Temp, perr) - 1.) / Dens / Cp_vsDT(Dens, Temp, perr);
  return val[20][0];
}
// (21) from SUBROUTINE DERIV (F, DI, TI)
// V*(DH/DV) at constant P [J/kG] as a function of density and temperature [SI units]
double cryoHe_SinglePhase::VdHdV_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(21, Dens, Temp)) {
    double out = Gamma_vsDT(Dens, Temp, perr) * dPdD_vsDT(Dens, Temp, perr);
    val[21][0] = (out < 1. ? 1. : out) / Grun_vsDT(Dens, Temp, perr);
  }
  return val[21][0];
}
// (22) from SUBROUTINE DTFPG (IDID, DCALC, TCALC, P, G)
// Density and temperature as a function of pressure and Gibbs energy [SI units]
// This subroutine valid only in the compressed liquid between 0.8 & 3 K
double cryoHe_SinglePhase::Dens_vsPG(double Pres, double Gibb, HepackError_t *perr) {
  if (todo(22, Pres, Gibb))
    DT_vsPG(Pres, Gibb, perr);
  return val[22][0];
}
// (23) from SUBROUTINE DTFPG (IDID, DCALC, TCALC, P, G)
// Density and temperature as a function of pressure and Gibbs energy [SI units]
// This subroutine valid only in the compressed liquid between 0.8 & 3 K
double cryoHe_SinglePhase::Temp_vsPG(double Pres, double Gibb, HepackError_t *perr) {
  if (todo(23, Pres, Gibb))
    DT_vsPG(Pres, Gibb, perr);
  return val[23][0];
}
// (24) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Density as a function of pressure and enthalpy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Dens_vsPH(double sDens, double sTemp, double Pres, double Enth, HepackError_t *perr) {
  if (todo(24, sDens, sTemp, Pres, Enth))
    DT_vsPH(sDens, sTemp, Pres, Enth, perr);
  return val[24][0];
}
// (25) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Temperature as a function of pressure and enthalpy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Temp_vsPH(double sDens, double sTemp, double Pres, double Enth, HepackError_t *perr) {
  if (todo(25, sDens, sTemp, Pres, Enth))
    DT_vsPH(sDens, sTemp, Pres, Enth, perr);
  return val[25][0];
}
// (26) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Density as a function of pressure and entropy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Dens_vsPS(double sDens, double sTemp, double Pres, double Entr, HepackError_t *perr) {
  if (todo(26, sDens, sTemp, Pres, Entr))
    DT_vsPS(sDens, sTemp, Pres, Entr, perr);
  return val[26][0];
}
// (27) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Temperature as a function of pressure and entropy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Temp_vsPS(double sDens, double sTemp, double Pres, double Entr, HepackError_t *perr) {
  if (todo(27, sDens, sTemp, Pres, Entr))
    DT_vsPS(sDens, sTemp, Pres, Entr, perr);
  return val[27][0];
}
// (28) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Density as a function of pressure and internal energy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Dens_vsPU(double sDens, double sTemp, double Pres, double Ener, HepackError_t *perr) {
  if (todo(28, sDens, sTemp, Pres, Ener))
    DT_vsPU(sDens, sTemp, Pres, Ener, perr);
  return val[28][0];
}
// (29) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Temperature as a function of pressure and internal energy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Temp_vsPU(double sDens, double sTemp, double Pres, double Ener, HepackError_t *perr) {
  if (todo(29, sDens, sTemp, Pres, Ener))
    DT_vsPU(sDens, sTemp, Pres, Ener, perr);
  return val[29][0];
}
// (30) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Density as a function of pressure and Gibbs energy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Dens_vsPG(double sDens, double sTemp, double Pres, double Gibb, HepackError_t *perr) {
  if (todo(30, sDens, sTemp, Pres, Gibb))
    DT_vsPG(sDens, sTemp, Pres, Gibb, perr);
  return val[30][0];
}
// (31) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Temperature as a function of pressure and Gibbs energy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Temp_vsPG(double sDens, double sTemp, double Pres, double Gibb, HepackError_t *perr) {
  if (todo(31, sDens, sTemp, Pres, Gibb))
    DT_vsPG(sDens, sTemp, Pres, Gibb, perr);
  return val[31][0];
}
// (32) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Density as a function of enthalpy and entropy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Dens_vsHS(double sDens, double sTemp, double Enth, double Entr, HepackError_t *perr) {
  if (todo(32, sDens, sTemp, Enth, Entr))
    DT_vsHS(sDens, sTemp, Enth, Entr, perr);
  return val[32][0];
}
// (33) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
// Recalculated Temperature as a function of enthalpy and entropy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
double cryoHe_SinglePhase::Temp_vsHS(double sDens, double sTemp, double Enth, double Entr, HepackError_t *perr) {
  if (todo(33, sDens, sTemp, Enth, Entr))
    DT_vsHS(sDens, sTemp, Enth, Entr, perr);
  return val[33][0];
}
// (34) from FUNCTION VISCOS (DKGM3,TK)
// Viscosity [Pa-s] as a function of density [kg/m3] and temperature [K]
double cryoHe_SinglePhase::Visc_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(34, Dens, Temp)) {
    //
    double Tl = 3.5, Th = 3.75, dT = Th - Tl, vlD = 69.6412374;
    //
    if (Temp <= Tl)
      val[34][0] = (Dens > vlD) ? visLam(Dens, Temp, perr) : visLpt(Dens, Temp, perr);
    else if (Temp >= Th)
      val[34][0] = visHi(Dens, Temp, perr);
    else
      val[34][0] = visHi(Dens, Temp, perr) * (Temp - Tl) / dT
          + (1. - (Temp - Tl) / dT) * ((Dens > vlD) ? visLam(Dens, Temp, perr) : visLpt(Dens, Temp, perr));
  }
  return val[34][0];
}
//  from SUBROUTINE AMLAP (TMINM, TMAXA, D)
// Computes the overlap temperatures between Arp and McCarty equations
// as a function of density [kg/m3].
// return 0 -- use Arp equations only
//        1 -- mixed region: use both types of equations
//        2 -- use McCarty equations only
int cryoHe_SinglePhase::overlapTemp(double Dens, double Temp) {
  //
  double tm = 2.53, ta = 2.98, dmin = 140, dmax = 190;
  double dt = 0;
  //
  minTempMcC = 0;
  maxTempArp = 0.1;
  if ((Dens >= dmin) && (Dens <= dmax)) {
    dt = -0.0056 * (Dens - dmin);  // Note: -0.28 [K] / (DMAX-DMIN) = -0.0056 = slope of boundary line
    minTempMcC = tm + dt;
    maxTempArp = ta + dt;
    if (Dens > 180) {
      dt = -0.035 * (Dens - 180.);
      minTempMcC += dt;
      maxTempArp += dt;
    }
  }
  if (Temp <= minTempMcC)
    return 0;
  if (Temp >= maxTempArp)
    return 2;
  return 1;
}
//
// Destructor(s)
cryoHe_SinglePhase::~cryoHe_SinglePhase() {
}
//
//  Private Part
//

// (22 23) from SUBROUTINE DTFPG (IDID, DCALC, TCALC, P, G)
// Density and temperature as a function of pressure and Gibbs energy [SI units]
// This subroutine valid only in the compressed liquid between 0.8 & 3 K
void cryoHe_SinglePhase::DT_vsPG(double Pres, double Gibb, HepackError_t *perr) {
  todo(22, Pres, Gibb);
  todo(23, Pres, Gibb);
  //
  double D = fixT.Dens175K_vsP(Pres);
  double G = Gibb_vsDTP(D, lowTemp, Pres, perr);
  double G3K = fixT.Enth3K_vsP(Pres) - 3. * fixT.Entr3K_vsP(Pres);
  //
  if ((Gibb < G) || (G3K < Gibb))
    HE_ERROR(GIBB_RANGE)
  //
  val[22][0] = Dens_vsPG(D, lowTemp + (Gibb - G) / (G3K - G) * 2.2, Pres, Gibb, perr);
  val[23][0] = Temp_vsPG(D, lowTemp + (Gibb - G) / (G3K - G) * 2.2, Pres, Gibb, perr);
}
// (24,25) from SUBROUTINE DTILXY (D, T, J, 'PH', X, Y)
// Recalculated Temperature as a function of pressure and enthalpy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
void cryoHe_SinglePhase::DT_vsPH(double sDens, double sTemp, double Pres, double Enth, HepackError_t *perr) {
  todo(24, sDens, sTemp, Pres, Enth);
  todo(25, sDens, sTemp, Pres, Enth);
  //
  double D = sDens, dD, T = sTemp, dT, P, dP, dH;
  double GR, VS, JT, Cp;
  int i = 30;
  //
  if (sDens <= 0.|| sTemp <= 0. || Pres <= 0.) {HE_ERROR(VALUE_OUT) return;}
  for (int clc = (0 == 0), cnd1 = (0 != 0), cnd2 = (0 != 0); ((i != 0) && (!cnd1) && (!cnd2)); i--) {
    if (clc) {
      GR = Grun_vsDT(D, T, perr);
      VS = VSound_vsDT(D, T, perr);
      JT = JThomson_vsDT(D, T, perr);
      Cp = Cp_vsDT(D, T, perr);
    }
    P = Pres_vsDT(D, T, perr);
    dP = Pres - P;
    dH = Enth - Enth_vsDTP(D, T, P, perr);
    if (VS <= 0.|| Cp == 0.) {HE_ERROR(DIV0) break;}
    dD = ((1. + GR) * dP - D * GR * dH) / VS / VS;
    dT = JT * dP + dH / Cp;
    if ((fabs(dD / D) + fabs(dT / T) < 0.0004) && (i > 3))
      clc = (0 != 0);
    if (clc) {
      if ((i > 7) && (fmod((double) i, (double) 4) == 0)) {
        dD = dD / 2;
        dT = dT / 2;
      }
      if (fabs(dD / D) > 0.3)
        dD *= (0.3 * D) / fabs(dD); // against long step and keep sign
      if (fabs(dT / T) > 0.3)
        dT *= (0.3 * T) / fabs(dT);
    }
    cnd1 = ((fabs(dP) < 3.e-6 * Pres + 3.e-2) && (fabs(dH) < 3.e-6 * Enth + 3.e-2));
    D += dD;
    T += dT;
    cnd2 = ((fabs(dD / D) < 1.e-7) && (fabs(dT / T) < 1.e-7));
  }
  if (i == 0)
    HE_ERROR(MAX_ITERATIONS)
  else {
    val[24][0] = D;
    val[25][0] = T;
  }
}
// (26,27) from SUBROUTINE DTILXY (D, T, J, 'PS', X, Y)
// Recalculated Temperature as a function of pressure and entropy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
void cryoHe_SinglePhase::DT_vsPS(double sDens, double sTemp, double Pres, double Entr, HepackError_t *perr) {
  todo(26, sDens, sTemp, Pres, Entr);
  todo(27, sDens, sTemp, Pres, Entr);
  //
  double D = sDens, dD, T = sTemp, dT, P, dP, dS;
  double GR = 0., VS= 0., AL= 0., Cp= 0.;
  int i = 30;
  //
  if (sDens <= 0.|| sTemp <= 0. || Pres <= 0.) {HE_ERROR(VALUE_OUT) return;}
  for (int clc = (0 == 0), cnd1 = (0 != 0), cnd2 = (0 != 0); ((i != 0) && (!cnd1) && (!cnd2)); i--) {
    if (clc) {
      GR = Grun_vsDT(D, T, perr);
      VS = VSound_vsDT(D, T, perr);
      AL = Alpha_vsDT(D, T, perr);
      Cp = Cp_vsDT(D, T, perr);
    }
    P = Pres_vsDT(D, T, perr);
    dP = Pres - P;
    dS = Entr - Entr_vsDT(D, T, perr);
    if (VS <= 0.|| Cp == 0.) {HE_ERROR(DIV0) break;}
    dD = (dP - GR * D * T * dS) / VS / VS;
    dT = (AL * dP / D + T * dS) / Cp;
    if ((fabs(dD / D) + fabs(dT / T) < 0.0004) && (i > 3))
      clc = (0 != 0);
    if (clc) {
      if ((i > 7) && (fmod((double) i, (double) 4) == 0)) {
        dD = dD / 2;
        dT = dT / 2;
      }
      if (fabs(dD / D) > 0.3)
        dD *= (0.3 * D) / fabs(dD); // against long step and keep sign
      if (fabs(dT / T) > 0.3)
        dT *= (0.3 * T) / fabs(dT);
    }
    cnd1 = ((fabs(dP / Pres) < 3.e-6) && (fabs(T * dS / Cp) < 1.e-5));
    D += dD;
    T += dT;
    cnd2 = ((fabs(dD / D) < 1.e-7) && (fabs(dT / T) < 1.e-7));
  }
  if (i == 0)
    HE_ERROR(MAX_ITERATIONS)
  else {
    val[26][0] = D;
    val[27][0] = T;
  }
}
// (28,29) from SUBROUTINE DTILXY (D, T, J, 'PU', X, Y)
// Recalculated Temperature as a function of pressure and internal energy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
void cryoHe_SinglePhase::DT_vsPU(double sDens, double sTemp, double Pres, double Ener, HepackError_t *perr) {
  todo(28, sDens, sTemp, Pres, Ener);
  todo(29, sDens, sTemp, Pres, Ener);
  //
  double D = sDens, dD, T = sTemp, dT, P, dP, dU;
  double GR= 0., VS= 0., AL= 0., Cp= 0., Kt= 0., GA= 0.;
  double xx= 0., yy= 0.;
  int i = 30;
  //
  if (sDens <= 0.|| sTemp <= 0. || Pres <= 0.) {HE_ERROR(VALUE_OUT) return;}
  for (int clc = (0 == 0), cnd1 = (0 != 0), cnd2 = (0 != 0); ((i != 0) && (!cnd1) && (!cnd2)); i--) {
    if (clc) {
      GR = Grun_vsDT(D, T, perr);
      VS = VSound_vsDT(D, T, perr);
      AL = Alpha_vsDT(D, T, perr);
      Cp = Cp_vsDT(D, T, perr);
      Kt = Kt_vsDT(D, T, perr);
      GA = Gamma_vsDT(D, T, perr);
    }
    P = Pres_vsDT(D, T, perr);
    dP = Pres - P;
    dU = Ener - Ener_vsDT(D, T, perr);
    xx = P * Kt;
    if (VS <= 0.|| Cp == 0. || GA == 0.) {HE_ERROR(DIV0) break;}
    yy = 1. - GR * yy / GA;
    if (yy == 0.) {HE_ERROR(DIV0) break;}
    dD = (dP - GR * D * dU) / VS / VS / yy;
    dT = ((AL - xx) * dP / D + dU) / Cp / yy;
    if ((fabs(dD / D) + fabs(dT / T) < 0.0004) && (i > 3))
      clc = (0 != 0);
    if (clc) {
      if ((i > 7) && (fmod((double) i, (double) 4) == 0)) {
        dD = dD / 2;
        dT = dT / 2;
      }
      if (fabs(dD / D) > 0.3)
        dD *= (0.3 * D) / fabs(dD); // against long step and keep sign
      if (fabs(dT / T) > 0.3)
        dT *= (0.3 * T) / fabs(dT);
    }
    cnd1 = ((fabs(dP) < 3.e-6 * Pres + 3.e-2) && (fabs(dU) < 3.e-6 * Ener + 1.e-2));
    D += dD;
    T += dT;
    if (D == 0.|| T == 0.) {HE_ERROR(DIV0) break;}
    cnd2 = ((fabs(dD / D) < 1.e-7) && (fabs(dT / T) < 1.e-7));
  }
  if (i == 0)
    HE_ERROR(MAX_ITERATIONS)
  else {
    val[28][0] = D;
    val[29][0] = T;
  }
}
// (30,31) from SUBROUTINE DTILXY (D, T, J, 'PG', X, Y)
// Recalculated Temperature as a function of pressure and Gibbs energy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
void cryoHe_SinglePhase::DT_vsPG(double sDens, double sTemp, double Pres, double Gibb, HepackError_t *perr) {
  todo(30, sDens, sTemp, Pres, Gibb);
  todo(31, sDens, sTemp, Pres, Gibb);
  //
  double D = sDens, dD, T = sTemp, dT, P, dP, dG, Entr;
  double GR= 0., Cv= 0., AL= 0.;
  int i = 30;
  //
  if (sDens <= 0.|| sTemp <= 0. || Pres <= 0.) {HE_ERROR(VALUE_OUT) return;}
  for (int clc = (0 == 0), cnd1 = (0 != 0), cnd2 = (0 != 0); ((i != 0) && (!cnd1) && (!cnd2)); i--) {
    if (clc) {
      GR = Grun_vsDT(D, T, perr);
      Cv = Cv_vsDT(D, T, perr);
      AL = Alpha_vsDT(D, T, perr);
    }
    P = Pres_vsDT(D, T, perr);
    dP = Pres - P;
    dG = Gibb - Gibb_vsDTP(D, T, P, perr);
    Entr = Entr_vsDT(D, T, perr);
    if (Entr == 0.) {HE_ERROR(DIV0) return;}
    dD = (AL / Entr / T) * ((Entr / (GR * Cv) - 1.) * dP + D * dG);
    dT = (dP / D - dG) / Entr;
    if ((fabs(dD / D) + fabs(dT / T) < 0.0004) && (i > 3))
      clc = (0 != 0);
    if (clc) {
      if ((i > 7) && (fmod((double) i, (double) 4) == 0)) {
        dD = dD / 2;
        dT = dT / 2;
      }
      if (fabs(dD / D) > 0.3)
        dD *= (0.3 * D) / fabs(dD); // against long step and keep sign
      if (fabs(dT / T) > 0.3)
        dT *= (0.3 * T) / fabs(dT);
    }
    Entr = Entr_vsDT(D, T, perr);
    if (Entr == 0.) {HE_ERROR(DIV0) return;}
    cnd1 = (((fabs(dP / Pres) < 1.e-6) || (fabs(dP) < 3.e-2)) && (fabs(dG / Entr) < 1.e-4));
    D += dD;
    T += dT;
    if (D == 0. || T == 0.) {HE_ERROR(DIV0) return;}
    cnd2 = ((fabs(dD / D) < 1.e-7) && (fabs(dT / T) < 1.e-7));
  }
  if (i == 0)
    HE_ERROR(MAX_ITERATIONS)
  else {
    val[30][0] = D;
    val[31][0] = T;
  }
}
// (32,33) from SUBROUTINE DTILXY (D, T, J, 'HS', X, Y)
// Recalculated Temperature as a function of enthalpy and entropy [SI units]
//  sDens -- an initial estimate for density
//  sTemp -- an initial estimate for temperature
void cryoHe_SinglePhase::DT_vsHS(double sDens, double sTemp, double Enth, double Entr, HepackError_t *perr) {
  todo(32, sDens, sTemp, Enth, Entr);
  todo(33, sDens, sTemp, Enth, Entr);
  //
  double D = sDens, dD, T = sTemp, dT, dH, dS, P;
  double GR = 0., VS= 0., JT= 0.;
  int i = 30;
  //
  if (sDens <= 0.|| sTemp <= 0.) {HE_ERROR(VALUE_OUT) return;}
  for (int clc = (0 == 0), cnd1 = (0 != 0), cnd2 = (0 != 0); ((i != 0) && (!cnd1) && (!cnd2)); i--) {
    if (clc) {
      GR = Grun_vsDT(D, T, perr);
      VS = VSound_vsDT(D, T, perr);
      JT = JThomson_vsDT(D, T, perr);
    }
    P = Pres_vsDT(D, T, perr);
    dH = Enth - Enth_vsDTP(D, T, P, perr);
    dS = Entr - Entr_vsDT(D, T, perr);
    if (VS <= 0.) {HE_ERROR(DIV0) break;}
    dD = D * (dH - (1. + GR) * T * dS) / VS / VS;
    dT = T * (GR * dH / VS - D * JT * dS);
    if ((fabs(dD / D) + fabs(dT / T) < 0.0004) && (i > 3))
      clc = (0 != 0);
    if (clc) {
      if ((i > 7) && (fmod((double) i, (double) 4) == 0)) {
        dD = dD / 2;
        dT = dT / 2;
      }
      if (fabs(dD / D) > 0.3)
        dD *= (0.3 * D) / fabs(dD); // against long step and keep sign
      if (fabs(dT / T) > 0.3)
        dT *= (0.3 * T) / fabs(dT);
    }
    cnd1 = ((fabs(dH) < 1.e-6 * fabs(Enth) + 1.e-2) && (fabs(T * dS) < 1.e-6 * fabs(Entr) + 1.e-2));
    D += dD;
    T += dT;
    if (D == 0. || T == 0.) {HE_ERROR(DIV0) return;}
    cnd2 = ((fabs(dD / D) < 1.e-7) && (fabs(dT / T) < 1.e-7));
  }
  if (i == 0)
    HE_ERROR(MAX_ITERATIONS)
  else {
    val[32][0] = D;
    val[33][0] = T;
  }
}
//  FUNCTION VISCHI (DKGM3,T)
// Viscosity [Pa-s] as a function of density [kg/m3] and temperature [K]
// this function is valid only for T>3.5K
double cryoHe_SinglePhase::visHi(double Dens, double Temp, HepackError_t *perr) {
  if (Dens <= 0.|| Temp <= 0.) {HE_ERROR(VALUE_OUT) return 0.;}
  double t1 = 100, t2 = 110, dT = t2 - t1, tm = 300, r = Dens / 1000.;
  double b = -47.5295259 / Temp + 87.6799309 + Temp * (-42.0741589 + Temp * (8.33128289 - Temp * 0.589252385));
  double c = 547.309267 / Temp - 904.870586 + Temp * (431.404928 + Temp * (-81.4504854 + Temp * 5.37008433));
  double d = -1684.39324 / Temp + 3331.08630 + Temp * (-1632.19172 + Temp * (308.804413 - Temp * 20.2936367));
  double lnT = (Temp <= tm) ? log(Temp) : log(tm), hV;
  double lV = exp(-0.135311743 / lnT + 1.00347841 + lnT * (1.20654649 + lnT * (-0.149564551 + lnT * 0.0125208416))),
      oV = lV;
  //
  if (Temp > t1) {
    hV = 196. * pow(Temp, 0.71938) * exp((12.451 - 295.67 / Temp) / Temp - 4.1249);
    oV = (Temp >= t2) ? hV : lV + (hV - lV) * (Temp - t1) / dT;
  }
  return (1e-07 * (oV + lV * (exp(r * (b + r * (c + r * d))) - 1.)));
}
//  FUNCTION VISLAM (DD, TT)
// Liquid helium viscosity [Pa-s] as a function of density [kg/m3] and temperature [K]
// this function is valid only for 1.2 <= T <= 3.8 and 132 <= D <= 180+
double cryoHe_SinglePhase::visLam(double Dens, double Temp, HepackError_t *perr) {
  double out = 0;
  //
  if (Temp >= 1.2) {
    //
    double e1 = 1.8, e2 = -4.7;
    double a1 = 0.2505885162e+01, a2 = 0.5230553382e+00, a3 = 0.5607799718e+00;
    double b1 = -0.1127424846e+03, b2 = 0.2095894826e+03, b3 = -0.1286503418e+03, b4 = -0.7951538104e+02;
    double b5 = 0.2015521019e+03, b6 = -0.1231199069e+03, b7 = -0.6460724357e+02, b8 = 0.5424829902e+02;
    double c1 = 0.8772148954e+03, c2 = -0.2515234338e+04, c3 = 0.2679676294e+04, c4 = 0.3469587682e+03;
    double c5 = -0.1509946785e+04, c6 = 0.2056348276e+04, c7 = 0.2886724375e+03, c8 = -0.3831832082e+03;
    double lT = lLine.Temp_vsD(Dens, perr), x = fabs(1. - Temp / lT), D = 10. * (Dens / 146.15 - 1.);
    //
    if (Temp <= lT) {
      out = x * x * (c1 + x * (c2 + x * c3) + D * (c4 + x * (c5 + x * c6) + D * x * (c7 + x * c8)))
          + 10. * (1. + e2 * pow(x + 1e-8, 0.84)) * (a1 + D * (a2 + D * a3));
    } else {
      out = x * x * (b1 + x * (b2 + x * b3) + D * (b4 + x * (b5 + x * b6) + D * x * (b7 + x * b8)))
          + 10. * (1. + e1 * pow(x + 1e-8, 0.84)) * (a1 + D * (a2 + D * a3));
    }
  }
  return out * 1e-7;
}
//  FUNCTION VISLPT (D, T)
// Vapor helium viscosity [Pa-s] as a function of density [kg/m3] and temperature [K]
// this function is valid only for T < 3.5
double cryoHe_SinglePhase::visLpt(double Dens, double Temp, HepackError_t *perr) {
  double a = 0.2539133848, b = 0.1024277783e-1;
  return (Temp < 1.2) ? 0 : (a * Temp + b * Dens) * 1e-6;
}

int cryoHe_SinglePhase::todo(int fNo, double p1) {
  return todo(fNo, p1, -1, -1, -1);
}
int cryoHe_SinglePhase::todo(int fNo, double p1, double p2) {
  return todo(fNo, p1, p2, -1, -1);
}
int cryoHe_SinglePhase::todo(int fNo, double p1, double p2, double p3) {
  return todo(fNo, p1, p2, p3, -1);
}
int cryoHe_SinglePhase::todo(int fNo, double p1, double p2, double p3, double p4) {
  val[fNo][1] = p1;
  val[fNo][2] = p2;
  val[fNo][3] = p3;
  val[fNo][4] = p4;
  stt[fNo] = 1;
  return 1;
}
