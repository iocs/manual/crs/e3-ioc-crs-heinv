//
//  Class spline
//
//  Function Interpolation by 3-Order Polynom
//
//  Created by: V.Gubarev -MKS1-
//

#ifndef _spline_h
#define _spline_h

#include <stdlib.h>
#include <stdio.h>

#include "splineUnit.h"

class spline
{
  public:
  
    //Constructor(s)
    spline(int nIntervals, double x0, double x1);
    
    //Operators
    double operator () (double x) const;        // y = spline(x)
  
    // Methods
    int    xIn(double x) const;
    double at(double x)  const;
    double dF(double x)  const;
    double d2F(double x) const;
    double Integral(double x0, double x1) const;
  
    const double Xo;      // start point
    const double Xn;      // end point
    const int    nUnits;  // # intervals or spline units
    
    // Destructor
    ~spline();

  protected:
  
    splineUnit *Unit;
    
};

#endif
