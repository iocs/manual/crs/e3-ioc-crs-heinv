//
//  He calculations via V.Arp equations
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 16-May-2006
//  Modified: 
//
//  Notes:
//    1. Valid in compressed liquid from 0.8 to about 3 K.
//    2. Test on input parameters excluded (!)
//
//  Exceptions:
//

extern "C" {
#include "HepackError.h"
}
#include "cryoHe_ArpEquations.h"

//  Class cryoHe_ArpEquations
//
cryoHe_ArpEquations::cryoHe_ArpEquations() {
  for (int i = 0; i <= cryoHe_ArpEquations_meth_; i++)
    stt[i] = 0;
}
// (1) FUNCTION PRESSA (D, TT)
// Pressure [Pa] as a function of density [kg/m3] and temperature [K]
double cryoHe_ArpEquations::Pres_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(1, Dens, Temp)) {
    //
    double *c = (lline.HeState_vsDT(Dens, Temp, perr) == 1) ? PrpHeI : PrpHeII;
    double t = lline.HeTemp_vsDT(Dens, Temp, perr), t2 = t * t;
    double x = lline.deltaV_vsD(Dens, perr), f = 1;
    double q[5] = { 0. };
    //
    for (int k = 0; k < 6; k++, f *= x) {
      for (int j = 0; j < 5; j++)
        q[j] += f * c[6 * (j + 1) + k - 1];
    }
    double out = q[0] + t2 * (q[1] + t2 * (q[2] + t2 * (q[3] + t2 * q[4]))); // "background" pressure [MPa]
    out = (logFun.Pres_vsDT(Dens, Temp, perr) + out) * 1e+6;          // to Si units: *1e+6
    val[1][0] = out;
  }
  return val[1][0];
}
// (2) FUNCTION DPDTA (D, TT)
// dP/dT as a function of density and temperature [SI units]
double cryoHe_ArpEquations::dPdT_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(2, Dens, Temp)) {
    //
    double *c = (lline.HeState_vsDT(Dens, Temp, perr) == 1) ? PrpHeI : PrpHeII;
    double t = lline.HeTemp_vsDT(Dens, Temp, perr), t2 = t * t;
    double x = lline.deltaV_vsD(Dens, perr), f = 1;
    double q[4] = { 0. };
    //
    if (!perr || !perr->code) {
      for (int k = 0; k < 6; k++, f *= x) {
        for (int j = 0; j < 4; j++)
          q[j] += f * c[6 * (j + 1) + k + 5];
      }
      double out = t * (2 * q[0] + t2 * (4 * q[1] + t2 * (6 * q[2] + t2 * 8 * q[3])));
      out = (logFun.dPdT_vsDT(Dens, Temp, perr) + out) * 1e+6;        // to Si units: *1e+6
      val[2][0] = out;
    }
  }
  return val[2][0];
}
// (3) FUNCTION DPDDA (D, TT)
// dP/dD as a function of density and temperature [SI units]
double cryoHe_ArpEquations::dPdD_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(3, Dens, Temp)) {
    //
    double *c = (lline.HeState_vsDT(Dens, Temp, perr) == 1) ? PrpHeI : PrpHeII;
    double t = lline.HeTemp_vsDT(Dens, Temp, perr), t2 = t * t;
    double x = lline.deltaV_vsD(Dens, perr), f = 1;
    double q[5] = { 0. };
    //
    if (!perr || !perr->code) {
      for (int k = 0; k < 5; k++, f *= x) {
        for (int j = 0; j < 5; j++)
          q[j] += f * c[6 * (j + 1) + k] * (k + 1);
      }
      double out = q[0] + t2 * (q[1] + t2 * (q[2] + t2 * (q[3] + t2 * q[4])));
      out = -(1e+9) * (logFun.dPdV_vsDT(Dens, Temp, perr) + out) / Dens / Dens;  // to Si units: *(-1e+9)
      val[3][0] = out;
    }
  }
  return val[3][0];
}
// (4) FUNCTION CVA (D, TT)
// Cv as a function of density and temperature [SI units]
double cryoHe_ArpEquations::Cv_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(4, Dens, Temp)) {
    //
    double *c = (lline.HeState_vsDT(Dens, Temp, perr) == 1) ? PrpHeI : PrpHeII;
    double t = lline.HeTemp_vsDT(Dens, Temp, perr), t2 = t * t;
    double x = lline.deltaV_vsD(Dens, perr), f = x;
    double q[4] = { 0. };
    //
    if (!perr || !perr->code) {
      for (int k = 0; k < 6; k++, f *= x) {
        for (int j = 0; j < 4; j++)
          q[j] += f * c[6 * (j + 1) + k + 5] / (k + 1);
      }
      double out = t * (2 * q[0] + t2 * (12 * q[1] + t2 * (30 * q[2] + t2 * 56 * q[3])))
          + t * (c[35] + t2 * (c[36] + t2 * (c[37] + t2 * c[38])));
      out = (logFun.Cv_vsDT(Dens, Temp, perr) + out) * (1e+3);  // to Si units: *(1e+3)
      val[4][0] = out;
    }
  }
  return val[4][0];
}
// (5) FUNCTION ENTRA (D, TT)
// Entropy as a function of density and temperature [SI units]
double cryoHe_ArpEquations::Entr_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(5, Dens, Temp)) {
    //
    double *c = (lline.HeState_vsDT(Dens, Temp, perr) == 1) ? PrpHeI : PrpHeII;
    double t = lline.HeTemp_vsDT(Dens, Temp, perr), t2 = t * t;
    double x = lline.deltaV_vsD(Dens, perr), f = x;
    double q[4] = { 0. };
    //
    if (!perr || !perr->code) {
      for (int k = 0; k < 6; k++, f *= x) {
        for (int j = 0; j < 4; j++)
          q[j] += f * c[6 * (j + 1) + k + 5] / (k + 1);
      }
      double out = t * (2 * q[0] + t2 * (4 * q[1] + t2 * (6 * q[2] + t2 * 8 * q[3])));
      out += t * (c[35] + t2 * (c[36] / 3 + t2 * (c[37] / 5 + t2 * c[38] / 7))) + c[39];
      out = (logFun.Entr_vsDT(Dens, Temp, perr) + out) * (1e+3);  // to Si units: *(1e+3)
      val[5][0] = out;
    }
  }
  return val[5][0];
}
// (6) FUNCTION HELMA (D, TT)
// Helmholtz energy as a function of density and temperature [SI units]
double cryoHe_ArpEquations::Helm_vsDT(double Dens, double Temp, HepackError_t *perr) {
  if (todo(6, Dens, Temp)) {
    //
    double *c = (lline.HeState_vsDT(Dens, Temp, perr) == 1) ? PrpHeI : PrpHeII;
    double t = lline.HeTemp_vsDT(Dens, Temp, perr), t2 = t * t;
    double x = lline.deltaV_vsD(Dens, perr), f = x;
    double q[5] = { 0. };
    //
    if (!perr || !perr->code) {
      for (int k = 0; k < 6; k++, f *= x) {
        for (int j = 0; j < 5; j++)
          q[j] += f * c[6 * (j + 1) + k - 1] / (k + 1);
      }
      double out = q[0] + t2 * (q[1] + t2 * (q[2] + t2 * (q[3] + t2 * q[4])));
      out += t * (c[39] + t * (c[35] / 2 + t2 * (c[36] / 12 + t2 * (c[37] / 30 + t2 * c[38] / 56)))) + c[40];
      out = (logFun.Helm_vsDT(Dens, Temp, perr) + out) * (-1e+3);  // to Si units: *(-1e+3)
      val[6][0] = out;
    }
  }
  return val[6][0];
}
// (7) SUBROUTINE DF2PT (IDID, D, PP, TT)
// This subroutine iterates for density D [kg/m3], given P [Pa] and T [K].
// It is valid only in the compressed liquid near the lambda line.
double cryoHe_ArpEquations::Dens_vsPT(double Pres, double Temp, HepackError_t *perr) {
  static int error_div0 = 0;
  if (todo(7, Pres, Temp)) {
    //
    double D = fixT.Dens175K_vsP(Pres);
    int i = 40;
    double ir = 0.;
    //
    error_div0 = 0;
    for (double dD = 1.; ((i != 0) && (fabs(dD) >= 0.001)); i--, D += dD) {
      ir = dPdD_vsDT(D, Temp, perr);
      if (ir != 0.) {
        dD = (Pres - Pres_vsDT(D, Temp, perr)) / ir;
      } else {
        HE_ERROR(DIV0)
        error_div0 = 1;
      }
    }
    val[7][0] = D;
  }
  else
    if (error_div0) HE_ERROR(DIV0)
  return val[7][0];
}
//
// Destructor(s)
cryoHe_ArpEquations::~cryoHe_ArpEquations() {
}
//
//  Private Part
//
int cryoHe_ArpEquations::todo(int fNo, double p1) {
  return todo(fNo, p1, -1);
}
int cryoHe_ArpEquations::todo(int fNo, double p1, double p2) {
  val[fNo][1] = p1;
  val[fNo][2] = p2;
  stt[fNo] = 1;
  return 1;
}
