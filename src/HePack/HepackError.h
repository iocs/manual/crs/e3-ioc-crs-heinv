/*
 * HepackError.h
 *
 *  Created on: Dec 10, 2013
 *      Author: hrickens
 */


#ifndef HEPACKERROR_H_
#define HEPACKERROR_H_

#ifdef __cplusplus
extern "C" {
#endif

#define ERR_STR_SIZE 50

#include <stdio.h>

// Hepack Errorstatus
typedef enum {NO_ERROR = 0, DIV0, SQRT_NEGATIV_VALU, MAX_ITERATIONS, GIBB_RANGE, VALUE_OUT, NOT_INITIALIZED, UNKNOWN_ERROR} error_code_t;

extern const char *err_string[];

typedef struct {
  error_code_t code;
  char line[ERR_STR_SIZE];
} HepackError_t;

#define HE_ERROR(err) \
{ \
 if(perr){ \
   perr->code = err; snprintf (perr->line, ERR_STR_SIZE, "%s:%s() Line %d", __FILE__, __func__, __LINE__); \
 } \
}

#ifdef __cplusplus
} // extern C
#endif

#endif /* HEPACKERROR_H_ */
