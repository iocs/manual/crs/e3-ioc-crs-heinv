#include "spline_Smooth.h"

//
//  Class spline_Smooth
//
//  Function Interpolation by 3-Order Special Spline
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 07-Feb-2005
//  Modified: 
//

// Constructor(s)
// ==============
//
//  XYdim -- dimension for (Xi,Yi) function definition
//  Xdata -- array of Xi where i=0..XYdim-1
//  Ydata -- array of Yi where i=0..XYdim-1
//  type  -- the type for the end conditions
//
spline_Smooth::spline_Smooth(int XYdim, const double *Xdata, const double *Ydata, int type)
  : spline(XYdim-1,Xdata[0],Xdata[XYdim-1])
{
  int i, nU = nUnits, nP = XYdim;
  double *DevI = new double[XYdim];
  double dx,sDevII,eDevII,dy1,dy2;
  //
  //  DerivativeI values in internal points for smoothing i=1..XYdim-2
  for (i=1; i<nU; i++) {
    dy2 = Ydata[i+1]-Ydata[i]; dy1 = Ydata[i]-Ydata[i-1];
    DevI[i] = (dy1*dy2 <= 0)?0:2/((Xdata[i]-Xdata[i-1])/dy1 + (Xdata[i+1]-Xdata[i])/dy2);
  }
  //
  //  Definition of the end points DevI[0] & DevI[nP] depending on Spline Type
  switch (type) {
    case 0:     // Natural spline: S''(Xo) = S''(Xn) = 0
      DevI[0]  = (3*(Ydata[1]-Ydata[0])/(Xdata[1]-Xdata[0]) - DevI[1])/2;
      DevI[nU] = (3*(Ydata[nU]-Ydata[nU-1])/(Xdata[nU]-Xdata[nU-1]) - DevI[nU-1])/2;
      break;
    case 1:     // Parabolic runout spline: S''(Xo) = S''(X1) && S''(Xn) = S''(Xn-1)
      DevI[0]  = 2*(Ydata[1]-Ydata[0])/(Xdata[1]-Xdata[0]) - DevI[1];
      DevI[nU] = 2*(Ydata[nU]-Ydata[nU-1])/(Xdata[nU]-Xdata[nU-1]) - DevI[nU-1];
      break;
    default:
      printf("Smooth Spline - Unknown type = %d\n",type);
      exit(1);
  }
  //
  for (i=0; i<nU; i++) {
    dx = Xdata[i+1] - Xdata[i];
    sDevII = -2*(DevI[i+1] + 2*DevI[i])/dx + 6*(Ydata[i+1] - Ydata[i])/dx/dx;
    eDevII =  2*(2*DevI[i+1] + DevI[i])/dx - 6*(Ydata[i+1] - Ydata[i])/dx/dx;
    Unit[i].init(Xdata[i],Xdata[i+1],Ydata[i],DevI[i],sDevII/2,(eDevII - sDevII)/6/dx);
  }
  delete [] DevI;
}

// Destructor
// ==========
spline_Smooth::~spline_Smooth() {}

