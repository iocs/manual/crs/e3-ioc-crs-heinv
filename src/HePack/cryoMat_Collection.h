//
//  Collection of Cryo Material Properties
//
#ifndef _cryoMat_Collection_h
#define _cryoMat_Collection_h

#include "HepackError.h"

//  Must be changed when new data block is included !!!
const int cryoMat_CollectionSize = 61;

//
const int _cryoMat_MaxDefArrSize  = 18;

struct MatCryoDef {
      // Material name
      char *name;
      //
      // Material description
      char *descr;
      //
      // Metal or not
      //    0 - non metal
      //    1 - metal
      int metal;
      //
      //  Init masks
      //     0 - the corresponding properties are unavailable
      //     1 - the value is read in as data
      //    >1 - the value is to be calculated
      //   for maskExpans
      //     1 - marks deltL
      //     2 - marks expansivity (1/L)(dL/dT) data, with deltL to be calculated by integration.
      int maskCp;
      int maskConduct;
      int maskExpans;
      int maskResist;
      //
      //  Parameters
      double density;
      //
      //  Data Dimension (for all arrays below)
      int dataDim;
      //
      //  Temperature steps common to all data.
      double dataTK[_cryoMat_MaxDefArrSize];
      //
      // Cp-data [J/g-K]
      double dataCp[_cryoMat_MaxDefArrSize];
      //
      // Conductivity data [W/cm-K]
      double dataConduct[_cryoMat_MaxDefArrSize];
      //
      // Expansivity data [percent]
      double dataExpans[_cryoMat_MaxDefArrSize];
      //
      // Resistivity data [micro-Ohm-cm]
      double dataResist[_cryoMat_MaxDefArrSize];
};

const static MatCryoDef CryoDefArray[cryoMat_CollectionSize] = 
{
  // 1 element
  {"Copper (specified by RRR and magnetic field, B)",
          // Description
"Transport properties (= conductivity and resistivity) of   \n\
\"pure\" metals are critically dependent on both impurity   \n\
content and mechanical parameters (anneal, cold work, etc.).\n\
For calculations, the sum of these effects is characterized \n\
by the single parameter RRR = residual resistance ratio.    \n\
The user is cautioned that this is a significant approxim-  \n\
ation which limits the calculation accuracy.                \n\
      Magnetic field effects, magnetoresistivity:           \n\
Transport properties of pure metals are also sensitive to   \n\
applied magnetic fields.  However, calculation of the effect\n\
is uncertain except in a few special cases.  The user should\n\
calculate properties both in the applied field and in zero  \n\
field.  The correct property values will probably be some-  \n\
where between these calculated values, depending on sample  \n\
and field orientation.  Available theory and data are simply\n\
inadequate to calculate values for the general case.        \n\
Transport properties are also affected by magnetic fields.  \n\
However, the magnetic field effects for this metal have not \n\
been documented.",
   1,     // Metal
          //
   1,     // maskCp
   2,     // maskConduct
   1,     // maskExpans
   2,     // maskResist
          //
   8.96,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000012,  0.000028,  0.000100,  0.00087,   0.00727,   0.0266,
     0.059,     0.095,     0.135,     0.17,      0.205,     0.23,
     0.251,     0.323,     0.356,     0.374,     0.38,      0.386},
          // Conductivity data [W/cm-K]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000},
          // Expansivity data [percent]
   {-0.29503,  -0.29502,  -0.295,    -0.2945,   -0.294,    -0.293,
    -0.291,    -0.288,    -0.285,    -0.277,    -0.27,     -0.261,
    -0.252,    -0.188,    -0.113,    -0.035,     0.,        0.042},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000}
  },
  //
  // 2 element
  {"Aluminum (specified by RRR and magnetic field, B)",
          // Description
"Transport properties (= conductivity and resistivity) of   \n\
\"pure\" metals are critically dependent on both impurity   \n\
content and mechanical parameters (anneal, cold work, etc.).\n\
For calculations, the sum of these effects is characterized \n\
by the single parameter RRR = residual resistance ratio.    \n\
The user is cautioned that this is a significant approxim-  \n\
ation which limits the calculation accuracy.                \n\
      Magnetic field effects, magnetoresistivity:           \n\
Transport properties of pure metals are also sensitive to   \n\
applied magnetic fields.  However, calculation of the effect\n\
is uncertain except in a few special cases.  The user should\n\
calculate properties both in the applied field and in zero  \n\
field.  The correct property values will probably be some-  \n\
where between these calculated values, depending on sample  \n\
and field orientation.  Available theory and data are simply\n\
inadequate to calculate values for the general case.        \n\
Transport properties are also affected by magnetic fields.  \n\
However, the magnetic field effects for this metal have not \n\
been documented.",
   1,     // Metal
          //
   1,     // maskCp
   2,     // maskConduct
   1,     // maskExpans
   2,     // maskResist
          //
   2.698, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0001,    0.000108,  0.000302,  0.0014,    0.0089,    0.0315,
     0.0775,    0.142,     0.214,     0.287,     0.357,     0.422,
     0.481,     0.684,     0.797,     0.859,     0.88,      0.902   },
          // Conductivity data [W/cm-K]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000},
          // Expansivity data [percent]
   {-0.3643,   -0.3642,   -0.364,    -0.3636,   -0.363,    -0.362,
    -0.36,     -0.355,    -0.35,     -0.343,    -0.335,    -0.326,
    -0.315,    -0.25,     -0.157,    -0.055,     0.,        0.06    },
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000}
  },
  //
  // 3 element
  {"304 Stainless Steel",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   7.9,   // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000464,  0.000931,  0.001976,  0.00502,   0.0126,    0.0293,
     0.0578,    0.100,     0.128,     0.167,     0.197,     0.23,
     0.25,      0.347,     0.419,     0.439,     0.444,     0.477 },
          // Conductivity data [W/cm-K]
   { 0.00042,   0.00103,   0.0024,    0.0077,    0.0195,    0.033,
     0.047,     0.058,     0.068,     0.076,     0.0826,    0.0886,
     0.094,     0.1152,    0.13,      0.1407,    0.145,     0.149 },
          // Expansivity data [percent]
   {-0.26404,  -0.26403,  -0.264,    -0.26392,  -0.2638,   -0.2635,
    -0.263,    -0.262,    -0.259,    -0.256,    -0.248,    -0.238,
    -0.229,    -0.175,    -0.108,    -0.035,     0.,        0.046 },
          // Resistivity data [micro-Ohm-cm]
   {49.001,    49.003,    49.01,     49.03,     49.1,      49.5,
    50.,       50.5,      51.,       52.,       53.,       53.8,
    54.5,      60.,       64.,       68.,       70.,       72. }
  },
  //
  // 4 element
  {"G-10 (fiberglass-epoxy), parallel to warp fibers",
          // Description
"is an epoxy resin impregnated glass fiber cloth.  Most      \n\
properties depend on the angles between the thermal gradient \n\
and the fiber directions.  \"Warp\" refers to the primary    \n\
fibers, and \"fill\" refers to the (smaller) secondary fibers\n\
at right angles.  \"Normal\" is the direction perpendicular  \n\
to the plane of the cloth; many normal properties approach   \n\
those of pure epoxy.  The user is cautioned that properties  \n\
also depend on relative fiber sizes and other factors, and   \n\
that variations from Cryocomp values may be expected.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.948, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 9.86e-6,   6.13e-5,   0.000624,  0.008468,  0.04155,   0.084592,
     0.1263,    0.1695,    0.2125,    0.25175,   0.28775,   0.3205,
     0.35475,   0.51,      0.6475,    0.7685,    0.82,      0.88 },
          // Conductivity data [W/cm-K]
   { 0.000125,  0.00035,   0.00065,   0.0012,    0.0019,    0.00235,
     0.0028,    0.0031,    0.0035,    0.0039,    0.0043,    0.0046,
     0.00489,   0.0061,    0.007,     0.0077,    0.00795,   0.0082 },
          // Expansivity data [percent]
   {-0.22065,  -0.2206,   -0.2205,   -0.2203,   -0.2195,   -0.218,
    -0.213,    -0.207,    -0.200,    -0.196,    -0.191,    -0.185,
    -0.178,    -0.137,    -0.088,    -0.031,     0.,        0.033 },
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 5 element
  {"G-10 (fiberglass-epoxy), parallel to fill fibers",
          // Description
"is an epoxy resin impregnated glass fiber cloth.  Most      \n\
properties depend on the angles between the thermal gradient \n\
and the fiber directions.  \"Warp\" refers to the primary    \n\
fibers, and \"fill\" refers to the (smaller) secondary fibers\n\
at right angles.  \"Normal\" is the direction perpendicular  \n\
to the plane of the cloth; many normal properties approach   \n\
those of pure epoxy.  The user is cautioned that properties  \n\
also depend on relative fiber sizes and other factors, and   \n\
that variations from Cryocomp values may be expected.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.948, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 9.86e-6,   6.13e-5,   0.000624,  0.008468,  0.04155,   0.084592,
     0.1263,    0.1695,    0.2125,    0.25175,   0.28775,   0.3205,
     0.35475,   0.51,      0.6475,    0.7685,    0.82,      0.88 },
          // Conductivity data [W/cm-K]
   { 0.000125,  0.00035,   0.00065,   0.0012,    0.0019,    0.00235,
     0.0028,    0.0031,    0.0035,    0.0039,    0.0043,    0.0046,
     0.00489,   0.0061,    0.007,     0.0077,    0.00795,   0.0082 },
          // Expansivity data [percent]
   {-0.2554,   -0.2553,   -0.255,    -0.254,    -0.2515,   -0.248,
    -0.245,    -0.24,     -0.235,    -0.228,    -0.222,    -0.213,
    -0.205,    -0.16,     -0.101,    -0.037,     0.,        0.04 },
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 6 element
  {"G-10 (fiberglass-epoxy), normal to cloth layer",
          // Description
"is an epoxy resin impregnated glass fiber cloth.  Most      \n\
properties depend on the angles between the thermal gradient \n\
and the fiber directions.  \"Warp\" refers to the primary    \n\
fibers, and \"fill\" refers to the (smaller) secondary fibers\n\
at right angles.  \"Normal\" is the direction perpendicular  \n\
to the plane of the cloth; many normal properties approach   \n\
those of pure epoxy.  The user is cautioned that properties  \n\
also depend on relative fiber sizes and other factors, and   \n\
that variations from Cryocomp values may be expected.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.948, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 9.86e-6,   6.13e-5,   0.000624,  0.008468,  0.04155,   0.084592,
     0.1263,    0.1695,    0.2125,    0.25175,   0.28775,   0.3205,
     0.35475,   0.51,      0.6475,    0.7685,    0.82,      0.88 },
          // Conductivity data [W/cm-K]
   { 0.00007,   0.000225,  0.00055,   0.00105,   0.0016,    0.002,
     0.0024,    0.00265,   0.0029,    0.00325,   0.0035,    0.00375,
     0.004,     0.0049,    0.00555,   0.00595,   0.00612,   0.0063 },
          // Expansivity data [percent]
   {-0.655,    -0.654,    -0.652,    -0.65,     -0.646,    -0.639,
    -0.63,     -0.62,     -0.608,    -0.6,      -0.585,    -0.567,
    -0.545,    -0.425,    -0.28,     -0.103,     0.,        0.11 },
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 7 element
  {"Beryllium",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   1.848, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00025,   0.000051,  0.000118,  0.000389,  0.00161,   0.0045,
     0.00996,   0.0192,    0.0341,    0.0562,    0.0906,    0.139,
     0.199,     0.625,     1.11,      1.56,      1.75,      1.97 },
          // Conductivity data [W/cm-K]
   { 0.023,     0.044,     0.105,     0.27,      0.54,      0.823,
     1.11,      1.4,       1.7,       1.96,      2.2,       2.35,
     2.5,       2.45,      1.9,       1.85,      1.825,     1.8 },
          // Expansivity data [percent]
   {-0.1094,   -0.1094,   -0.1094,   -0.1094,   -0.10939,  -0.10937,
    -0.10932,  -0.10922,  -0.10906,  -0.10876,  -0.10826,  -0.10751,
    -0.10641,  -0.09303,  -0.06516,  -0.02328,   0.0,       0.0297 },
          // Resistivity data [micro-Ohm-cm]
   { 0.00405,   0.00405,   0.00405,   0.00407,   0.00445,   0.005,
     0.0055,    0.0065,    0.008,     0.02,      0.0275,    0.055,
     0.092,     0.365,     1.02,      2.095,     2.7,       3.46 }
  },
  //
  // 8 element
  {"Niobium",
          // Description
"is superconducting below 9.25 K in zero applied magnetic    \n\
field. Cryocomp conductivity data assumes normal state       \n\
below this temperature.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.57,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000060,  0.000175,  0.00045,   0.00205,   0.012,     0.032,
     0.068,     0.099,     0.127,     0.152,     0.173,     0.189,
     0.202,     0.239,     0.254,     0.2625,    0.265,     0.268},
          // Conductivity data [W/cm-K]
   { 0.004,     0.018,     0.1,       0.55,      0.85,      0.86,
     0.77,      0.66,      0.61,      0.58,      0.56,      0.55,
     0.545,     0.545,     0.545,     0.545,     0.545,     0.545},
          // Expansivity data [percent]
   {-0.129,    -0.129,    -0.129,    -0.129,    -0.128,    -0.1268,
    -0.1253,   -0.124,    -0.122,    -0.12,     -0.117,    -0.112,
    -0.106,    -0.08,     -0.048,    -0.017,     0.,        0.017},
          // Resistivity data [micro-Ohm-cm]
   { 0.,        0.,        0.,        0.51,      0.564,     0.65,
     1.18,      1.73,      2.29,      2.84,      3.4,       3.96,
     4.5,       7.3,      10.1,      12.9,      14.1,      15.6}
  },
  //
  // 9 element
  {"Inconel 718, annealed",
          // Description
"specific heat data are not available, so Inconel 706 data   \n\
are presented for use.  The consequent error is unknown but \n\
presumed small. Low temperature properties are sensitive to \n\
heat treatment and cold work.                               \n\
Electrical resistivity data are for an annealed sample.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.193, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000399,  0.000781,  0.001600,  0.0037,    0.01,      0.03,
     0.063,     0.1,       0.13,      0.165,     0.19,      0.215,
     0.24,      0.33,      0.405,     0.425,     0.435,     0.444},
          // Conductivity data [W/cm-K]
   { 0.001073,  0.002326,  0.005325,  0.0148,    0.0295,    0.04,
     0.0465,    0.053,     0.057,     0.061,     0.0645,    0.068,
     0.071,     0.083,     0.092,     0.1,       0.104,     0.11},
          // Expansivity data [percent]
   {-0.21215,  -0.2121,   -0.212,    -0.2118,   -0.2115,   -0.211,
    -0.21,     -0.208,    -0.206,    -0.2015,   -0.197,    -0.191,
    -0.185,    -0.141,    -0.088,    -0.0285,    0.,        0.026},
          // Resistivity data [micro-Ohm-cm]
   {119.05,    119.,      118.8,     118.52,    118.3,     118.22,
    118.19,    118.2,     118.28,    118.4,     118.58,    118.8,
    119.02,    120.5,     121.55,    122.28,    122.5,     122.7}
  },
  //
  // 10 element
  {"Inconel 718, cold worked",
          // Description
"specific heat data are not available, so Inconel 706 data   \n\
are presented for use.  The consequent error is unknown but \n\
presumed small. Low temperature properties are sensitive to \n\
heat treatment and cold work.                               \n\
Electrical resistivity data are for an annealed sample.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.193, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000399,  0.000781,  0.001600,  0.0037,    0.01,      0.03,
     0.063,     0.1,       0.13,      0.165,     0.19,      0.215,
     0.24,      0.33,      0.405,     0.425,     0.435,     0.444},
          // Conductivity data [W/cm-K]
   { 0.000825,  0.001771,  0.004,     0.0105,    0.0223,    0.033,
     0.0435,    0.0507,    0.056,     0.061,     0.0645,    0.068,
     0.071,     0.083,     0.092,     0.1,       0.104,     0.11},
          // Expansivity data [percent]
   {-0.21215,  -0.2121,   -0.212,    -0.2118,   -0.2115,   -0.211,
    -0.21,     -0.208,    -0.206,    -0.2015,   -0.197,    -0.191,
    -0.185,    -0.141,    -0.088,    -0.0285,    0.,        0.026},
          // Resistivity data [micro-Ohm-cm]
   {119.05,    119.,      118.8,     118.52,    118.3,     118.22,
    118.19,    118.2,     118.28,    118.4,     118.58,    118.8,
    119.02,    120.5,     121.55,    122.28,    122.5,     122.7}
  },
  //
  // 11 element
  {"Titanium",
          // Description
"should not be used in a pure oxygen environment as impact   \n\
ignition and combustion is a hazard. This data is for a RRR \n\
of about 22, commercially pure.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   4.54,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00007,   0.000122,  0.00032,   0.00125,   0.007,     0.0245,
     0.0571,    0.1,       0.1467,    0.189,     0.23,      0.267,
     0.3,       0.406,     0.465,     0.498,     0.511,     0.522},
          // Conductivity data [W/cm-K]
   { 0.004,     0.025,     0.0535,    0.127,     0.263,     0.34,
     0.36,      0.345,     0.335,     0.325,     0.315,     0.308,
     0.3,       0.27,      0.25,      0.23,      0.215,     0.2},
          // Expansivity data [percent]
   {-0.13407,  -0.13406,  -0.13404,  -0.134,    -0.13392,  -0.1338,
    -0.1335,   -0.133,    -0.132,    -0.13,     -0.1266,   -0.1225,
    -0.118,    -0.09,     -0.059,    -0.02,      0.,        0.025},
          // Resistivity data [micro-Ohm-cm]
   {  0.,        0.,        2.02,      2.1,       2.2,       2.5,
      3.1,       3.85,      5.,        6.2,       7.3,       8.8,
      9.75,     17.,       28.,       36.,       43.,       48.}
  },
  //
  // 12 element
  {"Epoxy",
          // Description
   "",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.15,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00003,   0.00017,   0.0018,    0.0213,    0.084,     0.15,
     0.21,      0.27,      0.328,     0.38,      0.44,      0.49,
     0.54,      0.78,      1.0,       1.19,      1.27,      1.36},
          // Conductivity data [W/cm-K]
   { 0.00016,   0.000347,  0.00048,   0.000562,  0.0007,    0.0008,
     0.0009,    0.001,     0.001084,  0.00117,   0.00125,   0.00135,
     0.00148,   0.00186,   0.002,     0.00211,   0.00214,   0.00216},
          // Expansivity data [percent]
   {-1.153,    -1.152,    -1.15,     -1.145,    -1.132,    -1.12,
    -1.1,      -1.075,    -1.06,     -1.03,     -1.01,     -0.98,
    -0.95,     -0.76,     -0.55,     -0.243,     0.,        0.3},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 13 element
  {"YBCO",
          // Description
"atomic weight = 88.9 and the molecular weight = 666.26.     \n\
Thermal properties depend on the method of manufacture, heat\n\
treatment, and storage environment. Data presented are for  \n\
bulk polycrystalline, calcined, HIP, and oxygen annealed    \n\
samples. Data must be obtained for the exact manufacturing  \n\
process in critical applications.",      
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   6.38,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 8.99e-06,  0.0000118, 0.0000506, 0.00045,   0.00562,   0.018,
     0.033,     0.0478,    0.0625,    0.0787,    0.0928,    0.1049,
     0.1184,    0.1709,    0.208,     0.2329,    0.2391,    0.2443},
          // Conductivity data [W/cm-K]
   { 0.00103,   0.0047,    0.009,     0.023,     0.057,     0.076,
     0.083,     0.08,      0.073,     0.064,     0.055,     0.05,
     0.051,     0.047,     0.0455,    0.0455,    0.0463,    0.047},
          // Expansivity data [percent]
   {-0.17548,  -0.17545,  -0.1754,   -0.1753,   -0.175,    -0.174,
    -0.172,    -0.169,    -0.166,    -0.162,    -0.158,    -0.154,
    -0.15,     -0.115,    -0.074,    -0.025,     0.,        0.025},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  1200., 
     2250.,     3400.,     4500.,     5600.,     6428.,     7400.}
  },
  //
  // 14 element
  {"Pb-Sn soft solder",
          // Description
"conductivity data are for a 56-44 (Pb-Sn) solder, while     \n\
the other properties data are for a 60-40 solder.           \n\
The superconducting transition temperature is 7.05 K.       \n\
The critical field is 832 Gauss. Normal state resistivity is\n\
given, measured in fields of approximately 0.105 Tesla.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   9.51,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000007,  0.000063,  0.000531,  0.0118,    0.0479,    0.0768,
     0.0996,    0.116,     0.128,     0.137,     0.144,     0.149,
     0.152,     0.164,     0.17,      0.175,     0.177,     0.179},
          // Conductivity data [W/cm-K]
   { 0.0221,    0.053,     0.135,     0.2,       0.285,     0.333,
     0.38,      0.407,     0.43,      0.44,      0.445,     0.447,
     0.45,      0.46,      0.48,      0.497,     0.5,       0.51},
          // Expansivity data [percent]
   {-0.4634,   -0.4633,   -0.463,    -0.462,    -0.461,    -0.46,
    -0.459,    -0.453,    -0.447,    -0.4365,   -0.426,    -0.411,
    -0.396,    -0.292,    -0.178,    -0.057,     0.,        0.051},
          // Resistivity data [micro-Ohm-cm]
   { 0.35001,   0.35002,   0.35003,   0.4,       0.52,      0.7,
     1.16,      1.68,      2.21,      2.73,      3.26,      3.78,
     4.31,      6.93,      9.55,     12.18,     13.38,     14.8}
  },
  //
  // 15 element
  {"Polyamide (Nylon 6 ), PA6",
          // Description
"is extensively modified, by thermal treatment for example,  \n\
to obtain a wide range of properties. Care should be        \n\
exercised in developing material specifications.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.14,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000018,  0.000144,  0.00133,   0.016,     0.093,     0.197,
     0.3,       0.41,      0.5,       0.587,     0.68,      0.75,
     0.82,      1.141,     1.451,     1.75,      1.87,      2.0},
          // Conductivity data [W/cm-K]
   { 0.0000185, 0.000037,  0.0001,    0.00031,   0.0007,    0.0011,
     0.0015,    0.00174,   0.0019,    0.0021,    0.0022,    0.00234,
     0.0024,    0.0027,    0.00285,   0.003,     0.00305,   0.0031},
          // Expansivity data [percent]
   {-1.238,    -1.233,    -1.228,    -1.223,    -1.218,    -1.2045,
    -1.191,    -1.169,    -1.147,    -1.1165,   -1.086,    -1.0485,
    -1.011,    -0.785,    -0.512,    -0.177,     0.0,       0.222},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 16 element
  {"Pyrex Glass",
          // Description
   "",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   2.214,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000003,  0.000025,  0.000232,  0.00419,   0.0274,    0.06279,
     0.0984,    0.136,     0.174,     0.209,     0.237,     0.264,
     0.272,     0.435,     0.539,     0.628,     0.697,     0.737},
          // Conductivity data [W/cm-K]
   { 0.000165,  0.000435,  0.00093,   0.00121,   0.00146,   0.0019,
     0.00237,   0.0028,    0.0034,    0.0038,    0.00443,   0.00495,
     0.00545,   0.00756,   0.009,     0.0098,    0.01,      0.0102},
          // Expansivity data [percent]
   {-0.0485,   -0.0485,   -0.0485,   -0.049,    -0.0495,   -0.05,
    -0.0505,   -0.0493,   -0.05,     -0.0488,   -0.0475,   -0.0458,
    -0.044,    -0.0332,   -0.021,    -0.00675,   0.,        0.0085},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 17 element
  {"Silver",
          // Description
   "resistivity and conductivity are for RRR = 30, H=0 Tesla.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   10.47,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0000072, 0.0000239, 0.0001435, 0.0018,    0.0155,    0.0442,
     0.078,     0.108,     0.133,     0.151,     0.166,     0.177,
     0.187,     0.213,     0.225,     0.2325,    0.23413,   0.236},
          // Conductivity data [W/cm-K]
   { 0.67,      1.33,      2.75,      6.05,      8.5,       7.8,
     6.1,       5.05,      4.7,       4.43,      4.35,      4.3,
     4.27,      4.21,      4.21,      4.21,      4.21,      4.21},
          // Expansivity data [percent]
   {-0.37355,  -0.3735,   -0.3734,   -0.3732,   -0.3727,   -0.3697, 
    -0.3642,   -0.3587,   -0.3492,   -0.3382,   -0.3252,   -0.3117,
    -0.2987,   -0.2212,   -0.1352,   -0.0427,    0.,        0.0518},
          // Resistivity data [micro-Ohm-cm]
   { 0.04975,   0.04975,   0.04975,   0.04975,   0.0496,    0.0594,
     0.0825,    0.125,     0.1863,    0.2477,    0.309,     0.3703,
     0.4316,    0.7383,    1.0449,    1.3515,    1.4926,    1.6582}
//   { 0.0002,    0.000201,  0.000203,  0.00021,   0.0042,    0.02,
//     0.0532,    0.115,     0.1767,    0.2385,    0.3003,    0.3621,
//     0.4239,    0.7327,    1.0416,    1.3505,    1.4926,    1.59}
  },
  //
  // 18 element
  {"Gold",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   19.224,// Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000005,  0.000025,  0.000178,  0.002232,  0.0159,    0.0371,
     0.0572,    0.0726,    0.0842,    0.0928,    0.0992,    0.1043,
     0.1083,    0.1186,    0.1235,    0.1262,    0.12735,   0.1285},
          // Conductivity data [W/cm-K] (RRR=71 approx)
   { 0.36,      0.7,       1.25,      3.21,      4.3,       4.1,
     3.6,       3.3,       3.17,      3.1,       3.1,       3.07,
     3.08,      3.12,      3.12,      3.12,      3.12,      3.12},
          // Conductivity data [W/cm-K] (RRR=27 approx)
//   { 0.36,      0.7,       1.25,      3.21,      4.3,       4.1,
//     3.6,       3.3,       3.17,      3.1,       3.1,       3.07,
//     3.08,      3.12,      3.12,      3.12,      3.12,      3.12},
          // Expansivity data [percent]
   {-0.29655,  -0.2965,   -0.2964,   -0.2962,   -0.2952,   -0.2912,
    -0.2852,   -0.2782,   -0.2692,   -0.2602,   -0.2502,   -0.2392,
    -0.2282,   -0.1672,   -0.1012,   -0.0329,    0.,        0.038},
          // Resistivity data [micro-Ohm-cm]  (RRR=101 approx)
   { 0.02032,   0.02033,   0.02035,   0.0204,    0.024,     0.05,
     0.12,      0.25,      0.4,       0.59,      0.652,     0.734,
     0.817,     1.22,      1.642,     2.056,     2.246,     2.4645}
  },
  //
  // 19 element
  {"Quartz (Single Crystal [100])",
          // Description
   "",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   0,     // maskExpans
   0,     // maskResist
          //
   2.64,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 2.93e-7,   3.73e-6,   5.69e-5,   0.001465,  0.013849,  0.037641,
     0.064061,  0.090857,  0.120166,  0.153244,  0.188415,  0.218980,
     0.249545,  0.405301,  0.548497,  0.657359,  0.707184,  0.741099},
          // Conductivity data [W/cm-K]
   { 0.2,       1.23,      6.4,      18.5,       7.7,       2.8,
     1.5,       1.,        0.78,      0.63,      0.53,      0.45,
     0.37,      0.2,       0.13,      0.095,     0.085,     0.075},
          // Expansivity data [percent]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 },
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 20 element
  {"Sapphire, polycrystalline",
          // Description
   "",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   3.89,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 9.3e-9,    7.4e-8,    0.000004,  0.000083,  0.000694,  0.002577,
     0.006775,  0.01463,   0.02727,   0.04494,   0.06763,   0.09508,
     0.126,     0.3138,    0.5018,    0.657,     0.7114,    0.7788},
          // Conductivity data [W/cm-K]
   { 0.027,     0.172,     1.25,      9.,       34.3,      56.,
    58.,       48.,       28.,       14.,        9.,        6.3, 
     4.3,       1.4,       0.75,      0.5,       0.48,      0.45},  
          // Expansivity data [percent]
   {-0.058091, -0.05809,  -0.058088, -0.058085, -0.05808,  -0.05807,
    -0.05805,  -0.058,    -0.0578,   -0.0575,   -0.057,    -0.0555,
    -0.054,    -0.042,    -0.0263,   -0.009,     0.,        0.012},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 21 element
  {"6061-T6 Aluminum",
          // Description
"specific heat values are for pure Aluminum. The consequent  \n\
errors are unknown but presumed small.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   2.7126,// Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000051,  0.000108,  0.000302,  0.0014,    0.0089,    0.0315,
     0.0775,    0.142,     0.214,     0.287,     0.357,     0.422,
     0.481,     0.684,     0.797,     0.859,     0.88,      0.902},
          // Conductivity data [W/cm-K]
   { 0.022,     0.047,     0.1,       0.238,     0.501,     0.71,
     0.885,     1.,        1.11,      1.13,      1.18,      1.2,
     1.21,      1.3,       1.35,      1.5,       1.55,      1.6},
          // Expansivity data [percent]
   {-0.374004, -0.374003, -0.374002, -0.374001, -0.374,    -0.373,
    -0.371,    -0.3678,   -0.363,    -0.3564,   -0.348,    -0.3373,
    -0.325,    -0.2504,   -0.157,    -0.0511,    0.,        0.0608},
          // Resistivity data [micro-Ohm-cm]
   { 1.38,      1.3803,    1.381,     1.3825,    1.385,     1.395,
     1.405,     1.44,      1.51,      1.595,     1.705,     1.821,
     1.937,     2.516,     3.095,     3.674,     3.94,      4.253}
  },
  //
  // 22 element
  {"Indium",
          // Description
"critical temperature=3.4; critical field=276G. Conductivity \n\
and resistivity are for 3N4 purity. Properties for supercon-\n\
ducting state excepting expansion.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   7.33,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000019,  0.000141,  0.0011,    0.0155,    0.0608,    0.108,
     0.141,     0.162,     0.176,     0.186,     0.193,     0.198,
     0.203,     0.2185,    0.225,     0.2295,    0.2313,    0.233},
          // Conductivity data [W/cm-K]
   { 0.2,       3.9,       8.5,       4.3,       1.75,      1.28,
     0.95,      0.74,      0.62,      0.54,      0.5,       0.48,
     0.465,     0.44,      0.423,     0.42,      0.417,     0.414},
          // Expansivity data [percent]
   {-0.6433,   -0.6432,   -0.643,    -0.642,    -0.638,    -0.628,
    -0.613,    -0.595,    -0.575,    -0.554,    -0.532,    -0.509,
    -0.486,    -0.358,    -0.219,    -0.0715,    0.,        0.085},
          // Resistivity data [micro-Ohm-cm]
   { 0.0,       0.0,       0.00209,   0.0234,    0.1716,    0.4185,
     0.6947,    0.982,     1.25,      1.53,      1.82,      2.16,
     2.511,     4.19,      5.859,     7.58,      8.37,      9.27}
  },
  //
  // 23 element
  {"Teflon (Polytetrafluoroethylene, PTFE)",
          // Description
   "",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   2.2,   // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00004,   0.00032,   0.002963,  0.018,     0.076,     0.125,
     0.165,     0.202,     0.238,     0.274,     0.312,     0.35,
     0.386,     0.55,      0.741,     0.86,      0.95,      1.01},
          // Conductivity data [W/cm-K]
   { 0.000032,  0.00014,   0.00046,   0.00096,   0.00141,   0.00174,
     0.00193,   0.00208,   0.00219,   0.00228,   0.00235,   0.00241,
     0.00245,   0.00256,   0.00259,   0.0026,    0.0026,    0.0026},
          // Expansivity data [percent]
   {-1.5063,   -1.506,    -1.505,    -1.4925,   -1.475,    -1.45,
    -1.425,    -1.394,    -1.365,    -1.325,    -1.295,    -1.255,
    -1.215,    -0.965,    -0.605,    -0.155,     0.,        0.095},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 24 element
  {"Invar-36",
          // Description
", a controlled expansion alloy, has 36 & 64 %w Ni & Fe. The \n\
material expansion is controlled by internal magnetic       \n\
transitions which affect lattice spacing.  The thermal      \n\
conductivity at 1 and 2K are extrapolated using standard    \n\
principals of physics since no state transformations are    \n\
known to occur at that low temperature.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.118, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000251,  0.000481,  0.00098,   0.00322,   0.01213,   0.035,
     0.08,      0.13,      0.176,     0.211,     0.25,      0.272,
     0.297,     0.377,     0.4186,    0.445,     0.452,     0.463},
          // Conductivity data [W/cm-K]
   { 0.000198,  0.000605,  0.002,     0.0074,    0.018,     0.029,
     0.0385,    0.049,     0.058,     0.066,     0.074,     0.08,
     0.085,     0.103,     0.117,     0.13,      0.133,     0.137},
          // Expansivity data [percent]
   {-0.034,    -0.034,    -0.034,    -0.034,    -0.034,    -0.034,
    -0.034,    -0.034,    -0.0333,   -0.03265,  -0.03185,  -0.0305,
    -0.029,    -0.02043,  -0.0122,   -0.004,     0.,        0.004},
          // Resistivity data [micro-Ohm-cm]
   { 50.,       50.03,     50.1,      50.2,      50.5,      50.8,
     51.,       52.,       52.3,      52.5,      54.,       55.,
     56.5,      64.,       69.,       76.,       79.,       83.}
  },
  //
  // 25 element
  {"Polyethylene, parallel to sheet",
          // Description
   "expansivity values are PARALLEL to the sheet.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   0.918, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000025,  0.0002,    0.00195,   0.0245,    0.1,       0.18,
     0.26,      0.35,      0.4,       0.49,      0.56,      0.625,
     0.68,      0.92,      1.2,       1.5,       1.6,       1.7},
          // Conductivity data [W/cm-K]
   { 0.00002,   0.000049,  0.00012,   0.00042,   0.0013,    0.0018,
     0.00225,   0.00258,   0.00278,   0.00288,   0.003,     0.00308,
     0.00312,   0.00327,   0.0033,    0.00331,   0.00332,   0.00334},
          // Expansivity data [percent]
   {-1.4871,   -1.487,    -1.4865,   -1.486,    -1.485,    -1.48,
    -1.47,     -1.44,     -1.42,     -1.4,      -1.38,     -1.34,
    -1.31,     -1.07,     -0.72,     -0.24,      0.,        0.26},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 26 element
  {"Polyethylene, normal to sheet",
          // Description
   "expansivity values are NORMAL to the sheet.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   0.918, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000025,  0.0002,    0.00195,   0.0245,    0.1,       0.18,
     0.26,      0.35,      0.4,       0.49,      0.56,      0.625,
     0.68,      0.92,      1.2,       1.5,       1.6,       1.7},
          // Conductivity data [W/cm-K]
   { 0.00002,   0.000049,  0.00012,   0.00042,   0.0013,    0.0018,
     0.00225,   0.00258,   0.00278,   0.00288,   0.003,     0.00308,
     0.00312,   0.00327,   0.0033,    0.00331,   0.00332,   0.00334},
          // Expansivity data [percent]
   {-1.028,    -1.026,    -1.024,    -1.022,    -1.02,     -1.01,
    -1.005,    -1.,       -0.99,     -0.98,     -0.975,    -0.96,
    -0.93,     -0.73,     -0.47,     -0.15,      0.,        0.15},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 27 element
  {"Quartz glass",
          // Description
   "",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   2.28,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00000175,0.0000147, 0.000179,  0.004186,  0.02512,   0.05442,
     0.08581,   0.1139,    0.1457,    0.1783,    0.2118,    0.2407,
     0.269,     0.415,     0.553,     0.661,     0.704,     0.743},
          // Conductivity data [W/cm-K]
   { 0.00019,   0.00047,   0.001,     0.0013,    0.0015,    0.00195,
     0.0025,    0.003,     0.0035,    0.004,     0.0045,    0.005,
     0.0057,    0.0076,    0.009,     0.0097,    0.0103,    0.01045},
          // Expansivity data [percent]
   { 0.010111,  0.0099514, 0.0096115, 0.0087415, 0.0072915, 0.0059415,
     0.0046915, 0.0035415, 0.0025415, 0.0016915, 0.0009415, 0.0002865,
    -0.0002835,-0.0020335,-0.0021335,-0.0009085, 0.0,       0.001242},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 28 element
  {"Manganin (84%Cu 12%Mn 4%Ni weight basis)",
          // Description
"specific heat data are sparse but the rule of mixtures      \n\
shows good agreement with the existing data.                \n\
Error may exist in the specific heat values due to the      \n\
sparseness of the data and its computation from the rule of \n\
mixtures.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.5,   // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00009,   0.00015,   0.000256,  0.001117,  0.007418,  0.025772, 
     0.057084,  0.092968,  0.133,     0.1714,    0.2048,    0.23388,
     0.24712,   0.32812,   0.36476,   0.38528,   0.3924,    0.39964},
          // Conductivity data [W/cm-K]
   { 0.00073,   0.0018,    0.0052,    0.017,     0.041,     0.066,
     0.086,     0.101,     0.112,     0.12,      0.129,     0.135,
     0.14,      0.157,     0.172,     0.195,     0.2075,    0.22},
          // Expansivity data [percent]
   {-0.35719,  -0.35719,  -0.35718,  -0.35717,  -0.35697,  -0.35619,
    -0.35424,  -0.35071,  -0.34540,  -0.33824,  -0.32939,  -0.31908,
    -0.3075,   -0.235,    -0.118,    -0.049,     0.0,       0.058},
          // Resistivity data [micro-Ohm-cm]
   {41.3,       41.4,      41.6,      41.9,      42.5,      42.8,
    43.2,       43.65,     44.,       44.25,     44.6,      44.8,
    45.05,      46.1,      46.9,      47.3,      47.4,      47.55}
  },
  //
  // 29 element
  {"Beryllium copper (2 % Be)",
          // Description
"is Cu with 2% Be.  A commercial alloy.  The specific heat is\n\
calculated by the rule of mixtures and may be slightly in   \n\
error below 20K.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.25,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00001226,0.00002846,0.00009938,0.00084078,0.007156,  0.026158,
     0.058019,  0.093484,  0.13298,   0.17066,   0.2027,    0.2301,
     0.2441,    0.329,     0.3711,    0.3977,    0.4074,    0.4177},
          // Conductivity data [W/cm-K]
   { 0.004,     0.0085,    0.02,      0.051,     0.103,     0.15,
     0.2,       0.24,      0.28,      0.325,     0.36,      0.4,
     0.445,     0.65,      0.795,     0.95,      1.0,       1.12},
          // Expansivity data [percent]
   {-0.2853,   -0.2852,   -0.285,    -0.284,    -0.283,    -0.281,
    -0.28,     -0.278,    -0.275,    -0.2705,   -0.262,    -0.257,
    -0.247,    -0.185,    -0.117,    -0.037,     0.0,       0.045},
          // Resistivity data [micro-Ohm-cm]
   { 5.52,      5.53,      5.57,      5.62,      5.7,       5.78,
     5.83,      5.89,      5.95,      6.01,      6.06,      6.13,
     6.305,     6.7,       7.2,       7.8,       8.1,       8.3}
  },
  //
  // 30 element
  {"Titanium alloy (Ti-6Al-4V)",
          // Description
"can be superconducting at low temperature depending upon the\n\
material condition. Properties are given for the normal     \n\
state with the exception of resistivity. Only small domains \n\
of the material undergo the superconducting transition so   \n\
the approximation of normal state properties should be quite\n\
useful.  Cp and k below 20K and 4.2K respectively are values\n\
extrapolated using established techniques. Do not use in a  \n\
pure liq./gas oxygen environment-impact combustion hazard.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   4.54,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000001025, 0.0000082026, 0.00007596, 0.001025, 0.0082026, 0.027035,
     0.058338,    0.099477,     0.144089,   0.187529, 0.2287,    0.26675,
     0.3009,      0.4143,       0.4779,     0.5135,   0.5261,    0.5386},
          // Conductivity data [W/cm-K]
   { 0.00124,   0.002235,  0.0042,    0.0087,    0.015,     0.019,
     0.023,     0.026,     0.029,     0.032,     0.0346,    0.037,
     0.0398,    0.05,      0.059,     0.067,     0.071,     0.077},
          // Expansivity data [percent]
   {-0.15293,  -0.15292,  -0.1529,   -0.1527,   -0.1525,   -0.1515,
    -0.1505,   -0.1495,   -0.148,    -0.145,    -0.142,    -0.137,
    -0.132,    -0.1,      -0.062,    -0.011,     0.0,       0.026},
          // Resistivity data [micro-Ohm-cm]
   {   0.,        0.,        0.,      146.8,     147.,      147.3,
     147.95,    148.4,     149.,      149.7,     150.6,     151.5,
     152.4,     156.84,    161.27,    165.7,     167.7,     170.13}
  },
  //
  // 31 element
  {"Phosphor bronze (A alloy, 94Cu, 5Sn, .2P, ...)",
          // Description
"commercial \"A\", 94%Cu, 4.9%Sn, .2%P, <.3% Zn, Fe, Pb (%w),  \n\
specific heat data is sparse but the present data shows good\n\
agreement with the existing data. C51000 Cu alloy. Compare  \n\
with data presented in MCIC-HB-04, Sect. 5.5.1 and NIST     \n\
Monograph 177 by N. J. Simon, E. S. Drexler and R. P. Reed. ",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.86,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0000122, 0.0000289, 0.000109,  0.001207,  0.008914,  0.029144,
     0.061477,  0.096925,  0.135849,  0.172655,  0.203607,  0.22971,
     0.25,      0.3174,    0.349146,  0.3665,    0.37233,   0.378058},
          // Conductivity data [W/cm-K]
   { 0.002,     0.0067,    0.0155,    0.0523,    0.096,     0.13,
     0.16,      0.188,    -0.213,     0.239,     0.262,     0.285,
     0.31,      0.416,     0.507,     0.6,       0.63,      0.66},
          // Expansivity data [percent]
   {-0.297,    -0.297,    -0.297,    -0.297,    -0.297,    -0.296,
    -0.294,    -0.291,    -0.287,    -0.281,    -0.273,    -0.263,
    -0.252,    -0.187,    -0.115,    -0.036,     0.0,       0.042},
          // Resistivity data [micro-Ohm-cm]
   { 8.592,     8.591,     8.59,      8.585,     8.58,      8.59,
     8.62,      8.671,     8.752,     8.833,     8.914,     8.995,
     9.076,     9.481,     9.886,    10.291,    10.477,    10.696}
  },
  //
  // 32 element
  {"Nickel, 4N5",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.8 ,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00012,   0.000242,  0.000534,  0.00162,   0.0058,    0.0167,
     0.0381,    0.0682,    0.103,     0.139,     0.173,     0.204,
     0.232,     0.328,     0.383,     0.416,     0.429,     0.445},
          // Conductivity data [W/cm-K]
   { 0.25,      0.5,       1.05,      2.5,       4.48,      4.62,
     3.84,      3.08,      2.51,      2.16,      1.93,      1.77,
     1.56,      1.15,      0.99,      0.92,      0.908,     0.9},
          // Expansivity data [percent]
   {-0.199,    -0.199,    -0.199,    -0.199,    -0.199,    -0.198,
    -0.197,    -0.196,    -0.192,    -0.188,    -0.185,    -0.18,
    -0.176,    -0.135,    -0.084,    -0.025,     0.0,       0.0365},
          // Resistivity data [micro-Ohm-cm]
   { 0.0626,    0.0626,    0.0626,    0.0626,    0.0716,    0.09,
     0.126,     0.213,     0.32,      0.47,      0.63,      0.9109,
     1.182,     2.656,     4.14,      5.581,     6.229,     6.976}
  },
  //
  // 33 element
  {"Lead",
          // Description
"has a transition temperature of 7.23K and a critical field  \n\
of 803G. Superconducting properties are presented. The sharp\n\
transition in thermal conductivity at 7.23K is rounded by   \n\
interpolation but that variation is less than that caused by\n\
the cold work and contamination levels typical of Pb. Leads \n\
superconducting thermal expansion coefficient is about 5%   \n\
less than normal Pb but is too small to be seen in the      \n\
contraction integral. RRR=428. Purity about 4N5.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   11.32, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]  (RRR=860)
   { 0.000012,  0.00009,   0.00083,   0.0137,    0.0531,    0.0796,
     0.0944,    0.103,     0.108,     0.112,     0.114,     0.116,
     0.118,     0.122,     0.125,     0.1275,    0.1287,    0.13},
          // Conductivity data [W/cm-K]  (RRR=860)
   { 0.21,      2.75,      3.3,       1.65,      0.58,      0.46,
     0.43,      0.415,     0.405,     0.402,     0.39,      0.375,
     0.365,     0.3625,    0.36,      0.355,     0.357,     0.35},
          // Expansivity data [percent]  (RRR=860)
   {-0.6509,   -0.65089,  -0.65087,  -0.64973,  -0.64186,  -0.62761,
    -0.60936,  -0.58851,  -0.56616,  -0.54286,  -0.51896,  -0.49446,
    -0.46941,  -0.33941,  -0.20441,  -0.06566,   0.0,       0.078841},
          // Resistivity data [micro-Ohm-cm]  (RRR=860)
   { 0.0,       0.0,       0.0,       0.1,       0.56,       1.3,
     2.05,      2.78,      3.5,       4.24,      4.95,       5.67,
     6.4,      10.0,      13.62,     17.54,     19.32,      21.4}
          // Cp-data [J/g-K]  (RRR=428)
// { 0.000026,  0.00012,   0.00083,   0.0137,    0.0531,    0.0796,
//   0.0944,    0.103,     0.108,     0.112,     0.114,     0.116,
//   0.118,     0.122,     0.125,     0.1275,    0.1287,    0.13},
          // Conductivity data [W/cm-K]  (RRR=428)
// { 0.48,      0.96,      1.64,      1.65,      0.58,      0.46,
//   0.43,      0.415,     0.405,     0.402,     0.39,      0.375,
//   0.365,     0.3625,    0.36,      0.355,     0.357,     0.35},
          // Expansivity data [percent]  (RRR=428)
// {-0.6509,   -0.65089,  -0.65087,  -0.64973,  -0.64186,  -0.62761,
//  -0.60936,  -0.58851,  -0.56616,  -0.54286,  -0.51896,  -0.49446,
//  -0.46941,  -0.33941,  -0.20441,  -0.06566,   0.0,       0.078841},
          // Resistivity data [micro-Ohm-cm]  (RRR=428)
// { 0.05,      0.05,      0.05,      0.1,       0.56,       1.3,
//   2.05,      2.78,      3.5,       4.24,      4.95,       5.67,
//   6.4,      10.0,      13.62,     17.54,     19.32,      21.4}
  },
  //
  // 34 element
  {"Carbon Fiber Reinforced Plastic, CRFP parallel (T300)",
          // Description
"properties and the production of CFRP are not well          \n\
controlled at the present time except by specific           \n\
manufacturers.  Design data should be obtained for a given  \n\
application, but the available data for T300 fiber with     \n\
epoxy are given for engineering estimations.  Note that a   \n\
range of fiber volume fractions are identified due to the   \n\
variation in available data. Effective resistivity for the  \n\
composite (carbon plus epoxy) is given parallel to the      \n\
fibers and it is a strong function of the production techni-\n\
que of the fiber and composite. It is related to modulus.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   1.774, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0000184, 0.00024,   0.0018,    0.01,      0.03,      0.0506,
     0.0735,    0.105,     0.14,      0.173,     0.211,     0.239,
     0.274,     0.436,     0.592,     0.777,     0.8536,    0.98},
          // Conductivity data [W/cm-K]
   { 0.00018,   0.0002,    0.0003,    0.0005,    0.001,     0.0017,
     0.00265,   0.0038,    0.005,     0.007,     0.009,     0.0125,
     0.0143,    0.0315,    0.043,     0.05,      0.0503,    0.0505},
          // Expansivity data [percent]
   { 0.0397,    0.03965,   0.0395,    0.0392,    0.0387,    0.0382,
     0.0372,    0.0364,    0.035,     0.0331,    0.0315,    0.0296,
     0.0281,    0.0199,    0.0118,    0.0037,    0.0,      -0.0044},
          // Resistivity data [micro-Ohm-cm]
   { 1997.0,    1995.0,    1989.0,    1974.0,    1949.0,    1923.0,
     1897.0,    1872.0,    1846.0,    1820.0,    1794.0,    1769.0,
     1743.0,    1614.0,    1486.0,    1358.0,    1299.0,    1229.0}
  },
  //
  // 35 element
  {"Carbon Reinforced Plastic, CRFP normal to fiber (T300)",
          // Description
"properties and the production of CFRP are not well          \n\
controlled at the present time except by specific           \n\
manufacturers.  Design data should be obtained for a given  \n\
application, but the available data for T300 fiber with     \n\
epoxy are given for engineering estimations.  Note that a   \n\
range of fiber volume fractions are identified due to the   \n\
variation in available data. Effective resistivity for the  \n\
composite (carbon plus epoxy) is given parallel to the      \n\
fibers and it is a strong function of the production techni-\n\
que of the fiber and composite. It is related to modulus.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.774, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0000184, 0.00024,   0.0018,    0.01,      0.03,      0.0506,
     0.0735,    0.105,     0.14,      0.173,     0.211,     0.239,
     0.274,     0.436,     0.592,     0.777,     0.8536,    0.98},
          // Conductivity data [W/cm-K]
   { 0.00005,   0.00009,   0.00015,   0.0003,    0.00062,   0.0009,
     0.00115,   0.0014,    0.00175,   0.002,     0.0024,    0.00295,
     0.00315,   0.005,     0.0059,    0.006,     0.0063,    0.0065},
          // Expansivity data [percent]
   {-0.8425,   -0.8425,   -0.8425,   -0.8425,   -0.8425,   -0.8375,
    -0.8225,   -0.8025,   -0.7825,   -0.7525,   -0.7225,   -0.7025,
    -0.6705,   -0.4765,   -0.2825,    -0.0895,   0.0,       0.105},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 36 element
  {"Copper Nickel, 90-10",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.94,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0001256, 0.0002425, 0.000546,  0.001884,  0.007435,  0.0267,
     0.059,     0.095,     0.135,     0.173,     0.205,     0.232,
     0.245,     0.323,     0.356,     0.374,     0.38,      0.386},
          // Conductivity data [W/cm-K]
   { 0.0028,    0.00565,   0.0121,    0.056,     0.161,     0.255,
     0.316,     0.358,     0.375,     0.38,      0.37,      0.36,
     0.355,     0.345,     0.355,     0.365,     0.37,      0.375},
          // Expansivity data [percent]
   {-0.2575,   -0.2575,   -0.2575,   -0.2575,   -0.257,    -0.255,
    -0.254,    -0.251,    -0.2475,   -0.2525,   -0.237,    -0.23,
    -0.225,    -0.17,     -0.107,    -0.035,     0.0,       0.04},
          // Resistivity data [micro-Ohm-cm]
   { 14.,       14.,       14.,       14.,       14.,       14.,
     14.01,     14.06,     14.13,     14.22,     14.35,     14.5,
     14.75,     15.,       15.5,      15.9,      16.3,      16.8}
  },
  //
  // 37 element
  {"Copper Nickel, 70-30",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.939, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000118,  0.0002235, 0.0005019, 0.001796,  0.007435,  0.0267,
     0.059,     0.095,     0.135,     0.173,     0.205,     0.232,
     0.245,     0.323,     0.356,     0.374,     0.38,      0.386},
          // Conductivity data [W/cm-K]
   { 0.00265,   0.00534,   0.0113,    0.043,     0.104,     0.15,
     0.178,     0.192,     0.205,     0.216,     0.22,      0.225,
     0.229,     0.24,      0.25,      0.265,     0.275,     0.285},
          // Expansivity data [percent]
   {-0.2507,   -0.2507,   -0.2507,   -0.2507,   -0.2505,   -0.2502,
    -0.25,     -0.246,    -0.243,    -0.24,     -0.235,    -0.23,
    -0.225,    -0.17,     -0.107,    -0.035,     0.0,       0.04},
          // Resistivity data [micro-Ohm-cm]
   { 36.32,     36.39,     36.4,      36.46,     36.5,      36.52,
     36.56,     36.6,      36.63,     36.7,      36.73,     36.8,
     36.9,      37.3,      37.85,     38.3,      38.4,      38.55}
  },
  //
  // 38 element
  {"Constantan, Cu-Ni, 57-43%",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.939, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0001131, 0.0002111, 0.0004732, 0.001738,  0.007435,  0.0267,
     0.059,     0.095,     0.135,     0.173,     0.205,     0.232,
     0.245,     0.323,     0.356,     0.374,     0.38,      0.386},
          // Conductivity data [W/cm-K]
   { 0.00095,   0.00285,   0.0095,    0.035,     0.087,     0.13,
     0.155,     0.181,     0.183,     0.188,     0.1905,    0.195,
     0.2,       0.216,     0.228,     0.238,     0.241,     0.249},
          // Expansivity data [percent]
   {-0.246,    -0.246,    -0.246,    -0.246,    -0.2455,   -0.2452,
    -0.2449,   -0.2425,   -0.238,    -0.235,    -0.23,     -0.224,
    -0.218,    -0.17,     -0.107,    -0.035,     0.0,       0.04},
          // Resistivity data [micro-Ohm-cm]
   { 46.1,      46.1,      46.1,      46.1,      46.1,      46.12,
     46.13,     46.14,     46.25,     46.37,     46.5,      46.6,
     46.7,      47.4,      48.,       48.6,      48.9,      49.2}
  },
  //
  // 39 element
  {"Brass, 90-10% Cu-Zn, Commercial Bronze, CDA 22000",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.802, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0001131, 0.0002111, 0.0004732, 0.001738,  0.007435,  0.0267,
     0.059,     0.095,     0.135,     0.173,     0.205,     0.232,
     0.245,     0.323,     0.356,     0.374,     0.38,      0.386},
          // Conductivity data [W/cm-K]
   { 0.00095,   0.00285,   0.0095,    0.035,     0.087,     0.13,
     0.155,     0.181,     0.183,     0.188,     0.1905,    0.195,
     0.2,       0.216,     0.228,     0.238,     0.241,     0.249},
          // Expansivity data [percent]
   {-0.246,    -0.246,    -0.246,    -0.246,    -0.2455,   -0.2452,
    -0.2449,   -0.2425,   -0.238,    -0.235,    -0.23,     -0.224,
    -0.218,    -0.17,     -0.107,    -0.035,     0.0,       0.04},
          // Resistivity data [micro-Ohm-cm]
   { 46.1,      46.1,      46.1,      46.1,      46.1,      46.12,
     46.13,     46.14,     46.25,     46.37,     46.5,      46.6,
     46.7,      47.4,      48.,       48.6,      48.9,      49.2}
  },
  //
  // 40 element
  {"Brass, 80-20% Cu-Zn, Low Brass, CDA 24000",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.664, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0000114, 0.00003195,0.000133,  0.0012585, 0.00995,   0.03515,
     0.072225,  0.1128,    0.1511,    0.1851,    0.2152,    0.2407,
     0.2611,    0.3269,    0.3579,    0.373,     0.3771,    0.3799},
          // Conductivity data [W/cm-K]
   { 0.00786,   0.018,     0.0437,    0.15,      0.311,     0.435,
     0.505,     0.56,      0.6,       0.649,     0.697,     0.7427,
     0.7858,    0.9759,    1.138,     1.282,     1.344,     1.413},
          // Expansivity data [percent]
   {-0.33140,  -0.33140,  -0.33140,  -0.33138,  -0.33112,  -0.33006,
    -0.32752,  -0.32316,  -0.31693,  -0.30901,  -0.29957,  -0.28881,
    -0.27698,  -0.20764,  -0.12688,  -0.04069,   0.0,       0.04821},
          // Resistivity data [micro-Ohm-cm]
   { 3.26,      3.26,      3.26,      3.265,     3.27,      3.275,
     3.31,      3.37,      3.45,      3.54,      3.64,      3.727,
     3.814,     4.25,      4.69,      5.129,     5.33,      5.567}
  },
  //
  // 41 element
  {"Brass, 70-30% Cu-Zn, Cartridge Brass, CDA 26000",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.525, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.0000117, 0.0000346, 0.000159,  0.00147,   0.0119,    0.0407,
     0.0814,    0.1209,    0.1597,    0.1908,    0.2206,    0.247,
     0.2668,    0.3296,    0.3574,    0.3721,    0.375,     0.3795},
          // Conductivity data [W/cm-K]
   { 0.00626,   0.015,     0.0382,    0.118,     0.246,     0.328,
     0.383,     0.42,      0.463,     0.51,      0.55,      0.606,
     0.65,      0.826,     0.9326,    1.056,     1.11,      1.164},
          // Expansivity data [percent]
   {-0.34139,  -0.34139,  -0.34139,  -0.34137,  -0.34104,  -0.33978,
    -0.33684,  -0.33198,  -0.32523,  -0.31681,  -0.30691,  -0.29567,
    -0.28331,  -0.21161,  -0.12902,  -0.04132,   0.0,       0.048983},
          // Resistivity data [micro-Ohm-cm]
   { 4.22,      4.22,      4.22,      4.22,      4.22,      4.26,
     4.32,      4.4,       4.5,       4.592,     4.69312,   4.7945,
     4.8959,    5.4029,    5.9098,    6.41675,   6.65,      6.9237}
  },
  //
  // 42 element
  {"Brass, 65-35% Cu-Zn, Yellow Brass, CDA 27000",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.47,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00001175,0.000035925,0.000172, 0.001575,  0.012875,  0.0435,
     0.086,     0.125,     0.164,     0.194,     0.2235,    0.2503,
     0.2687,    0.3308,    0.358,     0.372,     0.3746,    0.3791},
          // Conductivity data [W/cm-K]
   { 0.005738,  0.0137,    0.035,     0.095,     0.2,       0.28,
     0.35,      0.4,       0.445,     0.472,     0.5096,    0.551,
     0.5908,    0.773,     0.9352,    1.084,     1.149,     1.22},
          // Expansivity data [percent]
   {-0.35630,  -0.35630,  -0.35630,  -0.35627,  -0.35591,  -0.35451,
    -0.35127,  -0.34601,  -0.33879,  -0.32986,  -0.31944,  -0.30762,
    -0.29467,  -0.21987,  -0.13393,  -0.04285,   0.0,       0.05078},
          // Resistivity data [micro-Ohm-cm]
   { 4.48,      4.48,      4.48,      4.48,      4.49,      4.513,
     4.573,     4.66,      4.76,      4.86,      4.96,      5.07,
     5.17,      5.68,      6.19,      6.7,       6.93,      7.21}
  },
  //
  // 43 element
  {"Mylar, PET, amorphous, partly crystalline, typical MLI",
          // Description
"fibers are stretched to obtain varying degrees of           \n\
crystallinity and preferred property orientation which give \n\
a wide range to the properties. Properties for amorphous and\n\
for a highly crystalline fraction of bonds are included to  \n\
show the known range. Trade names include Dacron, Terylene, \n\
Mylar, Hostaphan, Lavsan, and Melinex. A remarkable range of\n\
additives provide a wide range in the cryogenic properties.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.34,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000013,  0.000104,  0.00096,   0.013,     0.048,     0.125,
     0.195,     0.24,      0.282,     0.323,     0.36,      0.4,
     0.442,     0.665,     0.845,     1.0,       1.083,     1.16},
          // Conductivity data [W/cm-K]
   { 9.29e-6,   2.63e-5,   8.0e-5,    0.0003,    0.00058,   0.00075,
     0.00083,   0.00088,   0.00092,   0.00096,   0.001,     0.00104,
     0.00106,   0.00117,   0.0012,    0.00122,   0.00122,   0.00122},
          // Expansivity data [percent]
   {-0.423,    -0.423,    -0.421,    -0.4155,   -0.411,    -0.401,
    -0.39,     -0.379,    -0.367,    -0.355,    -0.3435,   -0.327,
    -0.315,    -0.245,    -0.153,    -0.05,      0.0,       0.055},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 44 element
  {"Mylar, PET, semicrystalline, approximately 50%",
          // Description
"fibers are stretched to obtain varying degrees of           \n\
crystallinity and preferred property orientation which give \n\
a wide range to the properties. Properties for amorphous and\n\
for a highly crystalline fraction of bonds are included to  \n\
show the known range. Trade names include Dacron, Terylene, \n\
Mylar, Hostaphan, Lavsan, and Melinex. A remarkable range of\n\
additives provide a wide range in the cryogenic properties.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.409, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000013,  0.000104,  0.00096,   0.011,     0.06,      0.125,
     0.195,     0.24,      0.282,     0.323,     0.36,      0.4,
     0.44,      0.6,       0.75,      0.885,     0.938,     1.00},
          // Conductivity data [W/cm-K]
   { 1.8e-5,    4.5e-5,    1.15e-4,   0.00036,   0.00075,   0.00108,
     0.00135,   0.00157,   0.0018,    0.00192,   0.0021,    0.00222,
     0.00236,   0.00261,   0.002745,  0.0028,    0.002825,  0.00284},
          // Expansivity data [percent]
   {-1.158,    -1.158,    -1.158,    -1.157,    -1.154,    -1.147,
    -1.136,    -1.119,    -1.096,    -1.067,    -1.033,    -0.9956,
    -0.955,    -0.722,    -0.45,    -0.147,      0.0,       0.18},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 45 element
  {"Apiezon \"N\" Grease",
          // Description
"crystallinity and structure are affected by conditions of   \n\
cooling and ambient pressure. Values given here are typical \n\
of cooldown without evacuation.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   0,     // maskExpans
   0,     // maskResist
          //
   0.96,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000028,  0.00021,   0.0017,    0.021,     0.0896,    0.17,
     0.27,      0.375,     0.432,     0.45,      0.48,      0.5,
     0.56,      0.9,       1.16,      1.82,      2.4,       3.65},
          // Conductivity data [W/cm-K]
   { 1.e-4,     3.16e-4,   9.5e-4,    0.0012,    0.0013,    0.00135,
     0.00138,   0.0014,    0.00142,   0.00143,   0.00144,   0.00147,
     0.0015,    0.0018,    0.0021,    0.0023,    0.0024,    0.0024},
          // Expansivity data [percent]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 },
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 46 element
  {"Styrofoam, SpGr = 0.05",
          // Description
"open cell, evacuated foam, test results are presented here. \n\
Thermal conductivity is influenced by foaming agents above  \n\
100K. Specific heat is for solid polystyrene and may have   \n\
significant error at very low temperature. Data are sensit- \n\
ive to cell size. Data should be used for estimation only.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   0.05,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000042,  0.00046,   0.00445,   0.031,     0.102,     0.166,
     0.219,     0.266,     0.305,     0.345,     0.383,     0.42,
     0.453,     0.617,     0.798,     0.988,     1.08,      1.21},
          // Conductivity data [W/cm-K]
   { 4.6e-5,    4.6e-5,    4.8e-5,    5.0e-5,    5.3e-5,    5.5e-5,
     5.8e-5,    6.1e-5,    6.5e-5,    6.9e-5,    7.4e-5,    7.8e-5,
     8.2e-5,    1.11e-4,   0.000147,  1.9e-4,    0.000215,  0.000258},
          // Expansivity data [percent]
   {-1.6,      -1.6,      -1.6,      -1.6,      -1.585,    -1.57,
    -1.53,     -1.49,     -1.43,     -1.38,     -1.32,     -1.265,
    -1.2,      -0.86,     -0.51,     -0.16,      0.0,       0.2},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 47 element
  {"Styrofoam, SpGr = 0.1",
          // Description
"open cell, evacuated foam, test results are presented here. \n\
Thermal conductivity is influenced by foaming agents above  \n\
100K. Specific heat is for solid polystyrene and may have   \n\
significant error at very low temperature. Data are sensit- \n\
ive to cell size. Data should be used for estimation only.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   0.1,   // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000042,  0.00046,   0.00445,   0.031,     0.102,     0.166,
     0.219,     0.266,     0.305,     0.345,     0.383,     0.42,
     0.453,     0.617,     0.798,     0.988,     1.08,      1.21},
          // Conductivity data [W/cm-K]
   { 9.0e-5,    9.03e-5,   9.1e-5,    9.2e-5,    9.4e-5,    9.7e-5,
     1.04e-4,   1.1e-4,    1.16e-4,   1.22e-4,   1.284e-4,  1.34e-4,
     1.4e-4,    1.79e-4,   0.00023,   2.88e-4,   0.00032,   0.000357},
          // Expansivity data [percent]
   {-1.6,      -1.6,      -1.6,      -1.6,      -1.585,    -1.57,
    -1.53,     -1.49,     -1.43,     -1.38,     -1.32,     -1.265,
    -1.2,      -0.86,     -0.51,     -0.16,      0.0,       0.2},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 48 element
  {"Polystyrene, PS, unmodified, general purpose, SpGr = 1.04",
          // Description
"is produced as highly crystalline (brittle) to amorphous.   \n\
Additives and copolymers give a wide range of properties;   \n\
the presented data are for amorphous material.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.04,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000042,  0.00046,   0.00445,   0.031,     0.102,     0.166,
     0.219,     0.266,     0.305,     0.345,     0.383,     0.42,
     0.453,     0.617,     0.798,     0.988,     1.08,      1.21},
          // Conductivity data [W/cm-K]
   { 1.15e-4,   2.22e-4,   2.9e-4,    3.0e-4,    3.13e-4,   3.39e-4,
     3.64e-4,   3.9e-4,    4.16e-4,   4.42e-4,   4.67e-4,   4.93e-4,
     5.18e-4,   6.48e-4,   7.77e-4,   9.05e-4,   0.000965,  0.00104},
          // Expansivity data [percent]
   {-1.325,    -1.324,    -1.322,    -1.32,     -1.307,    -1.287,
    -1.26,     -1.23,     -1.187,    -1.143,    -1.1,      -1.055,
    -1.0,      -0.73,     -0.45,     -0.14,      0.0,       0.175},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 49 element
  {"Polycarbonate, PC, Amorphous extruded plate",
          // Description
"crystallinity is readily controlled by heat treatment so    \n\
properties may vary widely.  The molecular weight is about  \n\
50,000 and bond structure, hence properties, is readily     \n\
controllable by additives. The elongation of PC diminishes  \n\
greatly around its 160K transition temperature. These data  \n\
are for low crystallinity, amorphous material. Do not use   \n\
this data for \"polycarbonate\" films.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.2,   // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000024,  0.000192,  0.00178,   0.024,     0.102,     0.159,
     0.21,      0.255,     0.286,     0.325,     0.364,     0.403,
     0.439,     0.636,     0.832,     1.028,     1.119,     1.228},
          // Conductivity data [W/cm-K]
   { 2.0e-4,    2.5e-4,    3.1e-4,    4.0e-4,    7.2e-4,    8.8e-4,
    10.0e-4,   11.25e-4,  12.2e-4,   13.0e-4,    0.00137,   0.00144,
     0.0015,    0.00177,   0.002,    0.00222,    0.0023,    0.0024},
          // Expansivity data [percent]
   {-1.320375, -1.320285, -1.319955, -1.317925, -1.310925, -1.298425,
    -1.279425, -1.253925, -1.221125, -1.181575, -1.136325, -1.086325,
    -1.033825, -0.755075, -0.458825, -0.150075,  0.0,       0.1836},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 50 element
  {"5083-T0 Aluminum Alloy (Annealed)",
          // Description
   "",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   2.657, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000051,  0.000108,  0.000302,  0.0014,    0.0089,    0.0315,
     0.0775,    0.142,     0.214,     0.287,     0.357,     0.422,
     0.481,     0.684,     0.797,     0.859,     0.88,      0.902},
          // Conductivity data [W/cm-K]
   { 0.00677,   0.0145,    0.0033,    0.086,     0.178,     0.266,
     0.345,     0.42,      0.482,     0.54,      0.59,      0.63,
     0.67,      0.83,      0.98,      1.13,      1.2,       1.28},
          // Expansivity data [percent]
   {-0.367,    -0.367,    -0.367,    -0.367,    -0.367,    -0.366,
    -0.364,    -0.36,     -0.355,    -0.349,    -0.34,     -0.33,
    -0.32,     -0.248,    -0.152,    -0.05,      0.0,       0.062},
          // Resistivity data [micro-Ohm-cm]
   { 3.03,      3.03,      3.03,      3.03,      3.03,      3.035,
     3.04,      3.07,      3.15,      3.265,     3.37,      3.49,
     3.604,     4.196,     4.789,     5.382,     5.655,     5.975}
  },
  //
  // 51 element
  {"HDPE, High Density Polyethylene, SpGr.=0.94",
          // Description
"has a molecular weight in the range of from 5,000 to 50,000 \n\
and varies from amorphous to nearly 100% crystallinity.     \n\
This thermoplastic may have extremely wide variations in    \n\
properties. The user should know and control specifications \n\
of all polyethylenes closely. This data applies to 50%      \n\
crystallinity.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   0.94,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00005,   0.0002,    0.00143,   0.0165,    0.075,     0.1625,
     0.251,     0.3375,    0.416,     0.5,       0.581,     0.669,
     0.753,     0.898,     1.11,      1.645,     1.8,       1.855},
          // Conductivity data [W/cm-K]
   { 0.00003,   0.0001,    0.00029,   0.0009,    0.00175,   0.0024,
     0.0029,    0.00335,   0.00371,   0.004,     0.0042,    0.00438,
     0.0045,    0.005,     0.00478,   0.0044,    0.0042,    0.0039},
          // Expansivity data [percent]
   {-1.74393,  -1.7439,   -1.74373,  -1.74278,  -1.73913,  -1.73163,
    -1.71913,  -1.70113,  -1.67678,  -1.64693,  -1.61228,  -1.57163,
    -1.52463,  -1.21263,  -0.77813,  -0.25438,   0.0,       0.29916},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 52 element
  {"Micarta, CE, Cotton Phenolic Composite, parallel to fiber",
          // Description
"is the cotton/phenolic material designated CE.  Data are for\n\
planes parallel to the cloth weave.",
   0,     // Metal
          //
   0,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   1.39,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 },
          // Conductivity data [W/cm-K]
   { 0.00005,   0.00019,   0.000385,  0.00083,   0.00145,   0.00215,
     0.0025,    0.0031,    0.0033,    0.0038,    0.00405,   0.0043,
     0.0046,    0.0053,    0.0058,    0.0059,    0.00595,   0.006},
          // Expansivity data [percent]
   {-0.234,    -0.234,    -0.234,    -0.234,    -0.233,    -0.231,
    -0.2275,   -0.2239,   -0.22,     -0.215,    -0.2105,   -0.2045,
    -0.197,    -0.152,    -0.099,    -0.035,     0.0,       0.043},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 53 element
  {"1020 Steel",
          // Description
"undergoes a low temperature \"brittle\" transition and is not \n\
considered a cryogenic grade material.  It is frequently    \n\
used for its ferromagnetic properties at low temperatures.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   7.85,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000377,  0.000661,  0.00136,   0.00377,   0.0115,    0.023,
     0.0391,    0.069,     0.1058,    0.143,     0.19,      0.23,
     0.251,     0.327,     0.373,     0.406,     0.427,     0.444},
          // Conductivity data [W/cm-K]
   { 0.00706,   0.0155,    0.035,     0.1,       0.21,      0.326,
     0.417,     0.488,     0.54,      0.58,      0.602,     0.618,
     0.625,     0.65,      0.65,      0.65,      0.65,      0.65},
          // Expansivity data [percent]
   {-0.1803,   -0.1803,   -0.1803,   -0.1803,   -0.1803,   -0.1798,
    -0.1793,   -0.1781,   -0.1763,   -0.1735,   -0.1703,   -0.1657,
    -0.1603,   -0.1263,   -0.0793,   -0.0253,    0.0,       0.0297},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 54 element
  {"9Ni Steel, 2800, ASTM A353",
          // Description
"is a cryogenic grade steel developed for use to LN2 temper- \n\
atures.  The addition of Ni to steel depresses its \"brittle\"\n\
transition temperature.  See ASTM A353.",
   1,     // Metal
          //
   0,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   7.33,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 },
          // Conductivity data [W/cm-K]
   { 0.000632,  0.00155,   0.00405,   0.0124,    0.0305,    0.0485,
     0.066,     0.083,     0.1,       0.116,     0.1335,    0.147,
     0.162,     0.212,     0.25,      0.28,      0.29,      0.295},
          // Expansivity data [percent]
   {-0.1835,   -0.1835,   -0.1835,   -0.1833,   -0.1828,   -0.182,
    -0.18,     -0.1775,   -0.1745,   -0.171,    -0.1675,   -0.163,
    -0.1575,   -0.1225,   -0.079,    -0.02725,   0.0,       0.0325},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 55 element
  {"GE 7031 Lacquer, well dried in application",
          // Description
"properties depend on application procedures. The present    \n\
data describe lacquer applied in thin coats and fully dried \n\
at room temperature and pressure.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   0,     // maskExpans
   0,     // maskResist
          //
   0.911, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000041,  0.0003,    0.003,     0.0245,    0.0825,    0.16,
     0.24,      0.312,     0.376,     0.44,      0.503,     0.566,
     0.629,     0.945,     1.26,      1.58,      1.72,      1.89},
          // Conductivity data [W/cm-K]
   { 0.0003,    0.0004,    0.00055,   0.0008,    0.00115,   0.0014,
     0.0016,    0.0018,    0.002,     0.00215,   0.00228,   0.0024,
     0.0025,    0.00305,   0.00357,   0.00408,   0.0043,    0.0044},
          // Expansivity data [percent]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 },
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 56 element
  {"K Monel, annealed, 67Ni-30Cu-1.4Fe-1Mn",
          // Description
"thermal conductivity data at 1 and 2K are extrapolated using\n\
generally accepted practices.  Resistivity is for the       \n\
annealed condition, data 14% higher are reported, but the   \n\
hardness is not reported.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   8.46,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00011,   0.00022,   0.000501,  0.0017,    0.0071,    0.021,
     0.045,     0.078,     0.11,      0.15,      0.18,      0.21,
     0.24,      0.325,     0.37,      0.405,     0.4165,    0.43},
          // Conductivity data [W/cm-K]
   { 0.001379,  0.003503,  0.0095,    0.0305,    0.071,     0.1003,
     0.122,     0.135,     0.142,     0.149,     0.155,     0.16,
     0.165,     0.185,     0.2,       0.21,      0.216,     0.222},
          // Expansivity data [percent]
   {-0.224,    -0.224,    -0.224,    -0.224,    -0.224,    -0.224,
    -0.223,    -0.221,    -0.218,    -0.214,    -0.209,    -0.203,
    -0.196,    -0.15,     -0.094,    -0.031,     0.0,       0.037},
          // Resistivity data [micro-Ohm-cm]
   {27.75,     27.75,     27.75,     27.75,     27.78,     27.8,
    27.83,     27.85,     27.88,     27.9,      28.0,      28.25,
    28.5,      31.3,      35.65,     39.9,      42.0,      44.37}
  },
  //
  // 57 element
  {"PCTFE, Kel-F,  50% crystallinity",
          // Description
"properties vary widely due to the ease of control of crys-  \n\
tallinity via quenching or slow cooling.  CTFE has a molec- \n\
ular weight in the range of 1-4E6.  Data are for 50% cryst.",
   0,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   0,     // maskResist
          //
   2.134, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.00006,   0.00047,   0.0041,    0.026,     0.079,     0.126,
     0.17,      0.21,      0.246,     0.278,     0.312,     0.352,
     0.39,      0.55,      0.68,      0.8,       0.85,      0.9},
          // Conductivity data [W/cm-K]
   { 0.00005,   0.000094,  0.0002,    0.000435,  0.00065,   0.00075,
     0.00086,   0.00093,   0.00098,   0.00102,   0.00106,   0.00109,
     0.0011,    0.00118,   0.00127,   0.00135,   0.00139,   0.00142},
          // Expansivity data [percent]
   {-0.96,     -0.958,    -0.955,    -0.95,     -0.934,    -0.913,
    -0.89,     -0.863,    -0.838,    -0.81,     -0.78,     -0.755,
    -0.725,    -0.565,    -0.38,     -0.13,      0.0,       0.175},
          // Resistivity data [micro-Ohm-cm]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 }
  },
  //
  // 58 element
  {"NbTi Alloy",
          // Description
"49 wt % Ti (65 at % Ti) having 90% residual cold work is re-\n\
ported here.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   6.55,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 1.64e-6,   2.7e-5,    0.0006,    0.0038,    0.021,     0.058,
     0.11,      0.15,      0.2,       0.24,      0.27,      0.3,
     0.307,     0.37,      0.4,       0.42,      0.424,     0.426},
          // Conductivity data [W/cm-K]
   { 0.00026,   0.0006,    0.0019,    0.0057,    0.0113,    0.017,
     0.0226,    0.0281,    0.0337,    0.0393,    0.0449,    0.0504,
     0.055,     0.0715,    0.081,     0.09,      0.0925,    0.095},
          // Expansivity data [percent]
   {-0.169,    -0.169,    -0.169,    -0.169,    -0.168,    -0.1656,
    -0.164,    -0.1609,   -0.1562,   -0.153,    -0.148,    -0.142,
    -0.132,    -0.102,    -0.0593,   -0.028,     0.0,       0.033},
          // Resistivity data [micro-Ohm-cm]
   { 0.0,       0.0,       0.0,      60.0,      60.3,       60.7,
    61.0,      61.4,      61.8,      62.1,      62.5,       62.8,
    63.2,      65.0,      66.7,      68.5,      69.3,       70.3}
  },
  //
  // 59 element
  {"7075-T6 Aluminum Alloy",
          // Description
"contains 4-6 % Zn as a major alloying element. Specific heat\n\
data are limited for this material. Data exists that contra-\n\
indicates the use of pure aluminum specific heat for 7075, a\n\
widely accepted practice.",
   1,     // Metal
          //
   0,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   2.796, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 },
          // Conductivity data [W/cm-K]
   { 0.00677,   0.0145,    0.033,     0.086,     0.178,     0.266,
     0.345,     0.42,      0.482,     0.54,      0.59,      0.63,
     0.67,      0.83,      0.98,      1.13,      1.2,       1.28},
          // Expansivity data [percent]
   {-0.37,     -0.37,     -0.37,     -0.37,     -0.37,     -0.3691,
    -0.367,    -0.3637,   -0.359,    -0.3529,   -0.345,    -0.3344,
    -0.322,    -0.2467,   -0.156,    -0.0513,    0.0,       0.0624},
          // Resistivity data [micro-Ohm-cm]
   { 2.76,      2.76,      2.76,      2.7625,    2.77,      2.777,
     2.8,       2.842,     2.902,     2.99,      3.086,     3.2,
     3.314,     3.884,     4.454,     5.024,     5.286,     5.594}
  },
  //
  // 60 element
  {"4340 Steel",
          // Description
"is classed as a low alloy steel. Thermal conductivity data  \n\
below 20K are extrapolated. Specific heat data are also not \n\
available. Caution is advised in using this data below 20K.",
   1,     // Metal
          //
   0,     // maskCp
   1,     // maskConduct
   1,     // maskExpans
   1,     // maskResist
          //
   7.833, // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 },
          // Conductivity data [W/cm-K]
   { 0.0014,    0.0032,    0.0075,    0.024,     0.06,      0.104,
     0.142,     0.177,     0.203,     0.22,      0.233,     0.243,
     0.251,     0.289,     0.32,      0.345,     0.355,     0.366},
          // Expansivity data [percent]
   {-0.176,    -0.176,    -0.176,    -0.176,    -0.176,    -0.1755,
    -0.175,    -0.1736,   -0.1718,   -0.1686,   -0.1645,   -0.1603,
    -0.156,    -0.123,    -0.0805,   -0.0265,    0.0,       0.033},
          // Resistivity data [micro-Ohm-cm]
   { 0.181494,  0.181495,  0.1815,    0.18156,   0.1816,    0.182,
     0.183,     0.1852,    0.1878,    0.1908,    0.1945,    0.199,
     0.204,     0.2314,    0.262,     0.2941,    0.31,      0.3275}
  },
  //
  // 61 element
  {"Nb3Sn",
          // Description
"data for thermal conductivity above 80K are estimated       \n\
values. Thermal expansion data for Nb3Sn is not available.  \n\
Caution is advised in using the thermal conductivity data at\n\
higher temperatures.",
   1,     // Metal
          //
   1,     // maskCp
   1,     // maskConduct
   0,     // maskExpans
   1,     // maskResist
          //
   8.95,  // Density
          //
   18,    // Data Dimension
          // Temperature steps common to all data.
   { 1.,  2.,  4.2,  10.,  20.,  30.,  40.,  50.,  60., 
    70., 80., 90.,  100., 150., 200., 250., 273., 300.},
          // Cp-data [J/g-K]
   { 2.69e-6,   2.15e-5,   0.000199,  0.00269,   0.0121,    0.0317,
     0.0575,    0.0822,    0.105,     0.125,     0.142,     0.154,
     0.165,     0.206,     0.227,     0.236,     0.239,     0.243},
          // Conductivity data [W/cm-K]
   { 5.62e-5,   0.0001,    0.0003551, 0.00671,   0.02482,   0.0283,
     0.0283,    0.02776,   0.0258,    0.0245,    0.0233,    0.0224,
     0.0217,    0.01823,   0.01608,   0.01392,   0.0132,    0.1292},
          // Expansivity data [percent]
   { 0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     0.000000,  0.000000,  0.000000,  0.000000,  0.000000,  0.000000 },
          // Resistivity data [micro-Ohm-cm]
   { 0.0,       0.0,       0.0,       0.0,      26.0,      26.5,
    27.0,      27.6,      28.1,      28.65,     29.2,      29.8,
    30.4,      33.5,      36.0,      37.8,      38.5,      39.0}
  }
};

#endif
