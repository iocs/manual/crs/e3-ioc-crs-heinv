//
//  Quasi-logarithmic thermodynamic functions
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 16-May-2006
//  Modified: 
//
//  Notes:
//

#ifndef _cryoHe_quasiLogFun_h
#define _cryoHe_quasiLogFun_h

#include "HepackError.h"

#include "cryoHe_LambdaLine.h"
#include "cryoHe_InitParameters.h"

class cryoHe_quasiLogFun
{
  public:
  
    // Constructor(s)
    cryoHe_quasiLogFun();
    
    // Methods

      //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
      // Pressure [MPa] as a function of density [kg/m3] and temperature [K]
    double Pres_vsDT(double Dens, double Temp, HepackError_t *perr);

      //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
      // dP/dT [Mpa/K] as a function of density [kg/m3] and temperature [K]
    double dPdT_vsDT(double Dens, double Temp, HepackError_t *perr);

      //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
      // dP/dV [MPa*kg/m3] as a function of density [kg/m3] and temperature [K]
    double dPdV_vsDT(double Dens, double Temp, HepackError_t *perr);

      //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
      // Cv [J/(kg*K)] as a function of density [kg/m3] and temperature [K]
    double Cv_vsDT(double Dens, double Temp, HepackError_t *perr);

      //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
      // Entropy [J/(kg*K)] as a function of density [kg/m3] and temperature [K]
    double Entr_vsDT(double Dens, double Temp, HepackError_t *perr);

      //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
      // Helmholz energy [J/kg] as a function of density [kg/m3] and temperature [K]
    double Helm_vsDT(double Dens, double Temp, HepackError_t *perr);

    // Destructor(s)
    ~cryoHe_quasiLogFun();
    
  private:
    
    void LogFun_vsDT(double Dens, double Temp, HepackError_t *perr);
  
    double lg_dens;
    double lg_temp;
    
    double lg_Pres;
    double lg_dPdV;
    double lg_dPdT;
    double lg_Cv;  
    double lg_Entr;
    double lg_Helm;
    
    cryoHe_LambdaLine lline;
};

#endif
