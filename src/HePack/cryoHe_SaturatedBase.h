//
//  Base calculations for Saturated Liquid and Saturated Vapor
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 06-Jul-2006
//  Modified: 
//    20.May.2007 by V.Gubarev
//      o Back functions are replaced by splines
//      o Add Function Temp_vsP(Pres)
//
//  Notes:
//    1. Valid for temperature range 0.8 to 5.1953 K, T76 scale.
//    2. Test on input parameters excluded (!)
//

#ifndef _cryoHe_SaturatedBase_h
#define _cryoHe_SaturatedBase_h

#include <stdlib.h>
#include <math.h>

#include "spline_Classic.h"
#include "spline_Smooth.h"

#include "HepackError.h"

#include "cryoHe_InitParameters.h"

#define cryoHe_SaturatedBase_meth_ 3

class cryoHe_SaturatedBase
{
  public:
  
    // Constructor(s)
    cryoHe_SaturatedBase();

      // (1) from SUBROUTINE PSATFT (P, DPDTS, T)
      // Saturation pressure as a function of temperature
    double Pres_vsT(double Temp, HepackError_t *perr);

      // (2) from SUBROUTINE PSATFT (P, DPDTS, T)
      // Saturation dP/dT as a function of temperature
    double dPdT_vsT(double Temp, HepackError_t *perr);

      // (3) FUNCTION TSATFP (PP)
      // Temperature [K] as a function of pressure [Pa]
    double Temp_vsP(double Pres, HepackError_t *perr);

    // Destructor(s)
    ~cryoHe_SaturatedBase();
    
    //  Limited Variables
    double lowTemp;   // low Temperature [K]
    double crtTemp;   // critical Temperature [K]
    double uppTemp;   // upper Temperature [K]
    
  private:
    // Spline(s)
    spline *_TvsP;
    //
    // Calculate or it was done
    int todo(int fNo, double p1);
    //
    double val[cryoHe_SaturatedBase_meth_+1][2];
    int    stt[cryoHe_SaturatedBase_meth_+1];
};

#endif
