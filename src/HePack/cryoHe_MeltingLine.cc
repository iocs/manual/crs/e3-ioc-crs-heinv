//
//  Calculation of the Melting Line parameters
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 14-Jun-2006
//  Modified: 
//    21.May.2007 by V.Gubarev
//      o Back functions are replaced by splines
//
//  Notes:
//    1. Valid for temperature from 0.8 to 14.0104 K.
//    2. Test on input parameters excluded (!)
//

#include "cryoHe_MeltingLine.h"

//  Class cryoHe_MeltingLine
//
cryoHe_MeltingLine::cryoHe_MeltingLine()
{
  for (int i=0; i<cryoHe_MeltingLine_Meth_; i++) stt[i] = 0;
  //
  lowTemp = HePrp_TMIN;
  uppTemp = 14.0104;
  lowDens = Dens_vsT(lowTemp, NULL);
  uppDens = Dens_vsT(uppTemp, NULL);
  lowPres = Pres_vsT(lowTemp, NULL);
  uppPres = Pres_vsT(uppTemp, NULL);
  //
  _TvsD = 0;
  _TvsP = 0;
}
  // (1) FUNCTION PMFT (TT)
  // melting pressure as a function of temperature, T76 scale below 5.1953
double cryoHe_MeltingLine::Pres_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(1,Temp)) {
    double dT=0.005;
    if      (Temp >  uppTemp)     val[1][0] = 1013.25e+05;
    else if (Temp >  5.1953 + dT) val[1][0] = f1(Temp);
    else if (Temp >  5.1953 - dT) val[1][0] = ((Temp - 5.1953)/2/dT + 0.5)*f1(Temp) + (1.-((Temp - 5.1953)/2/dT + 0.5))*f2(Temp);
    else if (Temp >= 2.0044 + dT) val[1][0] = f2(Temp);
    else if (Temp >  2.0044 - dT) val[1][0] = ((Temp - 2.0044)/2/dT + 0.5)*f2(Temp) + (1.-((Temp - 2.0044)/2/dT + 0.5))*f3(Temp);
    else if (Temp >= 1.7660 + dT) val[1][0] = f3(Temp);
    else if (Temp >  1.7660 - dT) val[1][0] = ((Temp - 1.7660)/2/dT + 0.5)*f3(Temp) + (1.-((Temp - 1.7660)/2/dT + 0.5))*f4(Temp);
    else if (Temp >= 1.4676 + dT) val[1][0] = f4(Temp);
    else if (Temp >  1.4676 - dT) val[1][0] = ((Temp - 1.4676)/2/dT + 0.5)*f4(Temp) + (1.-((Temp - 1.4676)/2/dT + 0.5))*f5(Temp);
    else if (Temp >= lowTemp)     val[1][0] = f5(Temp);
    else val[1][0] = -1;
  }
  return val[1][0];
}
  // (2) FUNCTION TMFP (PP)
  // melting temperature as a function of pressure [Pa]
double cryoHe_MeltingLine::Temp_vsP(double Pres, HepackError_t *perr)
{
    if (Pres < 0.) HE_ERROR(VALUE_OUT)
    else if (todo(2,Pres)) {
    if (_TvsP == (spline *)0) {
      int NN=200, i=0;
      double *P = new double[NN+1], *T = new double[NN+1];
      for (double t=lowTemp, dT=(uppTemp - t)/NN; i<=NN; T[i]=t, P[i]=Pres_vsT(t, perr), i++, t=lowTemp+dT*i);
      _TvsP  = new spline_Classic(NN+1,P,T,1);
      delete [] P; delete [] T;
    }
    val[2][0] = (*_TvsP)(Pres);
  }
  return val[2][0];
}
  // (3) FUNCTION DMFT (T)
  // Liquid density at the melting line [kg/m3] as a function of T [K]
  // Range 0.8 to 14.0 K; accuracy generally better than 0.3 kg/m3
double cryoHe_MeltingLine::Dens_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(3,Temp)) {
    double lT=1.7673, hT=3.5300;
    double lc1=0.1679361577e+03, lc2=0.6728283584e+01, lc3=-0.1018218341e+02;
    double mc1=0.1469940814e+03, mc2=0.1858136626e+02, mc3=-0.1497696476e+01;
    double hc1=0.1542809083e+03, hc2=0.1890207406e+02, hc3=-0.8746341757e+00, hc4=0.2235656147e-01;
    double dT=Temp - lT;
    if      (Temp > hT)     val[3][0] = hc1 + Temp*(hc2 + Temp*(hc3 + Temp*hc4));
    else if (dT >= 1.e-04)  val[3][0] = dT*log( dT)*mc3 + mc1 + Temp*mc2;
    else if (dT <= -1.e-04) val[3][0] = dT*log(-dT)*lc3 + lc1 + Temp*lc2;
    else val[3][0] = 0;
  }
  return val[3][0];
}
  // (4) FUNCTION TMFD (DD)
  // melting temperature as a function of density
double cryoHe_MeltingLine::Temp_vsD(double Dens, HepackError_t *perr)
{
  if (Dens <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(4,Dens)) {
    if (_TvsD == (spline *)0) {
      int NN=400, i=0;
      double *D = new double[NN+1], *T = new double[NN+1], minT=1.056;
      for (double t=minT, dT=(uppTemp - t)/NN; i<=NN; T[i]=t, D[i]=Dens_vsT(t, perr), i++, t=minT+dT*i);
      _TvsD  = new spline_Classic(NN+1,D,T,1);
      delete [] D; delete [] T;
    }
    val[4][0] = (*_TvsD)(Dens);
  }
  return val[4][0];
}
  // (5) FUNCTION DMFP (P)
  // melting density as a function of pressure
double cryoHe_MeltingLine::Dens_vsP(double Pres, HepackError_t *perr)
{
  if (Pres < 0.) HE_ERROR(VALUE_OUT)
  else if (todo(5,Pres))
    val[5][0] = Dens_vsT(Temp_vsP(Pres, perr), perr);
  return val[5][0];
}

cryoHe_MeltingLine::~cryoHe_MeltingLine()
{
  if (_TvsP != (spline *)0) delete _TvsP;
  if (_TvsD != (spline *)0) delete _TvsD;
}
//
//  Private Part
//
double cryoHe_MeltingLine::f1(double x)
{
  return ((-17.80 + 17.31457*pow(x,1.555414))*98066.5);
}
double cryoHe_MeltingLine::f2(double x)
{
  return ((34.2097 + x*(-45.31231 + x*(32.26926 + x*(-4.91282 + x*0.310795))))*98066.5);
}
double cryoHe_MeltingLine::f3(double x)
{
  return ((17.8537 + x*(-15.49444 + x*12.57562))*101325.);
}
double cryoHe_MeltingLine::f4(double x)
{
  return ((99.3328 + x*(-101.44970 + x*35.1175))*101325.);
}
double cryoHe_MeltingLine::f5(double x)
{
  return ((24.997 + 3.36930*pow(x-0.725,4))*101325.);
}

int cryoHe_MeltingLine::todo(int fNo, double p1)
{
  return todo(fNo, p1, -1);
}
int cryoHe_MeltingLine::todo(int fNo, double p1, double p2)
{
  val[fNo][1] = p1;
  val[fNo][2] = p2;
  stt[fNo]    = 1;
  return 1;
}
