//
//  He parameters at fixed temperature (1.75K and 3K).
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 09-Jun-2006
//  Modified: 
//    21.Apr.2007 by V.Gubarev
//      o Inserted recalculation check
//
//  Notes:
//    Valid for Pressure > 0. Internal check for input parameter is ommited.
//

#include "cryoHe_atFixedTemp.h"

//  Class: cryoHe_atFixedTemp
//
cryoHe_atFixedTemp::cryoHe_atFixedTemp()
{
  for (int i=0; i<=cryoHe_atFixedTemp_meth_; i++) stt[i] = 0;
}
  // (1) FUNCTION D175K (PASCAL)
  // Density [kg/m3] of HeII as a function of P [Pa] at T(58)=1.75
  // accuracy ~ 0.2%.
double cryoHe_atFixedTemp::Dens175K_vsP(double Pres)
{
  if (todo(1,Pres)) {
    double c1=-0.8129582813e+00, c2=0.1859901260e+02, c3=-0.6344068036e+01, c4=0.2844939685e+01, c5=-0.8216755033e+00, c6=0.1044392629e+00;
    double x = Pres*1e-6;
    val[1][0] = 146.081+c1+x*(c2+x*(c3+x*(c4+x*(c5+x*c6))));
  }
  return val[1][0];
}
  // (2) FUNCTION H3K (P)
  // Enthalpy [J/kg] at 3 K, for P > P(sat)
  // accuracy about +/- 4.  J/kG for pressures below 2 bars
  //          about +/- 25. J/kG for pressures above 2 bars
double cryoHe_atFixedTemp::Enth3K_vsP(double Pres)
{
  if (todo(2,Pres)) {
    double c1=0.4954376389e+04, c2=-0.1302167945e+03, c3=0.6571877607e+03, c4=-0.1382310121e+02;
    if (Pres < 0.) return 0.;
    double x = sqrt(Pres*1e-5);
    val[2][0] = c1+x*(c2+x*(c3+x*c4));
  }
  return val[2][0];
}
  // (3) FUNCTION S3K (P)
  // Entropy [J/(kg*K)] at 3 K, for P > P(sat)
  // accuracy about +/- 1. J/(kg*K) for pressures below 2 bars
  //          about +/- 8. J/(kg*K) for pressures above 2 bars
double cryoHe_atFixedTemp::Entr3K_vsP(double Pres)
{
  if (todo(3,Pres)) {
    double c1=0.2365990305e+04, c2=-0.3012076857e+02, c3=-0.3492403215e+02, c4=0.5141720002e+01, c5=-0.2612678040e+00;
    if (Pres < 0.) return 0.;
    double x = sqrt(Pres*1e-5);
    val[3][0] = c1+x*(c2+x*(c3+x*(c4+x*c5)));
  }
  return val[3][0];
}
  // (4) FUNCTION U3K (P)
  // Internal Energy [J/kg] at 3 K, for P > P(sat)
  // accuracy about +/- 4.  J/kg for pressures below 2 bars
  //          about +/- 10. J/kg for pressures above 2 bars
double cryoHe_atFixedTemp::Ener3K_vsP(double Pres)
{
  if (todo(4,Pres)) {
    double c1=0.4947998498e+04, c2=-0.1008309679e+03, c3=-0.1066139445e+03, c4=0.2995380841e+02, c5=-0.1536198940e+01;
    if (Pres < 0.) return 0.;
    double x = sqrt(Pres*1e-5);
    val[4][0] = c1+x*(c2+x*(c3+x*(c4+x*c5)));
  }
  return val[4][0];
}
  // (5) FUNCTION D3K (P)
  // Density [kg/m3] at 3 K, for P > P(sat)
  // accuracy about +/- 0.1 kg/m3 for pressures below 15 bars
  //          about +/- 0.3 kg/m3 for pressures above 15 bars
double cryoHe_atFixedTemp::Dens3K_vsP(double Pres)
{
  if (todo(5,Pres)) {
    double c1=0.1398535389e+03, c2=0.1496220385e+01, c3=0.2334289084e+01, c4=-0.3419086874e+00, c5=0.1716145720e-01;
    if (Pres < 0.) return 0.;
    double x = sqrt(Pres*1e-5);
    val[5][0] = c1+x*(c2+x*(c3+x*(c4+x*c5)));
  }
  return val[5][0];
}
  //
  // Destructor(s)
cryoHe_atFixedTemp::~cryoHe_atFixedTemp() {}
//
//  Private Part
//
int cryoHe_atFixedTemp::todo(int fNo, double p1)
{
  val[fNo][1] = p1;
  stt[fNo]    = 1;
  return 1;
}
