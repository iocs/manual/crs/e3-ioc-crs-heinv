//
//  Calculation of the Melting Line parameters
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 14-Jun-2006
//  Modified: 
//    21.May.2007 by V.Gubarev
//      o Back functions are replaced by splines
//
//  Notes:
//    1. Valid for temperature from 0.8 to 14.0104 K.
//    2. Test on input parameters excluded (!)
//

#ifndef _cryoHe_MeltingLine_h
#define _cryoHe_MeltingLine_h

#include <stdlib.h>
#include <math.h>

#include "spline_Classic.h"
#include "spline_Smooth.h"

#include "HepackError.h"

#include "cryoHe_InitParameters.h"

#define cryoHe_MeltingLine_Meth_ 5

class cryoHe_MeltingLine
{
  public:
  
    //Constructor(s)
    cryoHe_MeltingLine();
    
    // Methods

      // (1) FUNCTION PMFT (TT)
      // melting pressure as a function of temperature, T76 scale below 5.1953
    double Pres_vsT(double Temp, HepackError_t *perr);

      // (2) FUNCTION TMFP (PP)
      // melting temperature as a function of pressure [Pa]
    double Temp_vsP(double Pres, HepackError_t *perr);

      // (3) FUNCTION DMFT (T)
      // Liquid density at the melting line [kg/m3] as a function of T [K]
      // Range 0.8 to 14.0 K; accuracy generally better than 0.3 kg/m3
    double Dens_vsT(double Temp, HepackError_t *perr);

      // (4) FUNCTION TMFD (DD)
      // melting temperature as a function of density
    double Temp_vsD(double Dens, HepackError_t *perr);

      // (5) FUNCTION DMFP (P)
      // melting density as a function of pressure
    double Dens_vsP(double Pres, HepackError_t *perr);

    //Destructor(s)
    ~cryoHe_MeltingLine();
    
    //  Limited Variables
    double lowTemp;   // low Temperature [K]
    double uppTemp;   // upper Temperature [K]
    double lowDens;   // low Density [kg/m3]
    double uppDens;   // upper Density [kg/m3]
    double lowPres;   // low Pressure [Pa]
    double uppPres;   // upper Pressure [Pa]

  private:
    // Splines
    spline *_TvsD;
    spline *_TvsP;
    //
    double f1(double x);
    double f2(double x);
    double f3(double x);
    double f4(double x);
    double f5(double x);
    //
    // Calculate or it was done
    int todo(int fNo, double p1);
    int todo(int fNo, double p1, double p2);
    //
    double val[cryoHe_MeltingLine_Meth_+1][3];
    int    stt[cryoHe_MeltingLine_Meth_+1];
};

#endif
