//
//  Calculations for temperatures 0.8 to 1500 K (excluding the 2-phase region)
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 22-May-2006
//  Modified: 
//    on 18-Apr-2007 by V.Gubarev 
//        o Included Visc_vsDT(Dens, Temp)
//    on 23-Apr-2007 by V.Gubarev 
//        o Included Dens_vsPT(Pres, Temp)
//        o Included Temp_vsDP(Dens, Pres)
//
//  Notes:
//    1. Valid for temperature range 0.8 to 1500 K
//
//    2. Test on input parameters excluded (!)
//
//    3. Dens_vsPG(double Pres, double Gibb)
//       valid for compress liquid for temperature range 0.8 to 3 K
//
//    4. Temp_vsPG(double Pres, double Gibb)
//       valid for compress liquid for temperature range 0.8 to 3 K
//
//  Exceptions:
//

#ifndef _cryoHe_SinglePhase_h
#define _cryoHe_SinglePhase_h

#include "HepackError.h"

#include "cryoHe_InitParameters.h"
#include "cryoHe_atFixedTemp.h"
#include "cryoHe_LambdaLine.h"
#include "cryoHe_ArpEquations.h"
#include "cryoHe_McCartyEquations.h"
#include "cryoHe_SaturatedBase.h"
#include "cryoHe_MeltingLine.h"

#define cryoHe_SinglePhase_meth_ 40

class cryoHe_SinglePhase
{
  public:
  
    // Constructor(s)
    cryoHe_SinglePhase();

      // (1) FUNCTION PRESS (D, T)
      // Pressure [Pa] as a function of density [kg/m3] and temperature [K]
    double Pres_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (2) SUBROUTINE DFPT (IDID, D, X, P, T)
      // density given pressure and temperature [SI units].
    double Dens_vsPT(double Pres, double Temp, HepackError_t *perr);

      // (3) Temp_vsDP(Dens, Pres)
      // Temperature given Density and Pressure [SI units].
    double Temp_vsDP(double Dens, double Pres, HepackError_t *perr);

      // (4) FUNCTION DPDT (D, T)
      // dP/dT as a function of density and temperature [SI units]
    double dPdT_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (5) FUNCTION DPDD (D, T)
      // dP/dD as a function of density and temperature [SI units]
    double dPdD_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (6) FUNCTION CV (D, T)
      // Cv as a function of density and temperature [SI units]
    double Cv_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (7) FUNCTION ENTROP (D, T)
      // Entropy as a function of density and temperature [SI units]
    double Entr_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (8) SUBROUTINE SHAUG (PRP)
      // Helmholtz energy as a function of density and temperature [SI units]
    double Helm_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (9) SUBROUTINE SHAUG (PRP)
      // Internal energy as a function of density and temperature [SI units]
    double Ener_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (10) SUBROUTINE SHAUG (PRP)
      // Gibbs energy as a function of density, temperature, and pressure [SI units]
    double Gibb_vsDTP(double Dens, double Temp, double Press, HepackError_t *perr);

      // (11) FUNCTION DTG (D)
      // Gibbs energy as a function of density and (from /SUBRT/) T. [SI units]
    double Gibb_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (12) SUBROUTINE SHAUG (PRP)
      // Enthalpy as a function of density, temperature, and pressure [SI units]
    double Enth_vsDTP(double Dens, double Temp, double Press, HepackError_t *perr);
    
      // (13) special function to use in "Properties"
      // Enthalpy as a function of density and temperature [SI units]
    double Enth_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (14) from SUBROUTINE DERIV (F, DI, TI)
      // Cp = Specific heat at constant P [J/(kG-K)] as a function of density and temperature [SI units]
    double Cp_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (15) from SUBROUTINE DERIV (F, DI, TI)
      // Gamma = Cp/Cv as a function of density and temperature [SI units]
    double Gamma_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (16) from SUBROUTINE DERIV (F, DI, TI)
      // Alpha = (T/V)(dV/dT)  at constant P as a function of density and temperature [SI units]
    double Alpha_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (17) from SUBROUTINE DERIV (F, DI, TI)
      // Grun = (V/Cv)(dP/dT) at constant V as a function of density and temperature [SI units]
    double Grun_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (18) from SUBROUTINE DERIV (F, DI, TI)
      // Kt = (1/D)(dD/dP)  at constant T [1/Pa] as a function of density and temperature [SI units]
    double Kt_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (19) from SUBROUTINE DERIV (F, DI, TI)
      // VSound (Kt) = velocity of sound [m/s] as a function of density and temperature [SI units]
    double VSound_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (20) from SUBROUTINE DERIV (F, DI, TI)
      // JouleThomson (JT) = Joule-Thomson coefficient [K/Pa] as a function of density and temperature [SI units]
    double JThomson_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (21) from SUBROUTINE DERIV (F, DI, TI)
      // V*(DH/DV) at constant P [J/kG] as a function of density and temperature [SI units]
    double VdHdV_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (22) from SUBROUTINE DTFPG (IDID, DCALC, TCALC, P, G)
      // Density and temperature as a function of pressure and Gibbs energy [SI units]
      // This subroutine valid only in the compressed liquid between 0.8 & 3 K
    double Dens_vsPG(double Pres, double Gibb, HepackError_t *perr);
    
      // (23) from SUBROUTINE DTFPG (IDID, DCALC, TCALC, P, G)
      // Density and temperature as a function of pressure and Gibbs energy [SI units]
      // This subroutine valid only in the compressed liquid between 0.8 & 3 K
    double Temp_vsPG(double Pres, double Gibb, HepackError_t *perr);

      // (24) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Density as a function of pressure and enthalpy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Dens_vsPH(double sDens, double sTemp, double Pres, double Enth, HepackError_t *perr);

      // (25) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Temperature as a function of pressure and enthalpy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Temp_vsPH(double sDens, double sTemp, double Pres, double Enth, HepackError_t *perr);

      // (26) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Density as a function of pressure and entropy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Dens_vsPS(double sDens, double sTemp, double Pres, double Entr, HepackError_t *perr);

      // (27) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Temperature as a function of pressure and entropy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Temp_vsPS(double sDens, double sTemp, double Pres, double Entr, HepackError_t *perr);

      // (28) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Density as a function of pressure and internal energy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Dens_vsPU(double sDens, double sTemp, double Pres, double Ener, HepackError_t *perr);

      // (29) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Temperature as a function of pressure and internal energy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Temp_vsPU(double sDens, double sTemp, double Pres, double Ener, HepackError_t *perr);

      // (30) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Density as a function of pressure and Gibbs energy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Dens_vsPG(double sDens, double sTemp, double Pres, double Gibb, HepackError_t *perr);

      // (31) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Temperature as a function of pressure and Gibbs energy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Temp_vsPG(double sDens, double sTemp, double Pres, double Gibb, HepackError_t *perr);

      // (32) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Density as a function of enthalpy and entropy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Dens_vsHS(double sDens, double sTemp, double Enth, double Entr, HepackError_t *perr);

      // (33) from SUBROUTINE DTILXY (D, T, J, LABEL, X, Y)
      // Recalculated Temperature as a function of enthalpy and entropy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    double Temp_vsHS(double sDens, double sTemp, double Enth, double Entr, HepackError_t *perr);
    
      // (34) from FUNCTION VISCOS (DKGM3,TK)
      // Viscosity [Pa-s] as a function of density [kg/m3] and temperature [K]
    double Visc_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      //  from SUBROUTINE AMLAP (TMINM, TMAXA, D)
      // Computes the overlap temperatures between Arp and McCarty equations
      // as a function of density [kg/m3].
      // return 0 -- use Arp equations only
      //        1 -- mixed region: use both types of equations
      //        2 -- use McCarty equations only
    int overlapTemp(double Dens, double Temp);

    // Destructor(s)
    ~cryoHe_SinglePhase();
    
    //  Limited Variables
    double lowTemp;   // low Temperature [K]
    double uppTemp;   // upper Temperature [K]
    double lowPres;   // low Pressure [Pa]
    double uppPres;   // upper Pressure [Pa]

  private:

      // Variables
    double minTempMcC;  // is the minimum temperature for McCarty (in compressed liquid)
    double maxTempArp;  // is the maximum temperature for Arp
    
      // (22,23) from SUBROUTINE DTFPG (IDID, DCALC, TCALC, P, G)
      // Density and temperature as a function of pressure and Gibbs energy [SI units]
      // This subroutine valid only in the compressed liquid between 0.8 & 3 K
    void DT_vsPG(double Pres, double Gibb, HepackError_t *perr);

      // (24,25) from SUBROUTINE DTILXY (D, T, J, 'PH', X, Y)
      // Recalculated Temperature as a function of pressure and enthalpy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    void DT_vsPH(double sDens, double sTemp, double Pres, double Enth, HepackError_t *perr);

      // (26,27) from SUBROUTINE DTILXY (D, T, J, 'PS', X, Y)
      // Recalculated Temperature as a function of pressure and entropy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    void DT_vsPS(double sDens, double sTemp, double Pres, double Entr, HepackError_t *perr);

      // (28,29) from SUBROUTINE DTILXY (D, T, J, 'PU', X, Y)
      // Recalculated Temperature as a function of pressure and internal energy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    void DT_vsPU(double sDens, double sTemp, double Pres, double Ener, HepackError_t *perr);

      // (30,31) from SUBROUTINE DTILXY (D, T, J, 'PG', X, Y)
      // Recalculated Temperature as a function of pressure and Gibbs energy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    void DT_vsPG(double sDens, double sTemp, double Pres, double Gibb, HepackError_t *perr);

      // (32,33) from SUBROUTINE DTILXY (D, T, J, 'HS', X, Y)
      // Recalculated Temperature as a function of enthalpy and entropy [SI units]
      //  sDens -- an initial estimate for density
      //  sTemp -- an initial estimate for temperature
    void DT_vsHS(double sDens, double sTemp, double Enth, double Entr, HepackError_t *perr);
    
      //  FUNCTION VISCHI (DKGM3,T)
      // Viscosity [Pa-s] as a function of density [kg/m3] and temperature [K]
      // this function is valid only for T>3.5K
    double visHi(double Dens, double Temp, HepackError_t *perr);
    
      //  FUNCTION VISLAM (DD, TT)
      // Liquid helium viscosity [Pa-s] as a function of density [kg/m3] and temperature [K]
      // this function is valid only for 1.2 <= T <= 3.8 and 132 <= D <= 180+
    double visLam(double Dens, double Temp, HepackError_t *perr);
    
      //  FUNCTION VISLPT (D, T)
      // Vapor helium viscosity [Pa-s] as a function of density [kg/m3] and temperature [K]
      // this function is valid only for T < 3.5
    double visLpt(double Dens, double Temp, HepackError_t *perr);
    
    //
    // Calculate or it was done
    int todo(int fNo, double p1);
    int todo(int fNo, double p1, double p2);
    int todo(int fNo, double p1, double p2, double p3);
    int todo(int fNo, double p1, double p2, double p3, double p4);
    //
    double val[cryoHe_SinglePhase_meth_+1][5];
    int    stt[cryoHe_SinglePhase_meth_+1];
    //
    // Classes to use
    cryoHe_ArpEquations     calcArp;
    cryoHe_McCartyEquations calcMcC;
    cryoHe_LambdaLine       lLine;
    cryoHe_atFixedTemp      fixT;
    cryoHe_SaturatedBase    sBase;
    cryoHe_MeltingLine      mLine;
};

#endif
