//
//  Class spline_Smooth
//
//  Function Interpolation by 3-Order Polynom (special thmooth algorithm)
//
//  Created by: V.Gubarev -MKS1-
//

#ifndef _spline_Smooth_h
#define _spline_Smooth_h

#include "spline.h"

class spline_Smooth: public spline
{
  public:
  
    //Constructor(s)
    spline_Smooth(int XYdim, const double *Xdata, const double *Ydata, int type=0);
          //  the "type" value (end-points definition) can be:
          //      0 -- Natural     (f''(Xo)=f''(Xn)= 0)
          //      1 -- Parabolic   (f''(Xo)=f''(X1) && f''(Xn)=f''(Xn-1))
        
    // Destructor
    ~spline_Smooth();
};

#endif
