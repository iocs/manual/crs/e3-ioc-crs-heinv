//
//  Quasi-logarithmic thermodynamic functions
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 16-May-2006
//  Modified: 
//

#include "cryoHe_quasiLogFun.h"
extern "C" {
#include "HepackError.h"
}

//  Class cryoHe_quasiLogFun
//
cryoHe_quasiLogFun::cryoHe_quasiLogFun()
{
  lg_dens = -1;
  lg_temp = -1;
}
  //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
  // Pressure [MPa] as a function of density [kg/m3] and temperature [K]
double cryoHe_quasiLogFun::Pres_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  LogFun_vsDT(Dens, Temp, perr);
  return lg_Pres;
}
  //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
  // dP/dT [Mpa/K] as a function of density [kg/m3] and temperature [K]
double cryoHe_quasiLogFun::dPdT_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  LogFun_vsDT(Dens, Temp, perr);
  return lg_dPdT;
}
  //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
  // dP/dV [MPa*kg/m3] as a function of density [kg/m3] and temperature [K]
double cryoHe_quasiLogFun::dPdV_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  LogFun_vsDT(Dens, Temp, perr);
  return lg_dPdV;
}
  //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
  // Cv [J/(kg*K)] as a function of density [kg/m3] and temperature [K]
double cryoHe_quasiLogFun::Cv_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  LogFun_vsDT(Dens, Temp, perr);
  return lg_Cv;
}
  //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
  // Entropy [J/(kg*K)] as a function of density [kg/m3] and temperature [K]
double cryoHe_quasiLogFun::Entr_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  LogFun_vsDT(Dens, Temp, perr);
  return lg_Entr;
}
  //  from SUBROUTINE LOGFUN (PVTCSA, TT, DTT, M)
  // Helmholz energy [J/kg] as a function of density [kg/m3] and temperature [K]
double cryoHe_quasiLogFun::Helm_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  LogFun_vsDT(Dens, Temp, perr);
  return lg_Helm;
}
  //
  // Destructor(s)
cryoHe_quasiLogFun::~cryoHe_quasiLogFun() {}
//
//  Private Part
//
void cryoHe_quasiLogFun::LogFun_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  if ((lg_dens != Dens)||(lg_temp != Temp)) {
    lg_dens = Dens; lg_temp = Temp;
    //
    double *c = (lline.HeState_vsDT(Dens,Temp, perr) == 1) ? PrpHeI : PrpHeII;
    double  y[9] = {-100.,0,0,0,0,0,0,0,0};
    double cl[5] = {1., -4., 12., -24., 24.};
    double p, t=lline.HeTemp_vsDT(Dens,Temp, perr), t2=t*t, dt=lline.distT_vsDT(Dens,Temp, perr), r0=t2*t2;
    double xx, xp, xt, xd, xs, xa, xr, e, ej;
    int i,j;
    //
    if (dt != 0.) {
      y[0] = log(fabs(dt));
      for (i = 1, xx = dt; i < 9; i++, xx *= dt/i) {
        y[i] = ((i > 1)&&(fabs(y[i-1]) < 1e-25)) ? 0 : (dt*y[i-1] - xx)/i;
      }
    }
    lg_Pres = c[0]*y[1] + c[1]*(t2*y[1] - 4.*t*y[2] + 6.*y[3]);
    lg_dPdV = c[0]*y[0] + c[1]*(t2*y[0] - 4.*t*y[1] + 6.*y[2]);
    lg_dPdT = c[0]*y[0] + c[1]*(t2*y[0] - 2.*t*y[1] + 2.*y[2]);
    lg_Cv   = (c[0] + c[1]*t2)*t*y[0];
    lg_Entr = c[0]*y[1] + c[1]*(t2*y[1] - 2.*t*y[2] + 2.*y[3]);
    lg_Helm = c[0]*y[2] + c[1]*(t2*y[2] - 4.*t*y[3] + 6.*y[4]);

    if (t<=0.) {HE_ERROR(DIV0) return;}

    for (i=0; i<3; i++) {
      for (j=1, xp=r0*y[i+1], xt=r0*y[i], xd=r0*y[i], xs=r0*y[i+1], xa=r0*y[i+2], xr=r0/t; j<5; j++) {
        e   = cl[j]*xr;
        ej  = e*(j+1);
        xp += ej*y[i+j+1];
        xt += e *y[i+j];
        xd += ej*y[i+j];
        xs += e* y[i+j+1];
        xa += ej*y[i+j+2];
        xr  = xr/t;
      }
      lg_Pres += c[i+2]*xp;
      lg_dPdV += c[i+2]*xd;
      lg_dPdT += c[i+2]*xt;
      lg_Cv   += c[i+2]*t*r0*y[i];
      lg_Entr += c[i+2]*xs;
      lg_Helm += c[i+2]*xa;
    }
    lg_dPdV   =  lline.dTdV_vsD(Dens, perr)*lline.dTdV_vsD(Dens, perr)*lg_dPdV - lline.d2TdV2_vsD(Dens, perr)*lg_Pres;
    lg_Pres   = -lline.dTdV_vsD(Dens, perr)*lg_Pres;
    lg_dPdT   = -lline.dTdV_vsD(Dens, perr)*lg_dPdT;
  }
}
