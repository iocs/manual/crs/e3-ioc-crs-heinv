//
//  Class spline_Classic
//
//  Function Interpolation by 3-Order Polynom (Classic approach)
//
//  Created by: V.Gubarev -MKS1-
//

#ifndef _spline_Classic_h
#define _spline_Classic_h

#include "spline.h"

class spline_Classic: public spline
{
  public:
    //
    //Constructor(s)
    spline_Classic(int XYdim, const double *Xdata, const double *Ydata, int type=0);
          //  the "type" value (end-points definition) can be:
          //      0 -- Natural     (f''(Xo)=f''(Xn)= 0)
          //      1 -- Parabolic   (f''(Xo)=f''(X1) && f''(Xn)=f''(Xn-1))
          //      2 -- Runout      (f''(Xo)=2f''(X1)-f''(X2) && f''(Xn)=2f''(Xn-1)-f''(Xn-2)
    //
    // Destructor
    ~spline_Classic();
};

#endif
