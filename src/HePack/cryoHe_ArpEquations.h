//
//  He calculations via V.Arp equations
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 16-May-2006
//  Modified: 
//
//  Notes:
//    1. Valid in compressed liquid from 0.8 to about 3 K.
//    2. Test on input parameters excluded (!)
//
//  Exceptions:
//

#ifndef _cryoHe_ArpEquations_h
#define _cryoHe_ArpEquations_h

#include "HepackError.h"

#include "cryoHe_quasiLogFun.h"
#include "cryoHe_atFixedTemp.h"
#include "cryoHe_InitParameters.h"

#define cryoHe_ArpEquations_meth_ 7

class cryoHe_ArpEquations
{
  public:
    // Constructor(s)
    cryoHe_ArpEquations();

      // (1) FUNCTION PRESSA (D, TT)
      // Pressure [Pa] as a function of density [kg/m3] and temperature [K]
    double Pres_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (2) FUNCTION DPDTA (D, TT)
      // dP/dT as a function of density and temperature [SI units]
    double dPdT_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (3) FUNCTION DPDDA (D, TT)
      // dP/dD as a function of density and temperature [SI units]
    double dPdD_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (4) FUNCTION CVA (D, TT)
      // Cv as a function of density and temperature [SI units]
    double Cv_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (5) FUNCTION ENTRA (D, TT)
      // Entropy as a function of density and temperature [SI units]
    double Entr_vsDT(double Dens, double Temp, HepackError_t *perr);
    
      // (6) FUNCTION HELMA (D, TT)
      // Helmholtz energy as a function of density and temperature [SI units]
    double Helm_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (7) SUBROUTINE DF2PT (IDID, D, PP, TT)
      // This subroutine iterates for density D [kg/m3], given P [Pa] and T [K].
      // It is valid only in the compressed liquid near the lambda line.
    double Dens_vsPT(double Pres, double Temp, HepackError_t *perr);

    // Destructor(s)
    ~cryoHe_ArpEquations();
    
  private:
    //
    // Calculate or its was done
    int todo(int fNo, double p1);
    int todo(int fNo, double p1, double p2);
    //
    double val[cryoHe_ArpEquations_meth_+1][3];
    int    stt[cryoHe_ArpEquations_meth_+1];
    
    // Objects to use
    cryoHe_quasiLogFun logFun;
    cryoHe_LambdaLine  lline;
    cryoHe_atFixedTemp fixT;
};

#endif
