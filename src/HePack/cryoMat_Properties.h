//
//  Class cryoMat_Properties
//
//  Material Cryo Properties
//
//  Created by: V.Gubarev -MKS1-
//
#ifndef _cryoMat_Properties_h
#define _cryoMat_Properties_h

#include <iostream>
#include <cstring>
#include <cmath>

using namespace std;

#include "HepackError.h"

#include "cryoMat_Collection.h"
#include "spline_Smooth.h"
#include "spline_Classic.h"

class cryoMat_Properties
{
  public:
  
    // Constructor(s)
    cryoMat_Properties(int matNo, double pRRR=0, double pTESLA=0);
    
    // Information
    int   materialNo();             //  returns Material Number
    char *materialName();           //  returns "new" char array !!!
    char *materialName(int matNo);  //  returns "new" char array !!!
    char *materialDescr();          //  returns "new" char array !!!
    char *materialDescr(int matNo); //  returns "new" char array !!!
    //
    void printName();
    void printName(int matNo);
    void printDescr();
    void printDescr(int matNo);
    
    // Calculations
    double CpAt(double t)      const; //STATE(CP,...,T)
    double ConductAt(double t) const; //STATE(..,COND,...,T)
    double DiffAt(double t)    const; //STATE(..,DIFF,...,T)
    double ExpansAt(double t)  const; //STATE(..,EX,...,T)
    double ResistAt(double t)  const; //STATE(..,RESIS,...,T)
    //
    double EnthAt(double t) const;  //TRANS(H,,T)
    double IKdtAt(double t) const;  //TRANS(,IKDT,,T)
    double DelLAt(double t) const;  //TRANS(,,L,T)
    //
    double diffEnth(double t1, double t2) const;  //INTGRL(DELH,,T1,T2)
    double diffIKdt(double t1, double t2) const;  //INTGRL(,IKDT,,T1,T2)
    double diffDelL(double t1, double t2) const;  //INTGRL(,,DELL,T1,T2)
    //
    double designPCT(double rlen, double area, double t1, double t2)  const;  //DESIGN(PCT,,,,,T1,T2)
    double designWatt(double rlen, double area, double t1, double t2) const;  //DESIGN(,WATTS,,,,T1,T2)
    double designOhm(double rlen, double area, double t1, double t2)  const;  //DESIGN(,,OHMS,,,T1,T2)
    double designDelH(double rlen, double area, double t1, double t2) const;  //DESIGN(,,,DELH,,T1,T2)
    double designDelL(double rlen, double area, double t1, double t2) const;  //DESIGN(,,,,DELL,T1,T2)
    
    // Destructor
    ~cryoMat_Properties();
    
  protected:
    //
    int material;
    //
    int metal;
    //
    int maskCp;
    int maskConduct;
    int maskExpans;
    int maskResist;
    //
    double density;
    double valRRR;
    double valTESLA;
    //
    spline *funCp;
    spline *funConduct;
    spline *funExpans;
    spline *funResist;
    //
    spline *funEnth;
    spline *funRIKdt;
    spline *funDeltaL;

  private:
    //
    //  Thermal Conductivity of copper [W/m.K]
    double ThermConductCu(double pT,    //Temperature [K]
                          double pRRR); //RRR, R(273.15)/R(4)
    //
    //  Electrical Resistivity of copper [Ohm.m]
    double ElectrResistCu(double pT,    //Temperature [K]
                          double pRRR); //RRR, R(273.15)/R(4)
    //
    //  Transverse magnetoresistivity factor of copper, resistivity at field pFLD,
    //  divided by resistivity at zero field
    double MagnResistFactCu(double pT,    //Temperature [K]
                            double pRRR,  //RRR, R(273.15)/R(4)
                            double pFLD); //Magnetic Field [Tesla]
    //
    //  Thermal Conductivity of aluminum [W/m.K]
    double ThermConductAl(double pT,    //Temperature [K]
                          double pRRR); //RRR, R(273.15)/R(4)
    //
    //  Electrical Resistivity of aluminum [Ohm.m]
    double ElectrResistAl(double pT,    //Temperature [K]
                          double pRRR); //RRR, R(273.15)/R(4)
    //
    //  Transverse magnetoresistivity factor of aluminum, resistivity at field pFLD,
    //  divided by resistivity at zero field
    double MagnResistFactAl(double pT,    //Temperature [K]
                            double pRRR,  //RRR, R(273.15)/R(4)
                            double pFLD); //Magnetic Field [Tesla]
};
    
#endif

