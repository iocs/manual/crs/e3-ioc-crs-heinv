#include "cryoHe_SinglePhase.h"
#include "cryoHe_SaturatedVapor.h"
#include "cryoHe_SaturatedLiquid.h"

// Wrapper function for C interface

// ----------------------------------------------------------------------
//Class cryoHe_SinglePhase
extern "C"
        double cryoHe_SinglePhase_Dens_vsPT
         (cryoHe_SinglePhase* p, double _press, double _temp, HepackError_t *perr)
          { return p->Dens_vsPT (_press, _temp, perr); }
extern "C"
	double cryoHe_SinglePhase_Enth_vsDTP
	 (cryoHe_SinglePhase* p, double _dens, double _temp, double _press, HepackError_t *perr)
          { return p->Enth_vsDTP (_dens, _temp, _press, perr); }
extern "C"
        double cryoHe_SinglePhase_Entr_vsDT
         (cryoHe_SinglePhase* p, double _dens, double _temp, HepackError_t *perr)
          { return p->Entr_vsDT(_dens, _temp, perr); }

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
extern "C"
	cryoHe_SinglePhase* Construct_cryoHe_SinglePhase (void)
	  { return new cryoHe_SinglePhase; }
extern "C"
	void Destruct_cryoHe_SinglePhase (cryoHe_SinglePhase* p)
	  { delete p; return; }

// ----------------------------------------------------------------------
//Class cryoHe_SaturatedVapor
extern "C"
	double cryoHe_SaturatedVapor_Temp_vsP
	 (cryoHe_SaturatedVapor* p, double _press, HepackError_t *perr)
	  { return p->Temp_vsP (_press, perr); }
extern "C"
	double cryoHe_SaturatedVapor_Enth_vsT
	 (cryoHe_SaturatedVapor* p, double _temp, HepackError_t *perr)
	  { return p->Enth_vsT (_temp, perr); }
extern "C"
	double cryoHe_SaturatedVapor_Entr_vsT
	 (cryoHe_SaturatedVapor* p, double _temp, HepackError_t *perr)
	  { return p->Entr_vsT (_temp, perr); }
extern "C"
	double cryoHe_SaturatedVapor_Pres_vsT
	 (cryoHe_SaturatedVapor* p, double _temp, HepackError_t *perr)
	  { return p->Pres_vsT (_temp, perr); }
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
extern "C"
	cryoHe_SaturatedVapor* Construct_cryoHe_SaturatedVapor (void)
	  { return new cryoHe_SaturatedVapor; }
extern "C"
	void Destruct_cryoHe_SaturatedVapor (cryoHe_SaturatedVapor* p)
	  { delete p; return; }

// ----------------------------------------------------------------------
//Class cryoHe_SaturatedLiquid
extern "C"
	double cryoHe_SaturatedLiquid_Temp_vsP
	 (cryoHe_SaturatedLiquid* p, double _press, HepackError_t *perr)
	  { return p->Temp_vsP (_press, perr); }
extern "C"
	double cryoHe_SaturatedLiquid_Enth_vsT
	 (cryoHe_SaturatedLiquid* p, double _temp, HepackError_t *perr)
	  { return p->Enth_vsT (_temp, perr); }
extern "C"
	double cryoHe_SaturatedLiquid_Entr_vsT
	 (cryoHe_SaturatedLiquid* p, double _temp, HepackError_t *perr)
	  { return p->Entr_vsT (_temp, perr); }
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
extern "C"
	cryoHe_SaturatedLiquid* Construct_cryoHe_SaturatedLiquid (void)
	  { return new cryoHe_SaturatedLiquid; }
extern "C"
	void Destruct_cryoHe_SaturatedLiquid (cryoHe_SaturatedLiquid* p)
	  { delete p; return; }

// ----------------------------------------------------------------------
// Class
