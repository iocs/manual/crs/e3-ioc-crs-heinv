#include "cryoMat_Properties.h"
//
//  Class cryoMat_Properties
//
//  Calculation of Cryogenic Material Properties
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 11-Feb-2005
//  Modified: 
//

// Constructor(s)
// ==============
cryoMat_Properties::cryoMat_Properties(int matNo, double pRRR, double pTESLA)
  :material(matNo), valRRR(pRRR), valTESLA(pTESLA)
{
  if ((matNo < 1)||(matNo > cryoMat_CollectionSize)) {
    cerr << "cryoMat_Properties:: - material "<<matNo<<" is not exist"<< endl;
    exit(1);
  }
  //
  MatCryoDef DefData = CryoDefArray[matNo-1];
  double *tmpEnth, *tmpRIKdt, *tmpDeltaL;
  double fact,t, t0, t1;
  int i;
  //
  //  Save parameters from definition
  metal       = DefData.metal;
  density     = (1e+3)*DefData.density;
  maskCp      = DefData.maskCp;
  maskConduct = DefData.maskConduct;
  maskExpans  = DefData.maskExpans;
  maskResist  = DefData.maskResist;
  //
  //  Convert to SI units
  for (i=0; i<DefData.dataDim; i++) {
    DefData.dataCp[i]      *= 1e+3;
    DefData.dataConduct[i] *= 1e+2;
    DefData.dataResist[i]  *= 10;
  }
  //
  //  Special case (Material Cu and Al)
  valRRR = (((material <= 2)&&(valRRR == 0)) ? 1 : valRRR);
  valTESLA = (((material <= 2)&&(valTESLA == 0)) ? 1 : valTESLA);
  //
  if (material == 1) {
    for (i=0; i<DefData.dataDim; t=DefData.dataTK[i],i++) {
      fact = MagnResistFactCu(t,valRRR,valTESLA);
      DefData.dataResist[i]  = fact*1000*ElectrResistCu(t,valRRR);
      DefData.dataConduct[i] = (fact!=0.) ? ThermConductCu(t,valRRR)/fact : 0.;
    }
  }
  else if (material == 2) {
    for (i=0; i<DefData.dataDim; t=DefData.dataTK[i],i++) {
      fact = MagnResistFactAl(t,valRRR,valTESLA);
      DefData.dataResist[i]  = fact*1000*ElectrResistAl(t,valRRR);
      DefData.dataConduct[i] = (fact!=0.) ? ThermConductAl(t,valRRR)/fact : 0.;
    }
  }
  //
  //  Convert Resistivity into SI units
  if (maskResist == 2) {
    for (i=0; i<DefData.dataDim; i++) DefData.dataResist[i] *= 1e+6;
  }
  //
  //  Make interpolation splines
  funCp      = new spline_Classic(DefData.dataDim,DefData.dataTK,DefData.dataCp);      // Heat Capacity
  funConduct = new spline_Classic(DefData.dataDim,DefData.dataTK,DefData.dataConduct); // Thermal Conductivity
  funExpans  = new spline_Smooth(DefData.dataDim,DefData.dataTK,DefData.dataExpans);   // Material Expansivity
  funResist  = new spline_Smooth(DefData.dataDim,DefData.dataTK,DefData.dataResist);   // Electrical Resistivity
  //
  //  Obtain Enthalpy(i) from Cp, IKDT(i) from Conduct, DeltaL from Expans
  tmpEnth   = new double[DefData.dataDim];
  tmpRIKdt  = new double[DefData.dataDim];
  tmpDeltaL = new double[DefData.dataDim];
  //
  for (i=0,t0=DefData.dataTK[0]; i<DefData.dataDim; i++) {
    tmpEnth[i]   = (maskCp == 0 ? 0 : funCp->Integral(t0,DefData.dataTK[i]));
    tmpRIKdt[i]  = funConduct->Integral(t0,DefData.dataTK[i]);
    tmpDeltaL[i] = (maskExpans == 2 ? funExpans->Integral(t0,DefData.dataTK[i]) : DefData.dataExpans[i]);
  }
  //
  //  Make interpolation splines
  funEnth   = new spline_Classic(DefData.dataDim,DefData.dataTK,tmpEnth);    //  Enthalpy as f(T) with P=const
  funRIKdt  = new spline_Classic(DefData.dataDim,DefData.dataTK,tmpRIKdt);   //  
  funDeltaL = new spline_Classic(DefData.dataDim,DefData.dataTK,tmpDeltaL);  //
  //
  //
  delete [] tmpEnth;
  delete [] tmpRIKdt;
  delete [] tmpDeltaL;
}

// Methods
// =======
//
//  Informational
//  -------------
//materialNo()
int cryoMat_Properties::materialNo()
{
  return( material );
}
//materialName()
char * cryoMat_Properties::materialName()
{
  return( materialName(material) );
}
//materialName(int matNo)
char * cryoMat_Properties::materialName(int matNo)
{
  if ((matNo < 1)||(matNo > cryoMat_CollectionSize)) {
    cerr << "cryoMat_Properties:: - material "<<matNo<<" is not exist"<< endl;
    exit(1);
  }
  //
  MatCryoDef DefData = CryoDefArray[matNo-1];
  char *tmpbuf = new char[strlen(DefData.name)+1];
  return ( strcpy(tmpbuf,DefData.name) );
}
//materialDescr()
char * cryoMat_Properties::materialDescr()
{
  return( materialDescr(material) );
}
//materialDescr(int matNo)
char * cryoMat_Properties::materialDescr(int matNo)
{
  if ((matNo < 1)||(matNo > cryoMat_CollectionSize)) {
    cerr << "cryoMat_Properties:: - material "<<matNo<<" is not exist"<< endl;
    exit(1);
  }
  //
  MatCryoDef DefData = CryoDefArray[matNo-1];
  char *tmpbuf = new char[strlen(DefData.descr)+1];
  return ( strcpy(tmpbuf,DefData.descr) );
}
//printName()
void cryoMat_Properties::printName()
{
  printName(material);
}
//printName(int matNo)
void cryoMat_Properties::printName(int matNo)
{
  if ((matNo < 1)||(matNo > cryoMat_CollectionSize)) {
    cerr << "cryoMat_Properties:: - material "<<matNo<<" is not exist"<< endl;
    exit(1);
  }
  //
  cout<<CryoDefArray[matNo-1].name<<endl;
}
//printDescr()
void cryoMat_Properties::printDescr()
{
  printDescr(material);
}
//printDescr(int matNo)
void cryoMat_Properties::printDescr(int matNo)
{
  if ((matNo < 1)||(matNo > cryoMat_CollectionSize)) {
    cerr << "cryoMat_Properties:: - material "<<matNo<<" is not exist"<< endl;
    exit(1);
  }
  //
  cout<<CryoDefArray[matNo-1].descr<<endl;
}
//
//  Calculations
//  ------------
//STATE(CP,...,T)
double cryoMat_Properties::CpAt(double t) const
{
  return( maskCp == 0 ? 0 : (*funCp)(t) );
}
//STATE(..,COND,...,T)
double cryoMat_Properties::ConductAt(double t) const
{
  return( (*funConduct)(t) );
}
//STATE(..,DIFF,...,T)
double cryoMat_Properties::DiffAt(double t) const
{
  return( maskCp == 0 ? 0 : ((*funConduct)(t))/((*funCp)(t))/density );
}
//STATE(..,EX,...,T)
double cryoMat_Properties::ExpansAt(double t) const
{
  return( maskExpans != 2 ? 0 : (*funExpans)(t) );
}
//STATE(..,RESIS,...,T)
double cryoMat_Properties::ResistAt(double t) const
{
  return( maskResist == 0 ? 0 : (*funResist)(t) );
}
//TRANS(H,,T)
double cryoMat_Properties::EnthAt(double t) const
{
  return( (*funEnth)(t) );
}
//TRANS(,IKDT,,T)
double cryoMat_Properties::IKdtAt(double t) const
{
  return( (*funRIKdt)(t) );
}
//TRANS(,,L,T)
double cryoMat_Properties::DelLAt(double t) const 
{
  return( maskExpans == 0 ? 0 : (*funDeltaL)(t) );
}
//INTGRL(DELH,,T1,T2)
double cryoMat_Properties::diffEnth(double t1, double t2) const
{
  return( maskCp == 0 ? 0 : EnthAt(t2) - EnthAt(t1) );
}
//INTGRL(,IKDT,,T1,T2)
double cryoMat_Properties::diffIKdt(double t1, double t2) const
{
  return (IKdtAt(t2) - IKdtAt(t1));
}
//INTGRL(,,DELL,T1,T2)
double cryoMat_Properties::diffDelL(double t1, double t2) const
{
  return( maskExpans == 0 ? 0 : DelLAt(t2) - DelLAt(t1) );
}
//DESIGN(PCT,,,,,T1,T2) --  Calculate closing error
double cryoMat_Properties::designPCT(double rlen, double area, double t1, double t2) const
{
  int i=0, N=20;
  double watts = (IKdtAt(t2) - IKdtAt(t1))*area/rlen;
  double dt = (t2-t1)/N, sX=0, t=t1;
  //
  for (; i<N; i++,t+=dt) sX += IKdtAt(t+dt) - IKdtAt(t);
  sX *= area/watts;
  return( (sX - rlen)*100/rlen );
}
//DESIGN(,WATTS,,,,T1,T2)
double cryoMat_Properties::designWatt(double rlen, double area, double t1, double t2) const
{
  return ( (IKdtAt(t2) - IKdtAt(t1))*area/rlen );
}
//DESIGN(,,OHMS,,,T1,T2)  --  Electrical resistance (note: resistivity is in nano-ohm-m)
double cryoMat_Properties::designOhm(double rlen, double area, double t1, double t2) const
{
  int i=0, N=20;
  double watts = (IKdtAt(t2) - IKdtAt(t1))*area/rlen;
  double dt=(t2-t1)/N, sOhm=0, t=t1;
  //
  for (; i<N; i++,t+=dt) sOhm += ResistAt(t+dt/2)*(IKdtAt(t+dt) - IKdtAt(t));
  return ( 1e-9*sOhm/watts );
}
//DESIGN(,,,DELH,,T1,T2)  --  Heat removed (Joules) on cooldown from T2
double cryoMat_Properties::designDelH(double rlen, double area, double t1, double t2) const
{
  int i=0, N=20;
  double watts = (IKdtAt(t2) - IKdtAt(t1))*area/rlen;
  double dt=(t2-t1)/N, sH=0, t=t1;
  //
  for (; i<N; i++,t+=dt) sH += EnthAt(t+dt/2)*(IKdtAt(t+dt) - IKdtAt(t));
  sH *= area/watts;
  return ( (EnthAt(t2)*rlen - sH)*density*area );
}
//DESIGN(,,,,DELL,T1,T2)  --  Net contraction relative to T2
double cryoMat_Properties::designDelL(double rlen, double area, double t1, double t2) const
{
  int i=0, N=20;
  double watts = (IKdtAt(t2) - IKdtAt(t1))*area/rlen;
  double dt=(t2-t1)/N, sL=0, t=t1;
  //
  for (; i<N; i++,t+=dt) sL += DelLAt(t+dt/2)*(IKdtAt(t+dt) - IKdtAt(t));
  sL *= area/watts;
  return ( 0.01*(DelLAt(t2)*rlen - sL) );
}

// Destructor
// ==========
cryoMat_Properties::~cryoMat_Properties()
{
  if (material != 0) {
    delete funCp;
    delete funConduct;
    delete funExpans;
    delete funResist;
    delete funEnth;
    delete funRIKdt;
    delete funDeltaL;
  }
}

// Private Part
// ============
//
//  Thermal Conductivity of copper [W/m.K]
double cryoMat_Properties::ThermConductCu(double pT, double pRRR)
{
  double tMin = 0.1,   tMax = 1000.;
  double rrrMin = 10., rrrMax = 3000.;
  double rho273 = 15.4e-9;
  double el0 = 2.443e-8;
  double p1=1.754e-8, p2=2.763, p3=1102., p4=-0.165, p5=70.0, p6=1.765, p7;
  double arga,argb,argc,arg1,w0,wc,wi,wi0,beta;
  double result = 0;
  //
  if ((pT>=tMin)&&(pT<=tMax)&&(pRRR>=rrrMin)&&(pRRR<=rrrMax)) {
    //
    beta = rho273/(pRRR - 1)/el0;
    p7 = 0.838/pow((beta/0.0003),0.1661);
    arga = pow((log(pT/470)/0.7),2); if (arga > 30.) arga = 30.;
    argb = pow((log(pT/87)/0.45),2); if (argb > 30.) argb = 30.;
    argc = pow((log(pT/21)/0.5),2);  if (argc > 30.) argc = 30.;
    arg1 = pow((p5/pT),p6);          if (arg1 > 30.) arg1 = 30.;
    //
    w0 = beta/pT;
    wc = -0.00012*log(pT/420)*exp(-arga) - 0.00016*log(pT/73)*exp(-argb) - 0.00002*log(pT/18)*exp(-argc);
    wi = p1*pow(pT,p2)/(1.+p1*p3*pow(pT,(p2+p4))*exp(-arg1))+wc;
    wi0=p7*wi*w0/(wi+w0);
    result=1./(w0+wi+wi0);
  }
  return result;
}
//
//  Electrical Resistivity of copper [Ohm.m]
double cryoMat_Properties::ElectrResistCu(double pT, double pRRR)
{
  double tMin = 0.1,   tMax = 1000.;
  double rrrMin = 10., rrrMax = 3000.;
  double rho273=15.4e-9;
  double p1=0.1171e-16, p2=4.49, p3=3.841e+10, p4=-1.14, p5=50.0, p6=6.428, p7=0.4531;
  double arg1,rho0,rhoc,rhoi,rhoi0;
  double result = 0;
  //
  if ((pT>=tMin)&&(pT<=tMax)&&(pRRR>=rrrMin)&&(pRRR<=rrrMax)) {
    //
    arg1  = pow((p5/pT),p6); if (arg1 > 30.) arg1 = 30.;
    rho0  = rho273/(pRRR - 1);
    rhoc  = 0;
    rhoi  = p1*pow(pT,p2)/(1.+p1*p3*pow(pT,(p2+p4))*exp(-arg1))+rhoc;
    rhoi0 = p7*rhoi*rho0/(rhoi+rho0);
    result=rho0 + rhoi + rhoi0;
  }
  return result;
}
//
//  Transverse magnetoresistivity factor of copper, resistivity at field pFLD,
//  divided by resistivity at zero field
double cryoMat_Properties::MagnResistFactCu(double pT, double pRRR, double pFLD)
{
  double tMin = 0.1,   tMax = 1000.;
  double rrrMin = 10., rrrMax = 3000.;
  double fldMin = 0.0, fldMax = 30.;
  double tIce = 273.15;
  double brrMin = 0.0, brrMax = 40.e+3;
  double a=0.382806e-03, b=0.132407e+01, c=0.167634e-02, d=0.789953e+00;
  double brr = 0, coldResCu = 0, rho, rhoIce;
  double result = 0;
  //
  if ((pT>=tMin)&&(pT<=tMax)&&(pRRR>=rrrMin)&&(pRRR<=rrrMax)&&(pFLD>=fldMin)&&(pFLD<=fldMax)) {
    //
    rho = ElectrResistCu(pT,pRRR);
    rhoIce = ElectrResistCu(tIce,pRRR);
    if (rho > 0) brr = pFLD*rhoIce/rho;
    if ((brr>=brrMin)&&(brr<=brrMax)) {
      if (brr > 1) coldResCu = a*pow(brr,b)/(1.0+c*pow(brr,d));
      result = coldResCu +1.;
    }
  }
  return result;
}
//
//  Thermal Conductivity of aluminum [W/m.K]
double cryoMat_Properties::ThermConductAl(double pT, double pRRR)
{
  double tMin = 0.1,   tMax = 900.;
  double rrrMin = 10., rrrMax = 30000.;
  double rho273 = 24.8e-9;
  double el0 = 2.443e-8;
  double p1=4.716e-8, p2=2.446, p3=623.6, p4=-0.16, p5=130.9, p6=2.5, p7;
  double arga,argb,arg1,w0,wc,wi,wi0,beta;
  double result = 0;
  //
  if ((pT>=tMin)&&(pT<=tMax)&&(pRRR>=rrrMin)&&(pRRR<=rrrMax)) {
    //
    beta = rho273/(pRRR - 1)/el0;
    p7 = 0.8168/pow((beta/0.0003),0.1661);
    arga = pow((log(pT/380)/0.6),2); if (arga > 30.) arga = 30.;
    argb = pow((log(pT/94)/0.5),2);  if (argb > 30.) argb = 30.;
    arg1 = pow((p5/pT),p6);          if (arg1 > 30.) arg1 = 30.;
    //
    w0 = beta/pT;
    wc = -0.0005*log(pT/330)*exp(-arga) - 0.0013*log(pT/110)*exp(-argb);
    wi = p1*pow(pT,p2)/(1.+p1*p3*pow(pT,(p2+p4))*exp(-arg1))+wc;
    wi0=p7*wi*w0/(wi+w0);
    result=1./(w0+wi+wi0);
  }
  return result;
}
//
//  Electrical Resistivity of aluminum [Ohm.m]
double cryoMat_Properties::ElectrResistAl(double pT, double pRRR)
{
  double tMin = 0.1,   tMax = 900.;
  double rrrMin = 10., rrrMax = 30000.;
  double rho273=24.8e-9;
  double p1=0.09052e-16, p2=4.551, p3=5.173e+10, p4=-1.26, p5=13.64, p6=1.0, p7=0.7416;
  double arg1,rho0,rhoc,rhoi,rhoi0;
  double result = 0;
  //
  if ((pT>=tMin)&&(pT<=tMax)&&(pRRR>=rrrMin)&&(pRRR<=rrrMax)) {
    //
    arg1  = pow((p5/pT),p6); if (arg1 > 30.) arg1 = 30.;
    rho0  = rho273/(pRRR - 1);
    rhoc  = 0;
    rhoi  = p1*pow(pT,p2)/(1.+p1*p3*pow(pT,(p2+p4))*exp(-arg1))+rhoc;
    rhoi0 = p7*rhoi*rho0/(rhoi+rho0);
    result=rho0 + rhoi + rhoi0;
  }
  return result;
}
//
//  Transverse magnetoresistivity factor of aluminum, resistivity at field pFLD,
//  divided by resistivity at zero field
double cryoMat_Properties::MagnResistFactAl(double pT, double pRRR, double pFLD)
{
  double tMin = 0.1,   tMax = 900.;
  double rrrMin = 10., rrrMax = 30000.;
  double fldMin = 0.0, fldMax = 10.;
  double tIce = 273.15;
  double brrMin = 0.0, brrMax = 400000;
  double g1 = 0.116587e-04, g2 = 0.776391e-01, g3 = 0.687100e+00, g4 = 0.536580e-05;
  double g5 = 0.135597e+01, g6 = 0.534860e-05, g7 = 0.181991e+01;
  double brr = 0, rho, rhoIce;
  double result = 0;
  //
  if ((pT>=tMin)&&(pT<=tMax)&&(pRRR>=rrrMin)&&(pRRR<=rrrMax)&&(pFLD>=fldMin)&&(pFLD<=fldMax)) {
    //
    rho = ElectrResistAl(pT,pRRR);
    rhoIce = ElectrResistAl(tIce,pRRR);
    if (rho > 0) brr = pFLD*rhoIce/rho;
    if ((brr>=brrMin)&&(brr<=brrMax)) {
      result = g1*pow((rhoIce/rho),(g2*pow(pT,g3)))*pow(brr,g7)*(1+g4*brr)/(pow(pT,g5)*(1+g6*pow(brr,g7))) +1;
    }
  }
  return result;
}
//
//  a smallbit theory
//
//  Cp -- Heat Capacity whith P=const (pressure)
//    Enthalpy H(T)/dT = Cp(T) with condition H(0) = 0
//
//  The thermal conductivity of a material is the quantity of heat in Watt/sec passing through a body
//  1 m thick with a cross section of 1 sq. m when the temperature difference between the hot and cold
//  sides of the body is 1 deg.
//    dQ/dt = -Conduct(T)*area*dT/dx
//
