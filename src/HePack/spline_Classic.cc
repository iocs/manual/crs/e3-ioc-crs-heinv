#include "spline_Classic.h"

//
//  Class spline_Classic
//
//  Function Interpolation by 3-Order Classic Spline
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 31-Jan-2005
//  Modified: 
//

// Constructor(s)
// ==============
spline_Classic::spline_Classic(int XYdim, const double *Xdata, const double *Ydata, int type) : spline(XYdim-1,Xdata[0],Xdata[XYdim-1])
{
  int i, nU = nUnits, nP = XYdim;
  double dx,c,d, A,B,C,D;   //  S = A + B*x + C*x*x + D*x*x*x
  //
  //  Threediagonal Matrix
  double *uA = new double[nP]; // upper diagonal
  double *cA = new double[nP]; // center diagonal
  double *dA = new double[nP]; // down diagonal
  for (i=2; i<nP-2; i++) {
    uA[i]   =  Xdata[i+1] - Xdata[i];
    cA[i]   = (Xdata[i+1] - Xdata[i-1])*2;
    dA[i-1] =  Xdata[i]   - Xdata[i-1];
  }
  //  Right part Vector for Matrix equation (to be solved for points 1..nP-2)
  double *vC = new double[nP];
  for (i=1; i<nP-1; i++) vC[i] = ((Ydata[i+1]-Ydata[i])/(Xdata[i+1]-Xdata[i]) - (Ydata[i]-Ydata[i-1])/(Xdata[i]-Xdata[i-1]))*3;
  //
  //  Definition of the end points depending on Spline Type
  switch (type) {
    case 0:     // Natural spline: S''(Xo) = S''(Xn) = 0
      uA[1] = Xdata[2] - Xdata[1];
      cA[1] = (Xdata[2] - Xdata[0])*2;
      uA[nP-2] = 0;
      cA[nP-2] = (Xdata[nP-1] - Xdata[nP-3])*2;
      dA[nP-3] =  Xdata[nP-2] - Xdata[nP-3];
      vC[0]    = 0;
      vC[nP-1] = 0;
      break;
    case 1:     // Parabolic runout spline: S''(Xo) = S''(X1) && S''(Xn) = S''(Xn-1)
      uA[1] = Xdata[2] - Xdata[1];
      cA[1] = (3*(Xdata[1]-Xdata[0])/2 + Xdata[2]-Xdata[1])*2;
      uA[nP-2] = 0;
      cA[nP-2] = (3*(Xdata[nP-2]-Xdata[nP-3])/2 + Xdata[nP-1]-Xdata[nP-2])*2;
      dA[nP-3] =  Xdata[nP-2] - Xdata[nP-3];
      vC[0]    = vC[1];
      vC[nP-1] = vC[nP-2];
      break;
    case 2:     // Runout spline
      uA[1] = Xdata[2] - Xdata[0];
      cA[1] = (2*(Xdata[1]-Xdata[0]) + Xdata[2]-Xdata[1])*2;
      uA[nP-2] = 0;
      cA[nP-2] = (2*(Xdata[nP-2]-Xdata[nP-3]) + Xdata[nP-1]-Xdata[nP-2])*2;
      dA[nP-3] =  Xdata[nP-1] - Xdata[nP-3];
      vC[0]    = 2*vC[1] - vC[2];
      vC[nP-1] = 2*vC[nP-2] - vC[nP-3];
      break;
    default:
      printf("Classic Spline - Unknown type = %d\n",type);
      exit(1);
  }
  //  Solve SLE with 3-diagonal matrix
  for (i=2; i<=nP-2; i++) {
    c = cA[i-1]; d = dA[i-1];
    cA[i] = cA[i]/d - uA[i-1]/c;
    uA[i] = uA[i]/d;
    vC[i] = vC[i]/d - vC[i-1]/c;
  }
  vC[nP-2] = vC[nP-2]/cA[nP-2];
  for (i=nP-3; i>=1; i--) vC[i] = (vC[i] - vC[i+1]*uA[i])/cA[i];
  //
  for (i=0; i<nU; i++) {
    dx = Xdata[i+1]-Xdata[i];
    A = Ydata[i];
    B = (Ydata[i+1]-Ydata[i])/dx - (2*vC[i]+vC[i+1])*dx/3;
    C = vC[i];
    D = (vC[i+1] - vC[i])/dx/3;
    Unit[i].init(Xdata[i],Xdata[i+1],A,B,C,D);
  }
  //
  delete [] uA; delete [] cA; delete [] dA; delete [] vC;
}

// Destructor
// ==========
spline_Classic::~spline_Classic() {}

