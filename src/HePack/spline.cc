#include "spline.h"

//
//  Class spline
//
//  Function Interpolation by 3-Order Polynom
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 31-Jan-2005
//  Modified: 
//

// Constructor(s)
// ==============
spline::spline(int nIntervals, double x0, double x1) : Xo(x0), Xn(x1), nUnits(nIntervals)
{
  Unit = new splineUnit[nUnits];
}

// Operators
// =========
double spline::operator () (double x) const
{
  return at(x);
}

// Methods

int spline::xIn(double x) const
{
  return((x - Xo)*(x - Xn) <= 0);
}
double spline::at(double x) const
{
  if (xIn(x)) { for (int i=0; ;i++) if (Unit[i].xIn(x)) return Unit[i].at(x); }
  return ((Xo - x)*(Xo - Xn) < 0)?Unit[0].at(x):Unit[nUnits-1].at(x);
}

double spline::dF(double x) const
{
  if (xIn(x)) { for (int i=0; ;i++) if (Unit[i].xIn(x)) return Unit[i].dF(x); }
  return ((Xo - x)*(Xo - Xn) < 0)?Unit[0].dF(x):Unit[nUnits-1].dF(x);
}

double spline::d2F(double x) const
{
  if (xIn(x)) { for (int i=0; ;i++) if (Unit[i].xIn(x)) return Unit[i].d2F(x); }
  return ((Xo - x)*(Xo - Xn) < 0)?Unit[0].d2F(x):Unit[nUnits-1].d2F(x);
}

double spline::Integral(double x0, double x1) const
{
  double out = 0;
  //
  for (int i=0; i<nUnits; i++) out += Unit[i].Integral(x0,x1);
  return out;
}

// Destructor
// ==========
spline::~spline()
{
  delete [] Unit;
}

