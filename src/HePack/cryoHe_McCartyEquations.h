//
//  HeI calculations for temperature and density via McCarty equations
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 19-May-2006
//  Modified: 
//
//  Notes:
//    1. Valid for temperature range 2 to 1500 K (2-phase region excluded)
//    2. Test on input parameters excluded (!)
//

#ifndef _cryoHe_McCartyEquations_h
#define _cryoHe_McCartyEquations_h

#include <math.h>
#include <iostream>

#include "HepackError.h"

#include "cryoHe_InitParameters.h"

#define cryoHe_McCartyEquations_meth_ 6

class cryoHe_McCartyEquations
{
  public:
    // Constructor(s)
    cryoHe_McCartyEquations();

      // (1) FUNCTION PRESSM (RHO,T)
      // Pressure [Pa] as a function of density [kg/m3] and temperature [K]
    double Pres_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (2) FUNCTION DPDTM (RHO,T)
      // dP/dT as a function of density and temperature [SI units]
    double dPdT_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (3) FUNCTION DPDDM (RHO,T)
      // dP/dD as a function of density and temperature [SI units]
    double dPdD_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (4) FUNCTION CVM (RHO,T)
      // Cv as a function of density and temperature [SI units]
    double Cv_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (5) FUNCTION ENTRM (RHO,T)
      // Entropy as a function of density and temperature [SI units]
    double Entr_vsDT(double Dens, double Temp, HepackError_t *perr);

      // (6) FUNCTION ENERM (RHO,T)
      // Internal energy as a function of density and temperature [SI units]
    double Ener_vsDT(double Dens, double Temp, HepackError_t *perr);

    // Destructor(s)
    ~cryoHe_McCartyEquations();
    
  private:
    //
    // Calculate or its was done
    int todo(int fNo, double p1);
    int todo(int fNo, double p1, double p2);
    //
    double val[cryoHe_McCartyEquations_meth_+1][3];
    int    stt[cryoHe_McCartyEquations_meth_+1];

};

#endif
