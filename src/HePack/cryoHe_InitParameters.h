//
//  Init Properties for He
//
#ifndef _cryoHe_InitParameters_h
#define _cryoHe_InitParameters_h

#include "HepackError.h"

//  COMMON /PRINIT/
static const double HePrp_R      = 2077.23476;
static const double HePrp_AMW    = 4.0026;
static const double HePrp_PCRIT  = 227462.3;
static const double HePrp_TCRIT  = 5.1953;
static const double HePrp_DCRIT  = 69.6412374;
static const double HePrp_HCRIT  = 21932.;
static const double HePrp_UCRIT  = 18682.;
static const double HePrp_SCRIT  = 5768.6;
static const double HePrp_PTR    = 5041.8;
static const double HePrp_TTR    = 2.1768;
static const double HePrp_DTRF   = 146.15;
static const double HePrp_DTRG   = 1.1923727;
static const double HePrp_HTRF   = 2961.33;
static const double HePrp_HTRG   = 25757.05;
static const double HePrp_UTRF   = 2926.83;
static const double HePrp_UTRG   = 21528.67;
static const double HePrp_STRF   = 1579.97;
static const double HePrp_STRG   = 11991.95;
static const double HePrp_THI    = 300.;
static const double HePrp_DHI    = 0.16040;
static const double HePrp_HHI    = 1573.5e+3;
static const double HePrp_UHI    = 950.1e+3;
static const double HePrp_SHI    = 31.612e+3;
static const double HePrp_CPHI   = 5193.;
static const double HePrp_CVHI   = 3117.;
static const double HePrp_HMAX2P = 30744.;
static const double HePrp_UMAX2P = 24724.;
static const double HePrp_PMAX   = 20280.0e+5;
static const double HePrp_PMIN   = 0.1;
static const double HePrp_TMAX   = 1500.;
static const double HePrp_TMIN   = 0.8;

//  COMMON /SUBHEC/ C1(39),C2(39),V0,T0
static double PrpHeI[41]  = {-0.55524231e+00, 0.18333872e+00,-0.42819388e-01,-0.49962336e-01,
                             -0.83818656e-01,-0.14081863e+00, 0.89901300e+00, 0.66841845e+01,
                              0.98899347e+01, 0.73876336e+01, 0.20130513e+01,-0.25251119e-01,
                             -0.10649342e+01,-0.35520547e+01,-0.58465160e+01,-0.42352097e+01,
                             -0.12206228e+01, 0.16905918e-01, 0.21835833e+00, 0.74548499e+00,
                              0.11932777e+01, 0.86121784e+00, 0.25519882e+00,-0.13224602e-02,
                             -0.20161157e-01,-0.65799115e-01,-0.10330654e+00,-0.75388298e-01,
                             -0.23788871e-01, 0.52810077e-04, 0.66823412e-03, 0.21297216e-02,
                              0.32541554e-02, 0.24421110e-02, 0.82971274e-03,-0.19221178e+01,
                              0.11203003e+01,-0.16430199e+00,-0.34805651e-02,
                              3.63345, -4.32500};
static double PrpHeII[41] = {-0.14629809e+01, 0.76365652e+00,-0.11943389e+00, 0.30525707e-02,
                             -0.10405828e+00,-0.14748127e+00,-0.96308013e+00, 0.67943869e+00,
                             -0.55276320e+00, 0.19535989e-01,-0.47928840e-01, 0.75410775e-01,
                             -0.80609070e-01,-0.76641800e+00,-0.35260824e+01,-0.51006607e+01,
                             -0.20845765e+01,-0.12991643e-01,-0.14392344e-01, 0.72407645e+00,
                              0.35487925e+01, 0.49508203e+01, 0.20014366e+01, 0.12086022e-02,
                              0.10894370e-01,-0.21841979e+00,-0.10697343e+01,-0.14541070e+01,
                             -0.56844527e+00,-0.82701229e-04,-0.10295381e-02, 0.20777040e-01,
                              0.10085164e+00, 0.13267276e+00, 0.49701540e-01, 0.95829687e+00,
                             -0.14025436e+01, 0.63551829e+00,-0.55978553e-01,
                             -0.044009, -0.787770};
static const double PrpHe_Vo = 6.842285;
static const double PrpHe_To = 2.1768;

//  Helium I coefficients (for R.D. McCarty calculations)
//  COMMON /MC1/ G(32),R,GAMMA,VP(9),DTP,PCC,PTP,TCC,TTP,TUL,TLL,PUL,DCC
static double McfHeI_g[32] = {                      .4558980227431e-04,  .1260692007853e-02,
                              -.7139657549318e-02,  .9728903861441e-02, -.1589302471562e-01,
                               .1454229259623e-05, -.4708238429298e-04,  .1132915223587e-02,
                               .2410763742104e-02, -.5093547838381e-08,  .2699726927900e-05,
                              -.3954146691114e-04,  .1551961438127e-08,  .1050712335785e-07,
                              -.5501158366750e-07, -.1037673478521e-09,  .6446881346448e-12,
                               .3298960057071e-10, -.3555585738784e-12, -.6885401367690e-02,
                               .9166109232806e-02, -.6544314242937e-05, -.3315398880031e-04,
                              -.2067693644676e-07,  .3850153114958e-07, -.1399040626999e-10,
                              -.1888462892389e-11, -.4595138561035e-14,  .6872567403738e-14,
                              -.6097223119177e-18, -.7636186157005e-17,  .3848665703556e-17};
static const double McfHeI_r     = 0.00831430;
static const double McfHeI_gamma = -0.33033259e-02;
static double McfHeI_vp[9] = { .1723358273506e+01, .2355572223663e+01, -.1899616867304e+00,
                              -.6145205348730e-01, .1394346356392e+01,  .1500000000000e+01,
                               .2176800000000e+01, .5195300000000e+01,  .5041800000000e-02};
static const double McfHeI_dtp = 0.3651377e+02;
static const double McfHeI_pcc = 0.2275;
static const double McfHeI_ptp = 0.50418e-02;
static const double McfHeI_tcc = 0.51953e+01;
static const double McfHeI_ttp = 0.21768e+01;
static const double McfHeI_tul = 1500.00;
static const double McfHeI_tll = 2.17;
static const double McfHeI_pul = 100.00;
static const double McfHeI_dcc = 17.3990;

//  COMMON /MC8/ T0,S0,H0
static const double McfHeI_To = 0.;
static const double McfHeI_So = 0.0078616;
static const double McfHeI_Ho = 0.0611315;

#endif
