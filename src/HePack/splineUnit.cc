#include "splineUnit.h"

//
//  Class splineUnit
//
//  Cubic Interpolation on [Xo,Xn]
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 01-Feb-2005
//  Modified: 
//

// Constructor(s)
// ==============
splineUnit::splineUnit() {}
//
void splineUnit::init(double x0, double x1, const double *Coeff)
{
  Xo = x0; Xn = x1;
  A = Coeff[0]; B = Coeff[1]; C = Coeff[2]; D = Coeff[3];
}
//
void splineUnit::init(double x0, double x1, double c1,double c2,double c3,double c4)
{
  Xo = x0; Xn = x1;
  A = c1; B = c2; C = c3; D = c4;
}

// Operators
// =========
double splineUnit::operator () (double x) const
{
  return at(x);
}

// Methods
// =======
//
//  Informational Methods
//  ---------------------
int splineUnit::xIn(double x) const
{
  return((x - Xo)*(x - Xn) <= 0);
}
//
double splineUnit::at(double x) const
{
  double dx = x - Xo;
  return (A + dx*(B + dx*(C + D*dx)));
}
//
double splineUnit::dF(double x) const
{
  double dx = x - Xo;
  return (B + dx*(2*C + 3*D*dx));
}

double splineUnit::d2F(double x) const
{
  return (2*C + 6*D*(x-Xo));
}

double splineUnit::Integral(double x0, double x1) const
{
  double sp = xIn(x0)?x0:(Xo - x0)*(Xo - Xn)?Xo:Xn;
  double ep = xIn(x1)?x1:(Xn - x0)*(Xn - Xo)?Xn:Xo;
  //
  return((sp-ep) == 0 ? 0 : (ep*(A +ep*(B/2 +ep*(C/3 +D*ep/4)))-sp*(A +sp*(B/2 +sp)*(C/3 + D*sp/4))));
}

// Destructor
// ==========
splineUnit::~splineUnit() {}
