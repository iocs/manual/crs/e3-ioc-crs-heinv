//
//  HeI calculations for temperature and density via McCarty equations
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 19-May-2006
//  Modified: 
//
//  Notes:
//    1. Valid for temperature range 2 to 1500 K (2-phase region excluded)
//    2. Test on input parameters excluded (!)
//

#include "cryoHe_McCartyEquations.h"

//  Class cryoHe_McCartyEquations
//
cryoHe_McCartyEquations::cryoHe_McCartyEquations()
{
  for (int i=0; i<=cryoHe_McCartyEquations_meth_; i++) stt[i] = 0;
}
  // (1) FUNCTION PRESSM (RHO,T)
  // Pressure [Pa] as a function of density [kg/m3] and temperature [K]
double cryoHe_McCartyEquations::Pres_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(1,Dens,Temp)) {
    //
    double *g=McfHeI_g, gm=McfHeI_gamma, r=McfHeI_r;
    double t=Temp, d=Dens/4.0026, d2=d*d, ti, ti2;
    //
    ti = 1./t; ti2=ti*ti;
    val[1][0] = (((((((((((ti*g[31]+g[30])*ti+g[29])*d2+g[28]*ti+g[27])*d2+g[26]*ti2+g[25])*d2+g[24]*ti+g[23])*d2+g[22]*ti2+g[21])*d2+g[20]*ti+g[19])*exp(gm*d2)+g[8])*ti+g[7])*ti+g[6])*d+((((((d*g[18]+g[17])*ti+g[16])*d+g[15])*d+ti*g[14]+g[13])*d2+g[11])*ti+d*g[12]+g[10])*d2+(ti*g[4]+g[3])*ti+sqrt(t)*g[1]+((d*g[9]+g[5])*d+g[0])*t+g[2])*d2+r*d*t;
  }
  return val[1][0]*1e+6;
}
  // (2) FUNCTION DPDTM (RHO,T)
  // dP/dT as a function of density and temperature [SI units]
double cryoHe_McCartyEquations::dPdT_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(2,Dens,Temp)) {
    //
    double *g=McfHeI_g, gm=McfHeI_gamma, r=McfHeI_r;
    double t=Temp, d=Dens/4.0026, d2=d*d, ti, ti2;
    //
    ti = 1./t; ti2=ti*ti;
    val[2][0] = (d2*(g[0]+0.5*g[1]/sqrt(t)-ti2*(g[3]+2.*ti*g[4])+d*(g[5]-ti2*(g[7]+ti*(2.*g[8]+exp(gm*d2)*(2.*g[19]+3.*ti*g[20]+d2*(2.*g[21]+4.*ti2*g[22]+d2*(2.*g[23]+3.*ti*g[24]+d2*(2.*g[25]+4.*ti2*g[26]+d2*(2.*g[27]+3.*ti*g[28]+d2*(2.*g[29]+3.*ti*g[30]+4.*ti2*g[31]))))))))+d*(g[9]-ti2*g[11]-d2*ti2*(g[13]+2.*ti*g[14]+d*g[15]+d2*(g[16]+2.*ti*(g[17]+d*g[18]))))))+r*d);
  }
  return val[2][0]*1e+6;
}
  // (3) FUNCTION DPDDM (RHO,T)
  // dP/dD as a function of density and temperature [SI units]
double cryoHe_McCartyEquations::dPdD_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(3,Dens,Temp)) {
    //
    double *g=McfHeI_g, gm=McfHeI_gamma, r=McfHeI_r;
    double t=Temp, d=Dens/4.0026, d2=d*d;
    double ti, ti2, f=exp(gm*d2), f1=2.*f*gm*d, fz=f1*d2*d, fy=3.*f*d2;
    //
    ti = 1./t; ti2=ti*ti;
    val[3][0] = (d*(2.*(g[2]+g[1]*sqrt(t)+g[0]*t+ti*(g[3]+ti*g[4]))+d*(3.*(t*g[5]+g[6]+ti*(g[7]+ti*g[8]))+d*(4.*(t*g[9]+g[10]+ti*g[11]+d*1.25*g[12])+d2*ti*((6.*(g[13]+ti*g[14])+d*7.*g[15]+d2*(8.*(g[16]+ti*g[17])+9.*ti*d*g[18]))))))+ti2*((fy+fz)*(g[19]+ti*g[20])+d2*((5./3.*fy+fz)*(g[21]+ti2*g[22])+d2*((7./3.*fy+fz)*(g[23]+ti*g[24])+d2*((3.*fy+fz)*(g[25]+ti2*g[26])+d2*((11./3.*fy+fz)*(g[27]+ti*g[28])+d2*((13./3.*fy+fz)*(g[29]+ti*g[30]+ti2*g[31])))))))+r*t);
  }
  return val[3][0]*249837.6;
}
  // (4) FUNCTION CVM (RHO,T)
  // Cv as a function of density and temperature [SI units]
double cryoHe_McCartyEquations::Cv_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(4,Dens,Temp)) {
    //
    double *g=McfHeI_g, gm=McfHeI_gamma, r=McfHeI_r;
    double t=Temp, d=Dens/4.0026, d2=d*d, d4=d2*d2;
    double ti, ti2, f=exp(gm*d2);
    double gmi, gg2, gg3, gg4, gg5;
    double gg6, gd1, gd2, gd3;
    double gd4, gd5, gd6;

    ti = 1./t; ti2=ti*ti;
    gmi=0.5/gm; gg2=-2*gmi*gmi; gg3=-4*gg2*gmi; gg4=-6*gg3*gmi; gg5=-8*gg4*gmi;
    gg6=-10*gg5*gmi; gd1=f*gmi; gd2=(f*d2-2*gd1)*gmi; gd3=(f*d4-4*gd2)*gmi;
    gd4=(f*d4*d2-6*gd3)*gmi; gd5=(f*d4*d4-8*gd4)*gmi; gd6=(f*d4*d4*d2-10*gd5)*gmi;

    val[4][0] = d*(-0.25*g[1]/sqrt(t)+ti2*(2*g[3]+6*g[4]*ti+d*(g[7]+3*g[8]*ti+2./3*g[11]*d))+d4*ti2*((((0.75*g[18]*d+6./7*g[17])*ti+2./7*g[16])*d+1./3*g[15])*d+1.2*ti*g[14]+0.4*g[13]))+6*ti2*ti*((gd1-gmi)*(g[19]+2*ti*g[20])+(gd2-gg2)*(g[21]+10./3*ti2*g[22])+(gd3-gg3)*(g[23]+2*ti*g[24])+(gd4-gg4)*(g[25]+10./3*ti2*g[26])+(gd5-gg5)*(g[27]+2*ti*g[28])+(gd6-gg6)*(g[29]+ti*(2*g[30]+10./3*ti*g[31])));
    val[4][0] = 1e+3 * (2.5*0.00831430 - (r+val[4][0]));
  }
  return val[4][0]*1000./4.0026;
}
  // (5) FUNCTION ENTRM (RHO,T)
  // Entropy as a function of density and temperature [SI units]
double cryoHe_McCartyEquations::Entr_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(5,Dens,Temp)) {
    //
    double *g=McfHeI_g, gm=McfHeI_gamma, r=McfHeI_r;
    double t=Temp, d=Dens/4.0026, d2=d*d, d4=d2*d2;
    double ti, ti2, f;
    double gg1, gg2, gg3, gg4, gg5;
    double gg6, gd1, gd2, gd3;
    double gd4, gd5, gd6;
    //
    ti=1/t; ti2=ti*ti; f=exp(gm*d2);
    gg1=0.5/gm; gg2=-2.*gg1*gg1; gg3=-4.*gg2*gg1; gg4=-6.*gg3*gg1; gg5=-8.*gg4*gg1;
    gg6=-10.*gg5*gg1; gd1=f*gg1; gd2=(f*d2-2.*gd1)*gg1; gd3=(f*d4-4.*gd2)*gg1;
    gd4=(f*d4*d2-6.*gd3)*gg1; gd5=(f*d4*d4-8.*gd4)*gg1; gd6=(f*d4*d4*d2-10.*gd5)*gg1;

    val[5][0] = 2.5*r*log(t)+d*(-g[0]-0.5*g[1]/sqrt(t)-d*(0.5*g[5]+1./3*d*g[9])+ti2*(g[3]+d*(0.5*g[7]+d*(1./3*g[11]+d2*(0.2*g[13]+d*(1./6*g[15]+d*1./7*g[16]))))+ti*(2.*g[4]+d*(g[8]+d2*d*(0.4*g[14]+d2*(2./7*g[17]+d*0.25*g[18]))))))+2.*ti2*ti*((f-1.)*gg1*(g[19]+g[20]*ti*1.5)+(gd2-gg2)*(g[21]+g[22]*ti2*2.)+(gd3-gg3)*(g[23]+g[24]*ti*1.5)+(gd4-gg4)*(g[25]+g[26]*ti2*2.)+(gd5-gg5)*(g[27]+g[28]*ti*1.5)+(gd6-gg6)*(g[29]+g[30]*ti*1.5+g[31]*ti2*2.));
    val[5][0] = (val[5][0] + McfHeI_So - r*log(d*r*t/0.101325))*1e+3;
  }
  return val[5][0]*1000./4.0026;
}
  // (6) FUNCTION ENERM (RHO,T)
  // Internal energy as a function of density and temperature [SI units]
double cryoHe_McCartyEquations::Ener_vsDT(double Dens, double Temp, HepackError_t *perr)
{
  if (Dens <= 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(6,Dens,Temp)) {
    //
    double *g=McfHeI_g, gm=McfHeI_gamma, r=McfHeI_r;
    double t=Temp, d=Dens/4.0026, d2=d*d, d4=d2*d2;
    double ti, ti2, f;
    double gmi, gg2, gg3, gg4;
    double gg5, gg6, gd1, gd2;
    double gd3, gd4, gd5;
    double gd6;
    //
    if (t==0. || gm==0.) return 0.;
    ti=1/t; ti2=ti*ti; f=exp(gm*d2);
    gmi=0.5/gm; gg2=-2.*gmi*gmi; gg3=-4.*gg2*gmi; gg4=-6.*gg3*gmi;
    gg5=-8.*gg4*gmi; gg6=-10.*gg5*gmi; gd1=f*gmi; gd2=(f*d2-2.*gd1)*gmi;
    gd3=(f*d4-4.*gd2)*gmi; gd4=(f*d4*d2-6.*gd3)*gmi; gd5=(f*d4*d4-8.*gd4)*gmi;
    gd6=(f*d4*d4*d2-10.*gd5)*gmi;
    val[6][0] = 2.5*r*t+d*(g[1]*0.5*sqrt(t)+g[2]+ti*(g[3]*2.+ti*g[4]*3.0)+d*(g[6]*0.5+ti*(g[7]+ti*g[8]*1.5)+d*(g[10]*1./3+ti*g[11]*2./3+d*(g[12]*0.25+d*ti*(g[13]*0.4+ti*g[14]*0.6+d*(g[15]*1./3+d*(g[16]*2./7+ti*(g[17]*3./7+d*g[18]*0.375))))))))+ti2*((gd1-gmi)*(3.*g[19]+ti*4.*g[20])+(gd2-gg2)*(3.*g[21]+ti2*5.*g[22])+(gd3-gg3)*(3.*g[23]+ti*4.*g[24])+(gd4-gg4)*(3.*g[25]+ti2*5.*g[26])+(gd5-gg5)*(3.*g[27]+ti*4.*g[28])+(gd6-gg6)*(3.*g[29]+ti*(4.*g[30]+ti*5.*g[31])));
    val[6][0] = (val[6][0] - r*t + McfHeI_Ho)*1e+3;
  }
  return val[6][0]*1000./4.0026;
}
  //
  // Destructor(s)
cryoHe_McCartyEquations::~cryoHe_McCartyEquations() {}
//
//  Private Part
//
int cryoHe_McCartyEquations::todo(int fNo, double p1)
{
  return todo(fNo, p1, -1);
}
int cryoHe_McCartyEquations::todo(int fNo, double p1, double p2)
{
  val[fNo][1] = p1;
  val[fNo][2] = p2;
  stt[fNo]    = 1;
  return 1;
}
