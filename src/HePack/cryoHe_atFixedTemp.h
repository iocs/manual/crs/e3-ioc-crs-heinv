//
//  He parameters at fixed temperature (1.75K and 3K).
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 09-Jun-2006
//  Modified: 
//    21.Apr.2007 by V.Gubarev
//      o Inserted recalculation check
//
//  Notes:
//    Valid for Pressure > 0. Internal check for input parameter is ommited.
//

#ifndef _cryoHe_atFixedTemp_h
#define _cryoHe_atFixedTemp_h

#include "HepackError.h"

#define cryoHe_atFixedTemp_meth_ 5

#include "cryoHe_InitParameters.h"

#include <math.h>

class cryoHe_atFixedTemp
{
  public:
    // Constructor(s)
    cryoHe_atFixedTemp();

      // (1) FUNCTION D175K (PASCAL)
      // Density [kg/m3] of HeII as a function of P [Pa] at T(58)=1.75
      // accuracy ~ 0.2%.
    double Dens175K_vsP(double Pres);

      // (2) FUNCTION H3K (P)
      // Enthalpy [J/kg] at 3 K, for P > P(sat)
      // accuracy about +/- 4.  J/kG for pressures below 2 bars
      //          about +/- 25. J/kG for pressures above 2 bars
    double Enth3K_vsP(double Pres);

      // (3) FUNCTION S3K (P)
      // Entropy [J/(kg*K)] at 3 K, for P > P(sat)
      // accuracy about +/- 1. J/(kg*K) for pressures below 2 bars
      //          about +/- 8. J/(kg*K) for pressures above 2 bars
    double Entr3K_vsP(double Pres);

      // (4) FUNCTION U3K (P)
      // Internal Energy [J/kg] at 3 K, for P > P(sat)
      // accuracy about +/- 4.  J/kg for pressures below 2 bars
      //          about +/- 10. J/kg for pressures above 2 bars
    double Ener3K_vsP(double Pres);

      // (5) FUNCTION D3K (P)
      // Density [kg/m3] at 3 K, for P > P(sat)
      // accuracy about +/- 0.1 kg/m3 for pressures below 15 bars
      //          about +/- 0.3 kg/m3 for pressures above 15 bars
    double Dens3K_vsP(double Pres);
    
    // Destructor(s)
    ~cryoHe_atFixedTemp();

  private:
    //
    int todo(int fNo, double p1);
    //
    double val[cryoHe_atFixedTemp_meth_ +1][2];
    int    stt[cryoHe_atFixedTemp_meth_ +1];
};

#endif

