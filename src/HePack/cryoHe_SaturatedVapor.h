//
//  Calculations for Saturated Vapor
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 16-May-2006
//  Modified: 
//    24.Apr.2007 by V.Gubarev
//      o Back functions are replaced by splines
//
//  Notes:
//    1. Valid for temperature range 0.8 to 5.1953 K, T76 scale.
//    2. Test on input parameters is excluded (!)
//

#ifndef _cryoHe_SaturatedVapor_h
#define _cryoHe_SaturatedVapor_h

#include "cryoHe_SinglePhase.h"
#include "cryoHe_SaturatedBase.h"

#include "HepackError.h"

#define cryoHe_SaturatedVapor_Meth_ 60

class cryoHe_SaturatedVapor
{
  public:
  
    // Constructor(s)
    cryoHe_SaturatedVapor();

      /*  Functions vs. Temperature */
      /*  ------------------------- */

      // (1) FUNCTION D2VFPT (PSAT, TSAT)
      // Density [kg/m3] as a function of pressure [Pa] and temperature [K]
    double Dens_vsPT(double Pres, double Temp, HepackError_t *perr);

      // (2) FUNCTION SATD (T)  (Use it only)
      // Density [kg/m3] as a function of temperature [K]; 0.8 < T < 5.1953
    double Dens_vsT(double Temp, HepackError_t *perr);

      // (3) from SUBROUTINE PSATFT (P, DPDTS, T)
      // Saturation pressure as a function of temperature
    double Pres_vsT(double Temp, HepackError_t *perr);

      // (4) from SUBROUTINE PSATFT (P, DPDTS, T)
      // Saturation dP/dT as a function of temperature
    double dPdT_vsT(double Temp, HepackError_t *perr);
    
      // (5) from FUNCTION SATS (T)
      // Saturation Entropy as a function of temperature
    double Entr_vsT(double Temp, HepackError_t *perr);
    
      // (6) from FUNCTION SATVY (T)
      // Saturation Enthalpy as a function of temperature
    double Enth_vsT(double Temp, HepackError_t *perr);

      // (7) from FUNCTION SATVY (T)
      // Saturation Internal Energy as a function of temperature
    double Ener_vsT(double Temp, HepackError_t *perr);

      // (8) from FUNCTION SATVY (T)
      // Saturation Gibbs Energy as a function of temperature
    double Gibb_vsT(double Temp, HepackError_t *perr);

      /*  Functions vs. Pressure */
      /*  ---------------------- */

      // (9) FUNCTION TSATFP (PP)
      // Temperature [K] as a function of pressure [Pa]
    double Temp_vsP(double Pres, HepackError_t *perr);

      /*  Functions vs. Entropy */
      /*  --------------------- */

      // (10) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
      // Temperature as a function of entropy (SI units)
    double Temp_vsS(double Entr, HepackError_t *perr);

      // (11) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
      // Pressure as a function of entropy (SI units)
    double Pres_vsS(double Entr, HepackError_t *perr);

      // (12) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
      // dP/dT as a function of entropy (SI units)
    double dPdT_vsS(double Entr, HepackError_t *perr);

      // (13) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
      // Liquid Density as a function of entropy (SI units)
    double Dens_vsS(double Entr, HepackError_t *perr);

      /*  Functions vs. Density */
      /*  --------------------- */

      // (20) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
      // Temperature as a function of Density (SI units)
    double Temp_vsD(double Dens, HepackError_t *perr);

      // (21) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
      // Pressure as a function of Density (SI units)
    double Pres_vsD(double Dens, HepackError_t *perr);

      // (22) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
      // dP/dT as a function of Density (SI units)
    double dPdT_vsD(double Dens, HepackError_t *perr);

      // (23) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
      // Liquid Density as a function of Density (SI units)
    double Dens_vsD(double Dens, HepackError_t *perr);

      /*  Functions vs. Enthalpy */
      /*  ---------------------- */

      // (30) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
      // Temperature as a function of Enthalpy (SI units)
    double Temp_vsH(double Enth, HepackError_t *perr);

      // (31) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
      // Pressure as a function of Enthalpy (SI units)
    double Pres_vsH(double Enth, HepackError_t *perr);

      // (32) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
      // dP/dT as a function of Enthalpy (SI units)
    double dPdT_vsH(double Enth, HepackError_t *perr);

      // (33) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
      // Liquid Density as a function of Enthalpy (SI units)
    double Dens_vsH(double Enth, HepackError_t *perr);

      /*  Functions vs. Internal Energy */
      /*  ----------------------------- */

      // (40) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
      // Temperature as a function of Internal Energy (SI units)
    double Temp_vsU(double Ener, HepackError_t *perr);

      // (41) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
      // Pressure as a function of Internal Energy (SI units)
    double Pres_vsU(double Ener, HepackError_t *perr);

      // (42) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
      // dP/dT as a function of Internal Energy (SI units)
    double dPdT_vsU(double Ener, HepackError_t *perr);

      // (43) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
      // Liquid Density as a function of Internal Energy (SI units)
    double Dens_vsU(double Ener, HepackError_t *perr);

      /*  Functions vs. Gibbs Energy */
      /*  -------------------------- */

      // (50) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
      // Temperature as a function of Gibbs Energy (SI units)
    double Temp_vsG(double Gibb, HepackError_t *perr);

      // (51) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
      // Pressure as a function of Gibbs Energy (SI units)
    double Pres_vsG(double Gibb, HepackError_t *perr);

      // (52) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
      // dP/dT as a function of Gibbs Energy (SI units)
    double dPdT_vsG(double Gibb, HepackError_t *perr);

      // (53) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
      // Liquid Density as a function of Gibbs Energy (SI units)
    double Dens_vsG(double Gibb, HepackError_t *perr);

    // Destructor(s)
    ~cryoHe_SaturatedVapor();
    
    //  Limited Variables
    double lowTemp;   // low Temperature [K]
    double uppTemp;   // upper Temperature [K]
    double lowDens;   // low Density
    double uppDens;   // upper Density
    double lowPres;   // low Pressure
    double uppPres;   // upper Pressure
    double lowEntr;   // low Entropy
    double uppEntr;   // upper Entropy
    double lowEnth;   // low Enthalpy
    double uppEnth;   // upper Enthalpy
    double ttpEnth;   // Temperature for upper Enthalpy
    double lowEner;   // low Internal Energy
    double uppEner;   // upper Internal Energy
    double ttpEner;   // Temperature for upper Energy
    double lowGibb;   // low Gibbs Energy
    double uppGibb;   // upper Gibbs Energy
    
  private:
    //    FUNCTION DGSAT (T)
    // Density [kg/m3] as a function of temperature [K]
    // Saturated vapor density, 0.8 < T < 5.1953.
    // This is the best independent estimate of saturation density;
    // Theoretical form above TNRC, fitted for continuity at TNRC.
    double _svD_vsT(double Temp);
    //
    // Splines
    spline *_TvsD;
    spline *_TvsS;
    spline *_TvsH;
    spline *_TvsU;
    spline *_TvsG;
    //
    // Calculate or it's was done
    int todo(int fNo, double p1);
    int todo(int fNo, double p1, double p2);
    //
    double val[cryoHe_SaturatedVapor_Meth_+1][3];
    int    stt[cryoHe_SaturatedVapor_Meth_+1];
    
    // Base object
    cryoHe_SinglePhase   He1Ph;
    cryoHe_SaturatedBase sBase;
};

#endif
