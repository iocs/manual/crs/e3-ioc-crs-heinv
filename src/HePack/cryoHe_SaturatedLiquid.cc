//
//  Calculations for Saturated Liquid
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 16-May-2006
//  Modified: 
//    23.Apr.2007 by V.Gubarev
//      o Back functions are replaced by splines
//
//  Notes:
//    1. Valid for temperature range 0.8 to 5.1953 K, T76 scale.
//    2. Test on input parameters is excluded (!)
//    3. The maximum saturated liquid density of 146.1603 occurs at T = 2.1852 K,
//       according the HEPAK v3.2.  In the liquid, SATFD always returns a
//       temperature T > 2.1852; the saturated superfluid branch is not found.
//

#include "cryoHe_SaturatedLiquid.h"

//  Class cryoHe_SaturatedLiquid
//
cryoHe_SaturatedLiquid::cryoHe_SaturatedLiquid()
{
  for (int i=0; i<cryoHe_SaturatedLiquid_Meth_; i++) stt[i] = 0;
  //
  lowTemp = sBase.lowTemp;
  ttpDens = 2.18523;
  uppTemp = sBase.uppTemp;
  lowDens = Dens_vsT(uppTemp, NULL);
  uppDens = Dens_vsT(ttpDens, NULL);
  lowPres = Pres_vsT(lowTemp, NULL);
  uppPres = Pres_vsT(uppTemp, NULL);
  lowEntr = Entr_vsT(lowTemp, NULL);
  uppEntr = Entr_vsT(uppTemp, NULL);
  lowEnth = Enth_vsT(lowTemp, NULL);
  uppEnth = Enth_vsT(uppTemp, NULL);
  lowEner = Ener_vsT(lowTemp, NULL);
  uppEner = Ener_vsT(uppTemp, NULL);
  lowGibb = Gibb_vsT(uppTemp, NULL);
  uppGibb = Gibb_vsT(lowTemp, NULL);
  //
  _TvsD = 0;
  _TvsS = 0;
  _TvsH = 0;
  _TvsU = 0;
  _TvsG = 0;
}

  // (1) FUNCTION D2LFPT (PSAT, TSAT)
  // Density [kg/m3] as a function of pressure [Pa] and temperature [K]
double cryoHe_SaturatedLiquid::Dens_vsPT(double Pres, double Temp, HepackError_t *perr)
{
  static int error_max_iteration = 0;
  if (Pres < 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(1,Pres,Temp)) {
    double tnrc=5.106, tlow=4.804, delt=tnrc-tlow;
    error_max_iteration = 0;
    if (Temp >= tnrc) val[1][0] = _slD_vsT(Temp);
    else {
      double Do=HePrp_DCRIT, Fo=He1Ph.Pres_vsDT(Do,Temp, perr), dD=10, Dn, Fn;
      while (dD > 1e-10) {
	short count;
        for (Dn=Do+dD,Fn=He1Ph.Pres_vsDT(Dn,Temp, perr); ((Pres-Fo)*(Pres-Fn) > 0); Do=Dn,Fo=Fn,Dn+=dD,Fn=He1Ph.Pres_vsDT(Dn,Temp, perr));
	if (count > 1000) {
	  HE_ERROR(MAX_ITERATIONS)
	  error_max_iteration = 1;
	  break;
	}
        dD = (Dn - Do)/10;
      }
      val[1][0] = (Do + Dn)/2;
      if (Temp >= tlow) val[1][0] = ((tnrc-Temp)*val[1][0] + (Temp-tlow)*_slD_vsT(Temp))/delt;
    }
  }
  else
    if (error_max_iteration) HE_ERROR(MAX_ITERATIONS)
  return val[1][0];
}
  // (2) FUNCTION SATD (T)  (Use it only)
  // Density [kg/m3] as a function of temperature [K]; 0.8 < T < 5.1953
double cryoHe_SaturatedLiquid::Dens_vsT(double Temp, HepackError_t *perr)
{
  if (todo(2,Temp)) val[2][0] = Dens_vsPT(sBase.Pres_vsT(Temp, perr), Temp, perr);
  return val[2][0];
}
  // (3) from SUBROUTINE PSATFT (P, DPDTS, T)
  // Saturation pressure as a function of temperature
double cryoHe_SaturatedLiquid::Pres_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(3,Temp)) val[3][0] = sBase.Pres_vsT(Temp, perr);
  return val[3][0];
}
  // (4) from SUBROUTINE PSATFT (P, DPDTS, T)
  // Saturation dP/dT as a function of temperature
double cryoHe_SaturatedLiquid::dPdT_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(4,Temp)) val[4][0] = sBase.dPdT_vsT(Temp, perr);
  return val[4][0];
}
  // (5) from FUNCTION SATS (T)
  // Saturation Entropy as a function of temperature
double cryoHe_SaturatedLiquid::Entr_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(5,Temp)) val[5][0] = He1Ph.Entr_vsDT(Dens_vsPT(Pres_vsT(Temp, perr),Temp,perr),Temp,perr);
  return val[5][0];
}
  // (6) from FUNCTION SATLY (T)
  // Saturation Enthalpy as a function of temperature
double cryoHe_SaturatedLiquid::Enth_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(6,Temp)) val[6][0] = He1Ph.Enth_vsDTP(Dens_vsPT(Pres_vsT(Temp, perr),Temp,perr),Temp,Pres_vsT(Temp, perr),perr);
  return val[6][0];
}
  // (7) from FUNCTION SATLY (T)
  // Saturation Internal Energy as a function of temperature
double cryoHe_SaturatedLiquid::Ener_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(7,Temp)) val[7][0] = He1Ph.Ener_vsDT(Dens_vsPT(Pres_vsT(Temp, perr),Temp,perr),Temp,perr);
  return val[7][0];
}
  // (8) from FUNCTION SATLY (T)
  // Saturation Gibbs Energy as a function of temperature
double cryoHe_SaturatedLiquid::Gibb_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(8,Temp)) val[8][0] = He1Ph.Gibb_vsDTP(Dens_vsPT(Pres_vsT(Temp, perr),Temp,perr),Temp,Pres_vsT(Temp, perr),perr);
  return val[8][0];
}

  // (9) FUNCTION TSATFP (PP)
  // Temperature [K] as a function of pressure [Pa]
double cryoHe_SaturatedLiquid::Temp_vsP(double Pres, HepackError_t *perr)
{
  if (Pres < 0.) HE_ERROR(VALUE_OUT)
  else if (todo(9,Pres)) val[9][0] = sBase.Temp_vsP(Pres, perr);
  return val[9][0];
}

//  Function = Function(Entropy)
//  ============================

  // (10) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
  // Temperature as a function of entropy (SI units)
double cryoHe_SaturatedLiquid::Temp_vsS(double Entr, HepackError_t *perr)
{
  if (todo(10,Entr)) {
    if (_TvsS == (spline *)0) {
      int NN=600, N1=150, N2=150, N3=300, i=0;
      double *T = new double[NN+1], *S = new double[NN+1], crTemp1 = 2.18523, crTemp2 = 5.07;
      for (double t=lowTemp, dT=(crTemp1 - t)/N1; i< N1;    T[i]=t, S[i]=Entr_vsT(t, perr), i++, t=lowTemp+dT*i);
      for (double t=crTemp1, dT=(crTemp2 - t)/N2; i< N1+N2; T[i]=t, S[i]=Entr_vsT(t, perr), i++, t=crTemp1+dT*(i-N1));
      for (double t=crTemp2, dT=(uppTemp - t)/N3; i<=NN;    T[i]=t, S[i]=Entr_vsT(t, perr), i++, t=crTemp2+dT*(i-N1-N2));
      _TvsS  = new spline_Smooth(NN+1,S,T,1);
      delete [] T; delete [] S;
    }
    val[10][0] = (*_TvsS)(Entr);
  }
  return val[10][0];
}
  // (11) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
  // Pressure as a function of entropy (SI units)
double cryoHe_SaturatedLiquid::Pres_vsS(double Entr, HepackError_t *perr)
{
  todo (11,Entr);
  val[11][0] = Pres_vsT(Temp_vsS(Entr, perr), perr);
  return val[11][0];
}
  // (12) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
  // dP/dT as a function of entropy (SI units)
double cryoHe_SaturatedLiquid::dPdT_vsS(double Entr, HepackError_t *perr)
{
  todo (12,Entr);
  val[12][0] = dPdT_vsT(Temp_vsS(Entr, perr), perr);
  return val[12][0];
}
  // (13) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
  // Liquid Density as a function of entropy (SI units)
double cryoHe_SaturatedLiquid::Dens_vsS(double Entr, HepackError_t *perr)
{
  todo (13,Entr);
  val[13][0] = Dens_vsPT(Pres_vsS(Entr, perr),Temp_vsS(Entr, perr),perr);
  return val[13][0];
}

//  Function = Function(Density)
//  ============================

  // (20) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
  // Temperature as a function of Density (SI units)
double cryoHe_SaturatedLiquid::Temp_vsD(double Dens, HepackError_t *perr)
{
  if (Dens <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(20,Dens)) {
    if (_TvsD == (spline *)0) {
      int NN=600, i=0;
      double *D = new double[NN+1], *T = new double[NN+1];
      for (double t=ttpDens, dT=(uppTemp - t)/NN; i<=NN; T[i]=t, D[i]=Dens_vsT(t,perr), i++, t=ttpDens+dT*i);
      _TvsD  = new spline_Smooth(NN+1,D,T,1);
      delete [] D; delete [] T;
    }
    if (Dens >= 93.3275) val[20][0] = (*_TvsD)(Dens);
    else {
      val[20][0] = (Dens - 69.6412)/(93.3275 - 69.641);
      if (fabs(val[20][0]) < 1e-8) val[20][0] = 1e-8;
      val[20][0] = 5.1953 - 0.0893*pow(val[20][0],3);
    }
  }
  return val[20][0];
}
  // (21) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
  // Pressure as a function of Density (SI units)
double cryoHe_SaturatedLiquid::Pres_vsD(double Dens, HepackError_t *perr)
{
  todo (21,Dens);
  val[21][0] = Pres_vsT(Temp_vsD(Dens,perr), perr);
  return val[21][0];
}
  // (22) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
  // dP/dT as a function of Density (SI units)
double cryoHe_SaturatedLiquid::dPdT_vsD(double Dens, HepackError_t *perr)
{
  todo(22,Dens);
  val[22][0] = dPdT_vsT(Temp_vsD(Dens,perr), perr);
  return val[22][0];
}
  // (23) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
  // Liquid Density as a function of Density (SI units)
double cryoHe_SaturatedLiquid::Dens_vsD(double Dens, HepackError_t *perr)
{
  todo(23,Dens);
  val[23][0] = Dens_vsPT(Pres_vsD(Dens, perr),Temp_vsD(Dens,perr),perr);
  return val[23][0];
}

//  Function = Function(Enthalpy)
//  =============================

  // (30) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
  // Temperature as a function of Enthalpy (SI units)
double cryoHe_SaturatedLiquid::Temp_vsH(double Enth, HepackError_t *perr)
{
  if (todo(30,Enth)) {
    if (_TvsH == (spline *)0) {
      int NN=600, N1=150, N2=150, N3=300, i=0;
      double *T = new double[NN+1], *H = new double[NN+1], crTemp1 = 2.18523, crTemp2 = 5.07;
      for (double t=lowTemp, dT=(crTemp1 - t)/N1; i< N1;    T[i]=t, H[i]=Enth_vsT(t, perr), i++, t=lowTemp+dT*i);
      for (double t=crTemp1, dT=(crTemp2 - t)/N2; i< N1+N2; T[i]=t, H[i]=Enth_vsT(t, perr), i++, t=crTemp1+dT*(i-N1));
      for (double t=crTemp2, dT=(uppTemp - t)/N3; i<=NN;    T[i]=t, H[i]=Enth_vsT(t, perr), i++, t=crTemp2+dT*(i-N1-N2));
      _TvsH  = new spline_Smooth(NN+1,H,T,1);
      delete [] T; delete [] H;
    }
    val[30][0] = (*_TvsH)(Enth);
  }
  return val[30][0];
}
  // (31) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
  // Pressure as a function of Enthalpy (SI units)
double cryoHe_SaturatedLiquid::Pres_vsH(double Enth, HepackError_t *perr)
{
  todo(31,Enth);
  val[31][0] = Pres_vsT(Temp_vsH(Enth, perr), perr);
  return val[31][0];
}
  // (32) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
  // dP/dT as a function of Enthalpy (SI units)
double cryoHe_SaturatedLiquid::dPdT_vsH(double Enth, HepackError_t *perr)
{
  todo(32,Enth);
  val[32][0] = dPdT_vsT(Temp_vsH(Enth, perr), perr);
  return val[32][0];
}
  // (33) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
  // Liquid Density as a function of Enthalpy (SI units)
double cryoHe_SaturatedLiquid::Dens_vsH(double Enth, HepackError_t *perr)
{
  todo(33,Enth);
  val[33][0] = Dens_vsPT(Pres_vsH(Enth, perr),Temp_vsH(Enth, perr),perr);
  return val[33][0];
}

//  Function = Function(Internal Energy)
//  ====================================

  // (40) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
  // Temperature as a function of Internal Energy (SI units)
double cryoHe_SaturatedLiquid::Temp_vsU(double Ener, HepackError_t *perr)
{
  if (todo(40,Ener)) {
    if (_TvsU == (spline *)0) {
      int NN=600, N1=150, N2=150, N3=300, i=0;
      double *T = new double[NN+1], *U = new double[NN+1], crTemp1 = 2.18523, crTemp2 = 5.07;
      for (double t=lowTemp, dT=(crTemp1 - t)/N1; i< N1;    T[i]=t, U[i]=Ener_vsT(t, perr), i++, t=lowTemp+dT*i);
      for (double t=crTemp1, dT=(crTemp2 - t)/N2; i< N1+N2; T[i]=t, U[i]=Ener_vsT(t, perr), i++, t=crTemp1+dT*(i-N1));
      for (double t=crTemp2, dT=(uppTemp - t)/N3; i<=NN;    T[i]=t, U[i]=Ener_vsT(t, perr), i++, t=crTemp2+dT*(i-N1-N2));
      _TvsU  = new spline_Smooth(NN+1,U,T,1);
      delete [] U; delete [] T;
    }
    val[40][0] = (*_TvsU)(Ener);
  }
  return val[40][0];
}
  // (41) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
  // Pressure as a function of Internal Energy (SI units)
double cryoHe_SaturatedLiquid::Pres_vsU(double Ener, HepackError_t *perr)
{
  todo(41,Ener);
  val[41][0] = Pres_vsT(Temp_vsU(Ener, perr), perr);
  return val[41][0];
}
  // (42) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
  // dP/dT as a function of Internal Energy (SI units)
double cryoHe_SaturatedLiquid::dPdT_vsU(double Ener, HepackError_t *perr)
{
  todo(42,Ener);
  val[42][0] = dPdT_vsT(Temp_vsU(Ener, perr), perr);
  return val[42][0];
}
  // (43) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
  // Liquid Density as a function of Internal Energy (SI units)
double cryoHe_SaturatedLiquid::Dens_vsU(double Ener, HepackError_t *perr)
{
  todo(43,Ener);
  val[43][0] = Dens_vsPT(Pres_vsU(Ener, perr),Temp_vsU(Ener, perr),perr);
  return val[43][0];
}

//  Function = Function(Gibbs Energy)
//  =================================

  // (50) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
  // Temperature as a function of Gibbs Energy (SI units)
double cryoHe_SaturatedLiquid::Temp_vsG(double Gibb, HepackError_t *perr)
{
  if (todo(50,Gibb)) {
    if (_TvsG == (spline *)0) {
      int NN=600, i=0;
      double *G = new double[NN+1], *T = new double[NN+1];
      for (double t=lowTemp, dT=(uppTemp - t)/NN; i<=NN; T[i]=t, G[i]=Gibb_vsT(t, perr), i++, t=lowTemp+dT*i);
      _TvsG  = new spline_Smooth(NN+1,G,T,1);
      delete [] G; delete [] T;
    }
    val[50][0] = (*_TvsG)(Gibb);
  }
  return val[50][0];
}
  // (51) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
  // Pressure as a function of Gibbs Energy (SI units)
double cryoHe_SaturatedLiquid::Pres_vsG(double Gibb, HepackError_t *perr)
{
  todo(51,Gibb);
  val[51][0] = Pres_vsT(Temp_vsG(Gibb, perr), perr);
  return val[51][0];
}
  // (52) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
  // dP/dT as a function of Gibbs Energy (SI units)
double cryoHe_SaturatedLiquid::dPdT_vsG(double Gibb, HepackError_t *perr)
{
  todo(52,Gibb);
  val[52][0] = dPdT_vsT(Temp_vsG(Gibb, perr), perr);
  return val[52][0];
}
  // (53) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
  // Liquid Density as a function of Gibbs Energy (SI units)
double cryoHe_SaturatedLiquid::Dens_vsG(double Gibb, HepackError_t *perr)
{
  todo(53,Gibb);
  val[53][0] = Dens_vsPT(Pres_vsG(Gibb, perr),Temp_vsG(Gibb, perr),perr);
  return val[53][0];
}
  //
  // Destructor(s)
cryoHe_SaturatedLiquid::~cryoHe_SaturatedLiquid()
{
  if (_TvsD != (spline *)0) delete _TvsD;
  if (_TvsS != (spline *)0) delete _TvsS;
  if (_TvsH != (spline *)0) delete _TvsH;
  if (_TvsU != (spline *)0) delete _TvsU;
  if (_TvsG != (spline *)0) delete _TvsG;
}
//
//  Private Part
//
  //    FUNCTION DFSAT (T)  (Never is used directly in Helium Pack)
  // Density [kg/m3] as a function of temperature [K]; 0.8 < T < 5.1953
  // This is the best independent estimate of saturation density;
  // HeI range above TNRC:  theoretical form, fitted for continuity at TNRC
  // HeI range above 3.2 K: R.D.McCarty equation May 4, 1988.
  // HeI range below 3.2 K: Van Degrift' s equation, quoted by Barenghi Lucas and Donnelly
  // HeII range: V.Arp equation
double cryoHe_SaturatedLiquid::_slD_vsT(double Temp)
{
  double Ttp= 2.1768, Tbb=3.1853, Tnrc= 5.1060, Tcc= 5.1953;
  double Dtp=36.514,              Dnrc=93.3275, Dcc=17.399;
  double Deno=145.188, Denl=146.15;
  double out = -1;
  //
  if (Temp > Tnrc) {
    if ((out = (Tcc-Temp)/(Tcc-Tnrc)) < 1.e-20) out = 1.e-20;
    return (Dcc + (Dnrc/4.0026 - Dcc)*pow(out,1./3))*4.0026;
  }
  else if (Temp >= Tbb) {
    double a1=-0.3434500882e+02, a2= 0.4501470666e+00, a3=-0.6895460170e+01;
    double a4= 0.5442223002e+02, a5=-0.9716273774e+02, a6= 0.1785061367e+02;
    double x = (Temp - Tcc)/(Ttp - Tcc), y = pow(x,1./3);
    out = a1*log(x) + a2*(1.-1./x) + a3*(1.-y/x) + a4*(1.-y*y/x) + a5*(1.-y) + a6*(1.-y*y);
    return (Dcc + exp(out)*(Dtp - Dcc))*4.0026;
  }
  else if (Temp > Ttp) {
    double b1=-0.1297822470e+01, b2=-0.4911239491e+01, b3=0.1021276082e+01, b4=-0.2626058542e+01;
    double z = Temp - Ttp;
    return (Denl + z*(b1*log(z) + b2 + z*(b3 + z*b4)));
  }
  else if (Temp == Ttp) {
    return Denl;
  }
  else {
    double c1=-0.2433560558e+03, c2=0.1814476908e+04, c3=0.2425346403e+04, c4=-0.4604869802e+03, c5=0.2453561962e+03;
    double z = Temp - Ttp, y = Temp*Temp;
    return ((y*y*(c1*z*(log(-z) -1.) + c2*z + c3 + c4*y) + c5*y)*1.e-6 + 1.)*Deno;
  }
  return out;
}

int cryoHe_SaturatedLiquid::todo(int fNo, double p1)
{
  return todo(fNo, p1, -1);
}
int cryoHe_SaturatedLiquid::todo(int fNo, double p1, double p2)
{
  val[fNo][1] = p1;
  val[fNo][2] = p2;
  stt[fNo]    = 1;
  return 1;
}
