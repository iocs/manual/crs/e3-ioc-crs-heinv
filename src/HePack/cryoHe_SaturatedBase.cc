//
//  Base calculations for Saturated Liquid and Saturated Vapor
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 06-Jul-2006
//  Modified: 
//    20.May.2007 by V.Gubarev
//      o Back functions are replaced by splines
//      o Add Function Temp_vsP(Pres)
//
//  Notes:
//    1. Valid for temperature range 0.8 to 5.1953 K, T76 scale.
//    2. Test on input parameters excluded (!)
//

#include "cryoHe_SaturatedBase.h"

//  Class cryoHe_SaturatedBase
//
cryoHe_SaturatedBase::cryoHe_SaturatedBase()
{
  _TvsP =  (spline *)0;
  //
  lowTemp = HePrp_TMIN;
  crtTemp = HePrp_TTR;
  uppTemp = HePrp_TCRIT;
  //
  for (int i=0; i<=cryoHe_SaturatedBase_meth_; i++) stt[i] = 0;
}
  // (1) from SUBROUTINE PSATFT (P, DPDTS, T)
  // Saturation pressure as a function of temperature
double cryoHe_SaturatedBase::Pres_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(1,Temp)) { todo(2,Temp);
    //
    double c2[8]  = {-7.41816, 5.42128, 9.903203, -9.617095, 6.804602, -3.0154606, 0.7461357, -0.0791791};
    double c1[11] = {-30.93285, 392.47361, -2328.04587, 8111.30347, -17809.80901, 25766.52747, -24601.4, 14944.65142, -5240.36518, 807.93168, 14.53333};
    double x,q0,q1,tn;
    //
    if (Temp <= crtTemp) {  //M=2
      q0 =  c2[0]/Temp + c2[1]; q1 = -c2[0]/Temp/Temp; tn = 1.;
      for (int j=2; j<8; q1+=c2[j]*tn*(j-1), tn*=Temp, q0+=c2[j]*tn, j++);
      val[1][0] = exp(q0);
      val[2][0] = q1*val[1][0];
    }
    else {  //M=1
      x = (Temp >= uppTemp)?1.:Temp/uppTemp;
      q0=c1[0]/x + c1[1]; q1=-c1[0]/x/x; tn=1.;
      for (int j=2; j<10; q1+=c1[j]*tn*(j-1), tn*=x, q0+=c1[j]*tn, j++);
      if (x != 1.) {
        q0 += (1.- x)*c1[10]*pow(1.- x,0.9);
        q1 -= 1.9*c1[10]*pow(1.- x,0.9);
      }
      val[1][0] = exp(q0);
      val[2][0] = q1*val[1][0]/uppTemp;
    }
  }
  return val[1][0];
}
  // (2) from SUBROUTINE PSATFT (P, DPDTS, T)
  // Saturation dP/dT as a function of temperature
double cryoHe_SaturatedBase::dPdT_vsT(double Temp, HepackError_t *perr)
{
  if (Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(2,Temp)) Pres_vsT(Temp, perr);
  return val[2][0];
}
  // (3) FUNCTION TSATFP (PP)
  // Temperature [K] as a function of pressure [Pa]
double cryoHe_SaturatedBase::Temp_vsP(double Pres, HepackError_t *perr)
{
  if (Pres <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(3,Pres)) {
    if (_TvsP == (spline *)0) {
      int NN=300, N1=200, N2=100, i=0;
      double *T = new double[NN+1], *P = new double[NN+1], tmpTemp = 5.106;
      for (double t=lowTemp, dT=(tmpTemp - t)/N1; i< N1; T[i]=t, P[i]=Pres_vsT(t, perr), i++, t=lowTemp+dT*i);
      for (double t=tmpTemp, dT=(uppTemp - t)/N2; i<=NN; T[i]=t, P[i]=Pres_vsT(t, perr), i++, t=tmpTemp+dT*(i-N1));
      _TvsP  = new spline_Classic(NN+1,P,T,1);
      delete [] T; delete [] P;
    }
    val[3][0] = (*_TvsP)(Pres);
  }
  return val[3][0];
}
  //
  // Destructor(s)
cryoHe_SaturatedBase::~cryoHe_SaturatedBase()
{
  if (_TvsP != (spline *)0)  delete _TvsP;
}
//
//  Private Part
//
int cryoHe_SaturatedBase::todo(int fNo, double p1)
{
  val[fNo][1] = p1;
  stt[fNo]    = 1;
  return 1;
}
