//
//  Calculations for Saturated Vapor
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 16-May-2006
//  Modified: 
//    24.Apr.2007 by V.Gubarev
//      o Back functions are replaced by splines
//
//  Notes:
//    1. Valid for temperature range 0.8 to 5.1953 K, T76 scale.
//    2. Test on input parameters is excluded (!)
//

#include "cryoHe_SaturatedVapor.h"

//  Class cryoHe_SaturatedVapor
//
cryoHe_SaturatedVapor::cryoHe_SaturatedVapor()
{
  for (int i=0; i<cryoHe_SaturatedVapor_Meth_; i++) stt[i] = 0;
  //
  lowTemp = sBase.lowTemp;
  uppTemp = sBase.uppTemp;
  ttpEnth = 4.17278;
  ttpEner = 4.20873;
  lowDens = Dens_vsT(lowTemp, NULL);
  uppDens = Dens_vsT(uppTemp, NULL);
  lowPres = Pres_vsT(lowTemp, NULL);
  uppPres = Pres_vsT(uppTemp, NULL);
  lowEntr = Entr_vsT(uppTemp, NULL);
  uppEntr = Entr_vsT(lowTemp, NULL);
  lowEnth = Enth_vsT(uppTemp, NULL);
  uppEnth = Enth_vsT(ttpEnth, NULL);
  lowEner = Ener_vsT(uppTemp, NULL);
  uppEner = Ener_vsT(ttpEner, NULL);
  lowGibb = Gibb_vsT(uppTemp, NULL);
  uppGibb = Gibb_vsT(lowTemp, NULL);
  //
  _TvsD = 0;
  _TvsS = 0;
  _TvsH = 0;
  _TvsU = 0;
  _TvsG = 0;
}
  // (1) FUNCTION D2VFPT (PSAT, TSAT)
  // Density [kg/m3] as a function of pressure [Pa] and temperature [K]
double cryoHe_SaturatedVapor::Dens_vsPT(double Pres, double Temp, HepackError_t *perr)
{
  int count = 0;
  if (Pres < 0. || Temp <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(1,Pres,Temp)) {
    double tnrc=5.106, tlow=4.804, delt=tnrc-tlow;
    if (Temp >= tnrc) val[1][0] = _svD_vsT(Temp);
    else {
      double Do=1e-8, Fo=He1Ph.Pres_vsDT(Do,Temp, perr), dD=10, Dn, Fn;
      while (dD > 1e-10) {
        for (Dn=Do+dD,Fn=He1Ph.Pres_vsDT(Dn,Temp, perr); ((Pres-Fo)*(Pres-Fn) > 0); Do=Dn,Fo=Fn,Dn+=dD,Fn=He1Ph.Pres_vsDT(Dn,Temp, perr));
        dD = (Dn - Do)/10;
        if (++count > 1000)
	  HE_ERROR(MAX_ITERATIONS)
      }
      val[1][0] = (Do + Dn)/2;
      if (Temp > tlow) val[1][0] = ((tnrc-Temp)*val[1][0] + (Temp-tlow)*_svD_vsT(Temp))/delt;
    }

  }
  return val[1][0];
}
  // (2) FUNCTION SATD (T)  (Use it only)
  // Density [kg/m3] as a function of temperature [K]; 0.8 < T < 5.1953
double cryoHe_SaturatedVapor::Dens_vsT(double Temp, HepackError_t *perr)
{
  todo(2,Temp);
  val[2][0] = Dens_vsPT(sBase.Pres_vsT(Temp, perr),Temp, perr);
  return val[2][0];
}
  // (3) from SUBROUTINE PSATFT (P, DPDTS, T)
  // Saturation pressure as a function of temperature
double cryoHe_SaturatedVapor::Pres_vsT(double Temp, HepackError_t *perr)
{
  todo(3,Temp);
  val[3][0] = sBase.Pres_vsT(Temp, perr);
  return val[3][0];
}
  // (4) from SUBROUTINE PSATFT (P, DPDTS, T)
  // Saturation dP/dT as a function of temperature
double cryoHe_SaturatedVapor::dPdT_vsT(double Temp, HepackError_t *perr)
{
  todo(4,Temp);
  val[4][0] = sBase.dPdT_vsT(Temp, perr);
  return val[4][0];
}
  // (5) from FUNCTION SATS (T)
  // Saturation Entropy as a function of temperature
double cryoHe_SaturatedVapor::Entr_vsT(double Temp, HepackError_t *perr)
{
  todo(5,Temp);
  val[5][0] = He1Ph.Entr_vsDT(Dens_vsPT(Pres_vsT(Temp, perr),Temp, perr),Temp, perr);
  return val[5][0];
}
  // (6) from FUNCTION SATVY (T)
  // Saturation Enthalpy as a function of temperature
double cryoHe_SaturatedVapor::Enth_vsT(double Temp, HepackError_t *perr)
{
  todo(6,Temp);
  val[6][0] = He1Ph.Enth_vsDTP(Dens_vsPT(Pres_vsT(Temp, perr),Temp, perr),Temp,Pres_vsT(Temp, perr), perr);
  return val[6][0];
}
  // (7) from FUNCTION SATVY (T)
  // Saturation Internal Energy as a function of temperature
double cryoHe_SaturatedVapor::Ener_vsT(double Temp, HepackError_t *perr)
{
  todo(7,Temp);
  val[7][0] = He1Ph.Ener_vsDT(Dens_vsPT(Pres_vsT(Temp, perr),Temp, perr),Temp, perr);
  return val[7][0];
}
  // (8) from FUNCTION SATVY (T)
  // Saturation Gibbs Energy as a function of temperature
double cryoHe_SaturatedVapor::Gibb_vsT(double Temp, HepackError_t *perr)
{
  todo(8,Temp);
  val[8][0] = He1Ph.Gibb_vsDTP(Dens_vsPT(Pres_vsT(Temp, perr),Temp, perr),Temp,Pres_vsT(Temp, perr), perr);
  return val[8][0];
}

  // (9) FUNCTION TSATFP (PP)
  // Temperature [K] as a function of pressure [Pa]
double cryoHe_SaturatedVapor::Temp_vsP(double Pres, HepackError_t *perr)
{
  todo(9,Pres);
  val[9][0] = sBase.Temp_vsP(Pres, perr);
  return val[9][0];
}

//  Function = Function(Entropy)
//  ============================

  // (10) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
  // Temperature as a function of entropy (SI units)
double cryoHe_SaturatedVapor::Temp_vsS(double Entr, HepackError_t *perr)
{
  if (todo(10,Entr)) {
    if (_TvsS == (spline *)0) {
      int NN=600, N1=100, N2=200, N3=300, i=0;
      double *T = new double[NN+1], *S = new double[NN+1], crTemp1 = 0.82, crTemp2 = 5.07;
      for (double t=lowTemp, dT=(crTemp1 - t)/N1; i< N1;    T[i]=t, S[i]=Entr_vsT(t, perr), i++, t=lowTemp+dT*i);
      for (double t=crTemp1, dT=(crTemp2 - t)/N2; i< N1+N2; T[i]=t, S[i]=Entr_vsT(t, perr), i++, t=crTemp1+dT*(i-N1));
      for (double t=crTemp2, dT=(uppTemp - t)/N3; i<=NN;    T[i]=t, S[i]=Entr_vsT(t, perr), i++, t=crTemp2+dT*(i-N1-N2));
      _TvsS  = new spline_Smooth(NN+1,S,T,1);
      delete [] S; delete [] T;
    }
    val[10][0] = (*_TvsS)(Entr);
  }
  return val[10][0];
}
  // (11) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
  // Pressure as a function of entropy (SI units)
double cryoHe_SaturatedVapor::Pres_vsS(double Entr, HepackError_t *perr)
{
  todo(11,Entr);
  val[11][0] = Pres_vsT(Temp_vsS(Entr, perr), perr);
  return val[11][0];
}
  // (12) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
  // dP/dT as a function of entropy (SI units)
double cryoHe_SaturatedVapor::dPdT_vsS(double Entr, HepackError_t *perr)
{
  todo(12,Entr);
  val[12][0] = dPdT_vsT(Temp_vsS(Entr, perr), perr);
  return val[12][0];
}
  // (13) from SUBROUTINE SATFS (IDID, QUAL, PS, XDPDT, TS, DL, DV, SS)
  // Liquid Density as a function of entropy (SI units)
double cryoHe_SaturatedVapor::Dens_vsS(double Entr, HepackError_t *perr)
{
  todo(13,Entr);
  val[13][0] = Dens_vsPT(Pres_vsS(Entr, perr),Temp_vsS(Entr, perr), perr);
  return val[13][0];
}

//  Function = Function(Density)
//  ============================

  // (20) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
  // Temperature as a function of Density (SI units)
double cryoHe_SaturatedVapor::Temp_vsD(double Dens, HepackError_t *perr)
{
  if (Dens <= 0.) HE_ERROR(VALUE_OUT)
  else if (todo(20,Dens)) {
    double DNRCV=46.5087;
    if (Dens > DNRCV) {
      val[20][0] = (uppDens - Dens)/(uppDens - DNRCV);
      if (fabs(val[20][0]) < 1e-8) val[20][0] = 1e-8;
      val[20][0] = uppTemp - 0.0893*pow(val[20][0],3);
    }
    else {
      if (_TvsD == (spline *)0) {
        int NN=300, i=0;
        double *D = new double[NN+1], *T = new double[NN+1];
        for (double t=lowTemp, dT=(uppTemp - t)/NN; i<=NN; T[i]=t, D[i]=Dens_vsT(t, perr), i++, t=lowTemp+dT*i);
        _TvsD  = new spline_Classic(NN+1,D,T,1);
        delete [] D; delete [] T;
      }
      val[20][0] = (*_TvsD)(Dens);
    }
  }
  return val[20][0];
}
  // (21) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
  // Pressure as a function of Density (SI units)
double cryoHe_SaturatedVapor::Pres_vsD(double Dens, HepackError_t *perr)
{
  todo(21,Dens);
  val[21][0] = Pres_vsT(Temp_vsD(Dens, perr), perr);
  return val[21][0];
}
  // (22) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
  // dP/dT as a function of Density (SI units)
double cryoHe_SaturatedVapor::dPdT_vsD(double Dens, HepackError_t *perr)
{
  todo(22,Dens);
  val[22][0] = dPdT_vsT(Temp_vsD(Dens, perr), perr);
  return val[22][0];
}
  // (23) from SUBROUTINE SATFD (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, RHO)
  // Liquid Density as a function of Density (SI units)
double cryoHe_SaturatedVapor::Dens_vsD(double Dens, HepackError_t *perr)
{
  todo(23,Dens);
  val[23][0] = Dens_vsPT(Pres_vsD(Dens, perr),Temp_vsD(Dens, perr), perr);
  return val[23][0];
}

//  Function = Function(Enthalpy)
//  =============================

  // (30) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
  // Temperature as a function of Enthalpy (SI units)
double cryoHe_SaturatedVapor::Temp_vsH(double Enth, HepackError_t *perr)
{
  if (todo(30,Enth)) {
    if (_TvsH == (spline *)0) {
      int NN=600, i=0;
      double *H = new double[NN+1], *T = new double[NN+1];
      for (double t=ttpEnth, dT=(uppTemp - t)/NN; i<=NN; T[i]=t, H[i]=Enth_vsT(t, perr), i++, t=ttpEnth+dT*i);
      _TvsH  = new spline_Smooth(NN+1,H,T,1);
      delete [] H; delete [] T;
    }
    val[30][0] = (*_TvsH)(Enth);
  }
  return val[30][0];
}
  // (31) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
  // Pressure as a function of Enthalpy (SI units)
double cryoHe_SaturatedVapor::Pres_vsH(double Enth, HepackError_t *perr)
{
  if (todo(31,Enth)) val[31][0] = Pres_vsT(Temp_vsH(Enth, perr), perr);
  return val[31][0];
}
  // (32) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
  // dP/dT as a function of Enthalpy (SI units)
double cryoHe_SaturatedVapor::dPdT_vsH(double Enth, HepackError_t *perr)
{
  if (todo(32,Enth)) val[32][0] = dPdT_vsT(Temp_vsH(Enth, perr), perr);
  return val[32][0];
}
  // (33) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'H')
  // Liquid Density as a function of Enthalpy (SI units)
double cryoHe_SaturatedVapor::Dens_vsH(double Enth, HepackError_t *perr)
{
  if (todo(33,Enth)) val[33][0] = Dens_vsPT(Pres_vsH(Enth, perr),Temp_vsH(Enth, perr), perr);
  return val[33][0];
}

//  Function = Function(Internal Energy)
//  ====================================

  // (40) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
  // Temperature as a function of Internal Energy (SI units)
double cryoHe_SaturatedVapor::Temp_vsU(double Ener, HepackError_t *perr)
{
  if (todo(40,Ener)) {
    if (_TvsU == (spline *)0) {
      int NN=600, i=0;
      double *U = new double[NN+1], *T = new double[NN+1];
      for (double t=ttpEner, dT=(uppTemp - t)/NN; i<=NN; T[i]=t, U[i]=Ener_vsT(t, perr), i++, t=ttpEner+dT*i);
      _TvsU  = new spline_Smooth(NN+1,U,T,1);
      delete [] U; delete [] T;
    }
    val[40][0] = (*_TvsU)(Ener);
  }
  return val[40][0];
}
  // (41) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
  // Pressure as a function of Internal Energy (SI units)
double cryoHe_SaturatedVapor::Pres_vsU(double Ener, HepackError_t *perr)
{
  if (todo(41,Ener)) val[41][0] = Pres_vsT(Temp_vsU(Ener, perr), perr);
  return val[41][0];
}
  // (42) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
  // dP/dT as a function of Internal Energy (SI units)
double cryoHe_SaturatedVapor::dPdT_vsU(double Ener, HepackError_t *perr)
{
  if (todo(42,Ener)) val[42][0] = dPdT_vsT(Temp_vsU(Ener, perr), perr);
  return val[42][0];
}
  // (43) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'U')
  // Liquid Density as a function of Internal Energy (SI units)
double cryoHe_SaturatedVapor::Dens_vsU(double Ener, HepackError_t *perr)
{
  if (todo(43,Ener)) val[43][0] = Dens_vsPT(Pres_vsU(Ener, perr),Temp_vsU(Ener, perr), perr);
  return val[43][0];
}

//  Function = Function(Gibbs Energy)
//  =================================

  // (50) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
  // Temperature as a function of Gibbs Energy (SI units)
double cryoHe_SaturatedVapor::Temp_vsG(double Gibb, HepackError_t *perr)
{
  if (todo(50,Gibb)) {
    if (_TvsG == (spline *)0) {
      int NN=600, i=0;
      double *G = new double[NN+1], *T = new double[NN+1];
      for (double t=lowTemp, dT=(uppTemp - t)/NN; i<=NN; T[i]=t, G[i]=Gibb_vsT(t, perr), i++, t=lowTemp+dT*i);
      _TvsG  = new spline_Classic(NN+1,G,T,1);
      delete [] G; delete [] T;
    }
    val[50][0] = (*_TvsG)(Gibb);
  }
  return val[50][0];
}
  // (51) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
  // Pressure as a function of Gibbs Energy (SI units)
double cryoHe_SaturatedVapor::Pres_vsG(double Gibb, HepackError_t *perr)
{
  if (todo(51,Gibb)) val[51][0] = Pres_vsT(Temp_vsG(Gibb, perr), perr);
  return val[51][0];
}
  // (52) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
  // dP/dT as a function of Gibbs Energy (SI units)
double cryoHe_SaturatedVapor::dPdT_vsG(double Gibb, HepackError_t *perr)
{
  if (todo(52,Gibb)) val[52][0] = dPdT_vsT(Temp_vsG(Gibb, perr), perr);
  return val[52][0];
}
  // (53) from SUBROUTINE SATFY (IDID, QUAL, PSAT, DPDT, TSAT, DL, DV, YY, 'G')
  // Liquid Density as a function of Gibbs Energy (SI units)
double cryoHe_SaturatedVapor::Dens_vsG(double Gibb, HepackError_t *perr)
{
  if (todo(53,Gibb)) val[53][0] = Dens_vsPT(Pres_vsG(Gibb, perr),Temp_vsG(Gibb, perr), perr);
  return val[53][0];
}
  //
  // Destructor(s)
cryoHe_SaturatedVapor::~cryoHe_SaturatedVapor()
{
  if (_TvsD != (spline *)0) delete _TvsD;
  if (_TvsS != (spline *)0) delete _TvsS;
  if (_TvsH != (spline *)0) delete _TvsH;
  if (_TvsU != (spline *)0) delete _TvsU;
  if (_TvsG != (spline *)0) delete _TvsG;
}
//
//  Private Part
//
  //    FUNCTION DGSAT (T)
  // Density [kg/m3] as a function of temperature [K]
  // Saturated vapor density, 0.8 < T < 5.1953.
  // This is the best independent estimate of saturation density;
  // Theoretical form above TNRC, fitted for continuity at TNRC.
double cryoHe_SaturatedVapor::_svD_vsT(double Temp)
{
  //
  double Ttp=2.1768,      Tnrc=5.1060,  Tcc=5.1953;
  double Dtp=0.294058864, Dnrc=46.5087, Dcc=17.399;
  double out = -1;
  //
  if (Temp > Tnrc) {
    if ((out = (Tcc-Temp)/(Tcc-Tnrc)) < 1e-20) out = 1e-20;
    return (Dcc + (Dnrc/4.0026 - Dcc)*pow(out,1./3))*4.0026;
  }
  else if (Temp > Ttp) {
    double a1=-0.5618978079e+03, a2=0.3300736785e+01, a3=-0.6031200561e+02, a4=0.6129901560e+03;
    double a5=-0.2718577178e+04, a6=0.1285185020e+04, a7=-0.4406873907e+03, a8=0.7163145577e+02;
    double x = (Temp - Tcc)/(Ttp - Tcc), y = pow(x,1./3);
    if (x <= 0.) return 0.;
    out = a1*log(x) + a2*(1.-1./x) + a3*(1.-y/x) + a4*(1.-y*y/x) + a5*(1.-y) + a6*(1.-y*y)  + a7*(1.-y*y*y)+ a8*(1.-y*y*y*y);
    return (Dcc + exp(out)*(Dtp - Dcc))*4.0026;
  }
  else {
    double b1=-7.41816,  b2= 5.42128,   b3=9.903203,  b4=-9.617095;
    double b5= 6.804602, b6=-3.0154606, b7=0.7461357, b8=-0.0791791;
    if (Temp <= 0.) return 0.;
    out = exp(b1/Temp+b2+Temp*(b3+Temp*(b4+Temp*(b5+Temp*(b6+Temp*(b7+Temp*b8))))))/2077.2258/Temp;
    return (out/(1+out*(0.0537 - 0.514/Temp)/4.0026) + 0.00164*pow(out,3));
  }
  return out;
}

int cryoHe_SaturatedVapor::todo(int fNo, double p1)
{
  return todo(fNo, p1, -1);
}
int cryoHe_SaturatedVapor::todo(int fNo, double p1, double p2)
{
  val[fNo][1] = p1;
  val[fNo][2] = p2;
  stt[fNo]    = 1;
  return 1;
}
