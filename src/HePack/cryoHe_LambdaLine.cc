//
//  Calculation of the Lambda Line parameters according with input parameters
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 12-May-2006
//  Modified: 
//    21.Apr.2007 by V.Gubarev
//      o Excluded cryoHe_LambdaLineBase
//      o Functions are replaced by splines
//
//  Notes:
//    1. Valid in compressed liquid for temperature from 0.8 to about 3 K.
//    2. Test on input parameters excluded (!)
//    3. Notes for V.Arp equations:
//      If T is less than 0.8 K, T is assumed to be the isochoric distance
//      to the lambda line (negative in HeII, positive in HeI).
//

#include "cryoHe_LambdaLine.h"
extern "C" {
#include "HepackError.h"
}

//  Class cryoHe_LambdaLine
//
cryoHe_LambdaLine::cryoHe_LambdaLine() {
  uppVolm = PrpHe_Vo;
  lowDens = 1000. / uppVolm;
  uppDens = 179.84;
  lowTemp = Temp_vsD(uppDens, NULL);
  uppTemp = PrpHe_To;
  lowPres = Pres_vsT(uppTemp, NULL);
  uppPres = Pres_vsT(lowTemp, NULL);
  //
  _DvsT = 0;
  _TvsP = 0;
}
// from V.Arp equations
// returns 1 for HeI, 2 for HeII
int cryoHe_LambdaLine::HeState_vsDT(double Dens, double Temp, HepackError_t *perr) {
  return (distT_vsDT(Dens, Temp, perr) > 0) ? 1 : 2;
}
// from V.Arp equations
// isochoric distance to the lambda line
double cryoHe_LambdaLine::distT_vsDT(double Dens, double Temp, HepackError_t *perr) {
  return (Temp < 0.8) ? Temp : (Temp - Temp_vsD(Dens, perr));
}
// from V.Arp equations
// recalculated He Temperature according Lambda Line
double cryoHe_LambdaLine::HeTemp_vsDT(double Dens, double Temp, HepackError_t *perr) {
  return (Temp < 0.8) ? (Temp_vsD(Dens, perr) + Temp) : Temp;
}
//  FUNCTION TLFD (D)
// Lambda line temperature [K] as function of density [kg/m3]
double cryoHe_LambdaLine::Temp_vsD(double Dens, HepackError_t *perr) {
  double a1 = 0.91672438e-01, a2 = -0.82840336e-01, a3 = 0.71832749e-01, a4 = 0.48395170e-01, a5 = 0.39159012e-01;
  double x = 0.;
  double result = 0.;
  if (Dens > 0.) {
    x = 1000. / Dens - uppVolm;
    result = (PrpHe_To + x * (a1 + x * (a2 + x * (a3 + x * (a4 + x * a5)))));
  } else
    HE_ERROR(VALUE_OUT)

  return result;
}
//  from SUBROUTINE LAMDER (V)    !!! not Si-Units
// dT/dV [(K*g)/cm3] along the lambda line as a function of density [kg/m3]
double cryoHe_LambdaLine::dTdV_vsD(double Dens, HepackError_t *perr) {
  double a1 = 0.91672438e-01, a2 = -0.82840336e-01, a3 = 0.71832749e-01, a4 = 0.48395170e-01, a5 = 0.39159012e-01;
  double result = 0;
  if (Dens > 0.) {
    double x = 1000. / Dens - uppVolm;
    result = (a1 + x * (2. * a2 + x * (3. * a3 + x * (4. * a4 + x * 5. * a5))));
  } else
    HE_ERROR(VALUE_OUT)
 
  return result;
}
//  from SUBROUTINE LAMDER (V)    !!! not Si-Units
// d2T/dV2 [(K*g2)/cm6] along the lambda line as a function of density [kg/m3]
double cryoHe_LambdaLine::d2TdV2_vsD(double Dens, HepackError_t *perr) {
  double a2 = -0.82840336e-01, a3 = 0.71832749e-01, a4 = 0.48395170e-01, a5 = 0.39159012e-01;
  double result = 0;
  if (Dens > 0.) {
    double x = 1000. / Dens - uppVolm;
    result = (2. * a2 + x * (6. * a3 + x * (12. * a4 + x * 20. * a5)));
  } else
    HE_ERROR(VALUE_OUT)

  return result;
}
//  FUNCTION TLFP (P)
// Lambda line temperature [K] as a function of pressure [Pa]
double cryoHe_LambdaLine::Temp_vsP(double Pres, HepackError_t *perr) {
  if (_TvsP == (spline *) 0)
    mkFun_TvsP(perr);
  return (*_TvsP)(Pres);
}
//  FUNCTION DLFT (TT)
// Lambda line density [kg/m3] as a function of temperature [K]
// valid range is 2.1768 to 1.7673
double cryoHe_LambdaLine::Dens_vsT(double Temp, HepackError_t *perr) {
  if (_DvsT == (spline *) 0)
    mkFun_DvsT(perr);
  return ((lowTemp <= Temp) && (Temp <= uppTemp)) ? (*_DvsT)(Temp) : -1;
}
//  FUNCTION DLFP (P)
// Lambda line density [kg/m3] as a function of pressure [Pa]
double cryoHe_LambdaLine::Dens_vsP(double Pres, HepackError_t *perr) {
  return (Dens_vsT(Temp_vsP(Pres, perr), perr));
}
//                                !!! not Si-Units
// V-Vo [cm3/g], where Vo is the volume at the lower lambda point
double cryoHe_LambdaLine::deltaV_vsD(double Dens, HepackError_t *perr) {
  double result = 0;
  if (Dens > 0.) {
    result = (1000. / Dens - PrpHe_Vo);
  } else
    HE_ERROR(VALUE_OUT)

  return result;
}
//  from SUBROUTINE KIERST (P, XDPDT, D, DDDT, T)    !!! not Si-Units
// Lambda-line pressure [Pa] as function of T [K]
// 5041.8 at T = 2.1768, 30.134e+5 at T = 1.7673
double cryoHe_LambdaLine::Pres_vsT(double Temp, HepackError_t *perr) {
  double a1 = 0.42774167, a2 = -94.820469, a3 = -85.817089, a4 = -102.39597, a5 = -76.735240, a6 = -0.37798315, a7 =
      42.148155;
  double x = Temp - PrpHe_To;
  return ((a1 + (a2 + (a3 + (a4 + a5 * x) * x) * x) * x + a6 * exp(a7 * x)) * 101325.);
}
//  from SUBROUTINE KIERST (P, XDPDT, D, DDDT, T)    !!! not Si-Units
// dP/dT along the lambda line [Pa/K]
double cryoHe_LambdaLine::dPdT_vsT(double Temp, HepackError_t *perr) {
  double a2 = -94.820469, a3 = -85.817089, a4 = -102.39597, a5 = -76.735240, a6 = -0.37798315, a7 =
      42.148155;
  double x = Temp - PrpHe_To;
  return ((a2 + x * (2. * a3 + x * (3. * a4 + x * 4. * a5)) + a6 * a7 * exp(a7 * x)) * 101325.);
}
//  from SUBROUTINE KIERST (P, XDPDT, D, DDDT, T)    !!! not Si-Units
// dD/dT along the lambda line [kg/(K*m3)]
double cryoHe_LambdaLine::dDdT_vsT(double Temp, HepackError_t *perr) {
  double b2 = -0.15036724, b3 = -0.32811465, b4 = -0.52635312, b5 = -0.37937084, b6 = -0.00226216, b7 =
      36.645230;
  double x = Temp - PrpHe_To;
  return ((b2 + x * (2. * b3 + x * (3. * b4 + x * 4. * b5)) + b6 * b7 * exp(b7 * x)) * 1000.);
}
//
// Destructor(s)
cryoHe_LambdaLine::~cryoHe_LambdaLine() {
  if (_DvsT != (spline *) 0)
    delete _DvsT;
  if (_TvsP != (spline *) 0)
    delete _TvsP;
}
//
//  Private Part
//
void cryoHe_LambdaLine::mkFun_DvsT(HepackError_t *perr) {
  int NN = 100, i = 0;
  double *D = new double[NN + 1], *T = new double[NN + 1];
  for (double d = 0.9 * lowDens, dD = (1.1 * uppDens - d) / NN; i <= NN;
      D[i] = d, T[i] = Temp_vsD(d, perr), d += dD, i++)
    ;
  _DvsT = new spline_Classic(NN + 1, T, D, 1);
  delete[] D;
  delete[] T;
}

void cryoHe_LambdaLine::mkFun_TvsP(HepackError_t *perr) {
  int NN = 100, i = 0;
  double *T = new double[NN + 1], *P = new double[NN + 1];
  for (double t = 0.9 * lowTemp, dT = (1.1 * uppTemp - t) / NN; i <= NN; T[i] = t, P[i] = Pres_vsT(t, perr), t += dT, i++)
    ;
  _TvsP = new spline_Classic(NN + 1, P, T, 1);
  delete[] T;
  delete[] P;
}
