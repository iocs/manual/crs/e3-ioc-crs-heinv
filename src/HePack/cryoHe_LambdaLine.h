//
//  Calculation of the Lambda Line parameters according with input parameters
//
//  Created by: V.Gubarev -MKS1-
//  Created on: 12-May-2006
//  Modified: 
//    21.Apr.2007 by V.Gubarev
//      o Excluded cryoHe_LambdaLineBase
//      o Back functions are replaced by splines
//
//  Notes:
//    1. Valid in compressed liquid for temperature from 0.8 to about 3 K.
//    2. Test on input parameters excluded (!)
//    3. Notes for V.Arp equations:
//      If T is less than 0.8 K, T is assumed to be the isochoric distance
//      to the lambda line (negative in HeII, positive in HeI).
//

#ifndef _cryoHe_LambdaLine_h
#define _cryoHe_LambdaLine_h

#include <math.h>

#include "spline_Classic.h"
#include "spline_Smooth.h"

#include "HepackError.h"

#include "cryoHe_InitParameters.h"

class cryoHe_LambdaLine
{
  public:
  
    // Constructor(s)
    cryoHe_LambdaLine();
    
      //  from V.Arp equations
      // returns 1 for HeI, 2 for HeII
    int    HeState_vsDT(double Dens, double Temp, HepackError_t *perr);

      //  from V.Arp equations
      // isochoric distance to the lambda line
    double distT_vsDT(double Dens, double Temp, HepackError_t *perr);

      //  from V.Arp equations
      // recalculated He Temperature according Lambda Line
    double HeTemp_vsDT(double Dens, double Temp, HepackError_t *perr);

      //  FUNCTION TLFD (D)
      // Lambda line temperature [K] as function of density [kg/m3]
    double Temp_vsD(double Dens, HepackError_t *perr);

      //  FUNCTION TLFP (P)
      // Lambda line temperature [K] as a function of pressure [Pa]
    double Temp_vsP(double Pres, HepackError_t *perr);

      //  FUNCTION DLFT (TT)
      // Lambda line density [kg/m3] as a function of temperature [K]
    double Dens_vsT(double Temp, HepackError_t *perr);

      //  FUNCTION DLFP (P)
      // Lambda line density [kg/m3] as a function of pressure [Pa]
    double Dens_vsP(double Pres, HepackError_t *perr);

      //  from SUBROUTINE LAMDER (V)    !!! not Si-Units
      // dT/dV [(K*g)/cm3] along the lambda line as a function of density [kg/m3]
    double dTdV_vsD(double Dens, HepackError_t *perr);

      //  from SUBROUTINE LAMDER (V)    !!! not Si-Units
      // d2T/dV2 [(K*g2)/cm6] along the lambda line as a function of density [kg/m3]
    double d2TdV2_vsD(double Dens, HepackError_t *perr);

      //                                !!! not Si-Units
      // V-Vo [cm3/g], where Vo is the volume at the lower lambda point
    double deltaV_vsD(double Dens, HepackError_t *perr);

      //  from SUBROUTINE KIERST (P, XDPDT, D, DDDT, T)    !!! not Si-Units
      // Lambda-line pressure [Pa] as function of T [K]
      // 5041.8 at T = 2.1768, 30.134e+5 at T = 1.7673
    double Pres_vsT(double Temp, HepackError_t *perr);

      //  from SUBROUTINE KIERST (P, XDPDT, D, DDDT, T)    !!! not Si-Units
      // dP/dT along the lambda line [Pa/K]
    double dPdT_vsT(double Temp, HepackError_t *perr);

      //  from SUBROUTINE KIERST (P, XDPDT, D, DDDT, T)    !!! not Si-Units
      // dD/dT along the lambda line [kg/(K*m3)]
    double dDdT_vsT(double Temp, HepackError_t *perr);
    
    // Destructor(s)
    ~cryoHe_LambdaLine();
    
    //  Limited Variables
    double uppVolm;
    double lowTemp;   // low Temperature [K]
    double uppTemp;   // upper Temperature [K]
    double lowDens;   // low Density [kg/m3]
    double uppDens;   // upp Density [kg/m3]
    double lowPres;   // low Pressure [Pa]
    double uppPres;   // upp Pressure [Pa]
    
  private:
    //
    spline *_DvsT;
    spline *_TvsP;
    //
    void mkFun_DvsT(HepackError_t *perr);
    void mkFun_TvsP(HepackError_t *perr);
};

#endif
